-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 16, 2018 alle 09:22
-- Versione del server: 10.1.22-MariaDB
-- Versione PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `id_carrello` int(11) NOT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_sessione_utente` varchar(250) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_creazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `taglia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(11) NOT NULL,
  `url_categorie` varchar(25) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `immagine` varchar(150) NOT NULL,
  `label_color_class` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `url_categorie`, `nome`, `descrizione`, `immagine`, `label_color_class`, `stato`, `ordine`) VALUES
(1, 'house', 'House', 'House products', 'c82f6-th11.jpg', 3, 1, 1),
(2, 'clocks', 'Clocks', 'Clocks types', 'd537b-th14.jpg', 1, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery`
--

CREATE TABLE `categorie_gallery` (
  `id_categoria_gallery` int(11) NOT NULL,
  `nome_categoria_gallery` varchar(250) NOT NULL,
  `stato_categoria_gallery` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery`
--

INSERT INTO `categorie_gallery` (`id_categoria_gallery`, `nome_categoria_gallery`, `stato_categoria_gallery`) VALUES
(1, 'Design', 1),
(2, 'Clothes', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery_traduzioni`
--

CREATE TABLE `categorie_gallery_traduzioni` (
  `id_categorie_gallery_traduzioni` int(11) NOT NULL,
  `id_categoria_gallery` int(11) NOT NULL,
  `descrizione_categoria_gallery` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery_traduzioni`
--

INSERT INTO `categorie_gallery_traduzioni` (`id_categorie_gallery_traduzioni`, `id_categoria_gallery`, `descrizione_categoria_gallery`, `id_lingua`) VALUES
(1, 1, 'Design', 2),
(2, 1, 'Design', 1),
(3, 2, 'Clothes', 2),
(4, 2, 'Abbigliamento', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE `clienti` (
  `id_cliente` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `punti` bigint(20) NOT NULL DEFAULT '0',
  `id_lingua` int(11) NOT NULL DEFAULT '1',
  `id_indirizzo_fatturazione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`id_cliente`, `user_id`, `nome`, `cognome`, `email`, `telefono`, `partita_iva`, `codice_fiscale`, `newsletter`, `punti`, `id_lingua`, `id_indirizzo_fatturazione`) VALUES
(4, 9, 'Buongiorno', 'Amore', 'posta@buongiornoamore.it', '4242342342', '', '', 0, 11, 1, 2),
(5, 0, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', '3207753626', '', '', 1, 0, 2, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_classi`
--

CREATE TABLE `colori_classi` (
  `id_colore_classe` int(11) NOT NULL,
  `colore_classe` varchar(100) NOT NULL,
  `colore_classe_nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_classi`
--

INSERT INTO `colori_classi` (`id_colore_classe`, `colore_classe`, `colore_classe_nome`) VALUES
(1, 'text-danger', 'Rosso'),
(2, 'text-success', 'Verde'),
(3, 'text-warning', 'Arancio');

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_prodotti`
--

CREATE TABLE `colori_prodotti` (
  `id_colori_prodotti` int(11) NOT NULL,
  `nome_colore` varchar(50) NOT NULL,
  `codice_colore` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_prodotti`
--

INSERT INTO `colori_prodotti` (`id_colori_prodotti`, `nome_colore`, `codice_colore`) VALUES
(1, 'Black', '000000'),
(2, 'White', 'ffffff'),
(3, 'Aqua', '43a9d1'),
(4, 'Berry', 'd2528f'),
(5, 'Blue', '6680b3'),
(6, 'Coral', 'ce474d'),
(7, 'Creamy', 'e5ded8'),
(8, 'Navy', '26273b'),
(9, 'Purple', '523756'),
(10, 'Pink', 'eba3b9'),
(11, 'Raspberry', 'a91671'),
(12, 'Jade', '9cc3c0'),
(13, 'Red', 'ae2f38'),
(14, 'Yellow', 'f3dc5d'),
(15, 'Green', '357249'),
(16, 'Darkgrey', '45413e'),
(17, 'Hotpink', 'db2c77'),
(18, 'Royal', '3e609d'),
(19, 'Strawberry', 'a11927'),
(20, 'Silver', 'c6c6c6'),
(21, 'Darkgreen', '35734a'),
(22, 'Grey', '474340');

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.seiessenzialmentebella.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Sei essenzialmente bella', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit18.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'sei@seiessenzialmentebella.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'oz17@Sei', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.118', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'seiessen', 'Utente del DB'),
(8, 'DB_PASSWORD', 'sei17@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'seiessen_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Seiessenzialmentebella', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'sei@seiessenzialmentebella.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', '&copy; 2017 Seiessenzialmentebella', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3923576272', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Sardegna', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/dottoressaorlenazotti/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/dottoressaorlenazotti/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', 'UA-108374307-1', 'UID Google Analitycs'),
(22, 'PAYPAL_EMAIL', '', 'Email di Paypal per i pagamenti'),
(23, 'PAYPAL_ENV', '', 'Environment di paypal sviluppo/produzione'),
(24, 'PAYPAL_SANDBOX_CLIENT_ID', '', 'Cliend ID sandbox di Paypal'),
(25, 'PAYPAL_LIVE_CLIENT_ID', '', 'Cliend ID sandbox di Paypal LIVE'),
(26, 'STRIPE_PK', '', 'Stipe Public Key'),
(27, 'STRIPE_SK', '', 'Stripe Secret Key');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(2, 'Roberto', 'roberto.rossi77@gmail.com', '0', 'Ciao volevo sapere quanto costa una spedizione espressa e se è possibile.\r\nGrazie', '2018-01-08 10:14:04', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(6, 'roberto.rossi77@gmail.com', '2018-01-07 23:00:00', 1, '2018-01-08 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `coupon`
--

CREATE TABLE `coupon` (
  `id_coupon` int(11) NOT NULL,
  `data_scadenza_coupon` datetime DEFAULT NULL,
  `importo_coupon` double NOT NULL,
  `percentuale_coupon` int(5) NOT NULL,
  `codice_coupon` varchar(250) NOT NULL,
  `stato_coupon` int(11) NOT NULL,
  `tipo_coupon` int(11) NOT NULL,
  `utilizzatore_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `subject_template`, `titolo_template`, `testo_template`, `id_tipo_template`, `lingua_traduzione_id`) VALUES
(1, 'Prova ', 'Newsletter da Ma Chlò', 'La mia prima newsletter', 'Questo è il body della mia priam newsletter <br><br>\r\nGrazie\r\n<a href=\"buongiornoamore.it\">ba</a>', 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE `immagini_gallery` (
  `id_ig` int(11) NOT NULL,
  `id_categoria_ig` int(11) NOT NULL,
  `nome_ig` varchar(250) NOT NULL,
  `stato_ig` tinyint(4) NOT NULL,
  `immagine_thumb_ig` varchar(250) NOT NULL,
  `immagine_ig` varchar(250) NOT NULL,
  `ordine_ig` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`id_ig`, `id_categoria_ig`, `nome_ig`, `stato_ig`, `immagine_thumb_ig`, `immagine_ig`, `ordine_ig`) VALUES
(1, 2, 'Socks', 1, '09714-th05.jpg', '781e1-05.jpg', 1),
(2, 2, 'T-shirt', 1, '6ac21-th09.jpg', 'd54bb-09.jpg', 2),
(3, 1, 'Phone cover', 1, '4f48a-th07.jpg', '8c24a-07.jpg', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery_traduzioni`
--

CREATE TABLE `immagini_gallery_traduzioni` (
  `id_immagini_gallery_traduzioni` int(11) NOT NULL,
  `titolo_ig` varchar(250) NOT NULL,
  `testo_ig` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_ig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery_traduzioni`
--

INSERT INTO `immagini_gallery_traduzioni` (`id_immagini_gallery_traduzioni`, `titolo_ig`, `testo_ig`, `id_lingua`, `id_ig`) VALUES
(1, 'Cover telefono', 'Una cover di design', 1, 3),
(2, 'Phone cover', 'A design phone cover', 2, 3),
(3, 'T-shirt', 'Maglietta estiva', 1, 2),
(4, 'T-shirt', 'Summer t-short', 2, 2),
(5, 'Socks', 'Printed socks', 2, 1),
(6, 'Calzini', 'Calzini stampati', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_fatturazione`
--

CREATE TABLE `indirizzo_fatturazione` (
  `id_indirizzo_fatturazione` bigint(20) NOT NULL,
  `indirizzo_fatt` varchar(255) DEFAULT NULL,
  `civico_fatt` varchar(15) DEFAULT NULL,
  `cap_fatt` varchar(10) DEFAULT NULL,
  `nazione_fatt` varchar(50) DEFAULT NULL,
  `citta_fatt` varchar(50) DEFAULT NULL,
  `riferimento_fatt` varchar(250) DEFAULT NULL,
  `note_fatt` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_spedizione`
--

CREATE TABLE `indirizzo_spedizione` (
  `id_indirizzo_spedizione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_sped` varchar(255) DEFAULT NULL,
  `civico_sped` varchar(15) DEFAULT NULL,
  `cap_sped` varchar(10) DEFAULT NULL,
  `nazione_sped` varchar(50) DEFAULT NULL,
  `citta_sped` varchar(50) DEFAULT NULL,
  `riferimento_sped` varchar(250) DEFAULT NULL,
  `note_sped` varchar(100) DEFAULT NULL,
  `flag_predefinito_sped` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels`
--

CREATE TABLE `lingue_labels` (
  `id_lingue_labels` int(11) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `prodotti` varchar(50) NOT NULL,
  `fronte` varchar(20) NOT NULL,
  `retro` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels`
--

INSERT INTO `lingue_labels` (`id_lingue_labels`, `id_lingua`, `prodotti`, `fronte`, `retro`) VALUES
(1, 1, 'prodotti', 'FRONTE', 'RETRO'),
(2, 2, 'products', 'FRONT', 'BACK');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `id_ordine` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_ordine` date DEFAULT NULL,
  `totale_ordine` double DEFAULT NULL,
  `note_ordine` text NOT NULL,
  `tipo_pagamento` int(11) NOT NULL,
  `stato_pagamento` int(11) NOT NULL,
  `token_pagamento` varchar(250) NOT NULL,
  `stato_ordine` tinyint(4) NOT NULL DEFAULT '1',
  `id_indirizzo_spedizione` int(11) DEFAULT NULL,
  `id_indirizzo_fatturazione_spedizione` int(11) DEFAULT NULL,
  `punti` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(250) NOT NULL,
  `coupon_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `ordine_menu` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `ordine_menu`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', '', 0),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 1),
(3, 'Negozio', 'it/negozio', 1, 'frontend/Home/shop', 'dinamica', 'negozio', 2),
(4, 'Negozio', 'it/negozio/(:any)', 1, 'frontend/Home/shop/$1', 'dinamica', 'negozio', 0),
(5, 'Chi siamo', 'it/chisiamo', 1, 'frontend/Home/about', 'statica', '', 3),
(6, 'Spedizioni', 'it/spedizioni', 1, 'frontend/Home/shipping', 'statica', '', 4),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contacts', 'statica', '', 8),
(8, 'Regolamento', 'it/regolamento', 1, 'frontend/Home/rules', 'statica', '', 5),
(9, 'Prodotti', 'it/prodotti/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', 0),
(10, 'Varianti', 'it/prodotti/(:any)/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', 0),
(11, 'Account', 'it/account', 1, 'frontend/Account', 'statica', '', 0),
(12, 'Logout', 'it/logout', 1, 'frontend/Account/logout', 'statica', '', 0),
(13, 'Login', 'it/login', 1, 'frontend/Account/login', 'statica', '', 0),
(14, 'Registrati', 'it/registrati', 1, 'frontend/Account/register', 'statica', '', 0),
(15, 'Salva account', 'it/salva_account', 1, 'frontend/Account/salvaDatiProfilo', 'statica', '', 0),
(16, 'Carrello', 'it/carrello', 1, 'frontend/Cart', 'statica', '', 0),
(17, 'Checkout', 'it/checkout', 1, 'frontend/Cart/checkout', 'statica', '', 0),
(18, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 7),
(19, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 1),
(20, 'Shop', 'en/shop', 2, 'frontend/Home/shop', 'dinamica', 'negozio', 2),
(21, 'Shop', 'en/shop/(:any)', 2, 'frontend/Home/shop/$1', 'dinamica', 'negozio', 0),
(22, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 3),
(23, 'Shipping', 'en/shipping', 2, 'frontend/Home/shipping', 'statica', '', 4),
(24, 'Contacts', 'en/contacts', 2, 'frontend/Home/contacts', 'statica', '', 8),
(25, 'Rules', 'en/rules', 2, 'frontend/Home/rules', 'statica', '', 5),
(26, 'Products', 'en/products/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', 0),
(27, 'Variants', 'en/products/(:any)/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', 0),
(28, 'Account', 'en/account', 2, 'frontend/Account', 'statica', '', 0),
(29, 'Logout', 'en/logout', 2, 'frontend/Account/logout', 'statica', '', 0),
(30, 'Login', 'en/login', 2, 'frontend/Account/login', 'statica', '', 0),
(31, 'Register', 'en/register', 2, 'frontend/Account/register', 'statica', '', 0),
(32, 'Save account', 'en/save_account', 2, 'frontend/Account/salvaDatiProfilo', 'statica', '', 0),
(33, 'Shopping cart', 'en/cart', 2, 'frontend/Cart', 'statica', '', 0),
(34, 'Checkout', 'en/checkout', 2, 'frontend/Cart/checkout', 'statica', '', 0),
(35, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 7),
(36, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 6),
(37, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `pc_page_code` varchar(150) NOT NULL,
  `pc_titolo_1` varchar(250) NOT NULL,
  `pc_immagine_1` varchar(250) NOT NULL,
  `pc_testo_body_1` longtext NOT NULL,
  `pc_titolo_2` varchar(250) NOT NULL,
  `pc_immagine_2` varchar(250) NOT NULL,
  `pc_testo_body_2` longtext NOT NULL,
  `pc_testo_bold_1` text NOT NULL,
  `pc_testo_bold_2` text NOT NULL,
  `pc_testo_bold_3` text NOT NULL,
  `pc_top_image` varchar(250) NOT NULL,
  `pc_top_image_mobile` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `pc_page_code`, `pc_titolo_1`, `pc_immagine_1`, `pc_testo_body_1`, `pc_titolo_2`, `pc_immagine_2`, `pc_testo_body_2`, `pc_testo_bold_1`, `pc_testo_bold_2`, `pc_testo_bold_3`, `pc_top_image`, `pc_top_image_mobile`, `id_lingua`) VALUES
(1, 'HOME', 'Home', 'tiracconto3.jpg', 'Ti racconto...<br><br>Una gran parte delle tue paure riguardo la medicina estetica, vengono alimentate dalla società stessa, dove ti fanno vedere orrori o ti fanno credere che la bellezza fisica non conta.<br><br>\r\n    Per me conta (e siamo sinceri, perché vivi in un mondo che è materia), ma conta quando hai in <span class=\"blu-text\"><b>equilibrio la bellezza esterna e quella interiore.</b></span><br>\r\n    Forse non conosci la Medicina Estetica e se la “conosci” (e questo non significa che sai veramente di cosa si tratta), hai paura perché pensi che ti gonfierà, ti trasformerà o diventerai un’altra.<br><br>\r\n    Io <span class=\"orange-text\"><b>ti offro armonia ed equilibrio</b></span>, rimarrai chi sei fisicamente ma ti vedrai e ti vedranno <span class=\"blu-text\"><b>meglio, riposata e fantastica</b></span> e se me lo permetti ti guiderò verso la tua rinnovazione interna.\r\n    <br><span class=\"orange-text\"><b>Rompi le credenze limitanti</b></span>, per quello che riguarda la bellezza esteriore, <span class=\"blu-text\"><b>non avere vergogna</b></span> di sentirti bene dentro della tua propria pelle e cercare di sentirti più bella.<br><br>\r\n    <span class=\"orange-text\"><b>Fai quello che senti</b></span> di fare, <span class=\"blu-text\"><b>ma sentilo con il cuore</b></span> e vivi con piacere l\'<span class=\"orange-text\"><b>armonia esterna e il rinnovamento interno</b></span> che potresti raggiungere se veramente lo vuoi.<br><br><span class=\"blu-text text-24\"><b>Tutto dipende da te!</b></span>', '', '', '', 'Ciao, sono Orlena Zotti, medico estetico, creatrice di <span class=\"orange-text\"><b>\"Sei Essenzialmente Bella\"</b></span>.<br>Aiuto le donne a trovare la stabilità e rafforzare l\'amor proprio attraverso l\'armonia fisica.<br>Amo fornire strumenti alle donne che vogliono un cambiamento e cercano di connettersi con l\'essenza della bellezza.', '<span class=\"text-24\"><b>Cosa è la medicina estetica?</b></span><br>\r\nLa definirei come tutto quello che è alternativo alla chirurgia plastica per armonizzare la bellezza fisica, tramite trattamenti realizzati con procedure poco o per niente invasive.', '“La Medicina Estetica è un viaggio, un percorso, che si realizza passo a passo.<br>\r\nL’obbiettivo è farti sentire più bella facendo credere agli altri che ti sei semplicemente riposata.”', '1950_500.png', '580_700.png', 1),
(2, 'HOME', 'Home', 'tiracconto3.jpg', 'I tell you ...<br><br>A lot of your fears about aesthetic medicine are fueled by the company itself, where they show you horrors or make you believe that physical beauty does not matter.<br/><br/>\r\n    For me it counts (and we are honest, because you live in a world that is matter), but it matters when you are in the <span class=\"blu-text\"><b>balance between outer and inner beauty.</b></span><br>\r\n    Maybe you do not know the Aesthetic Medicine and if you \"know\" (and this does not mean you really know what it is), you\'re afraid because you think it will swell, turn you or you will become another.<br><br>\r\n    <span class=\"orange-text\"><b>I offer you harmony and balance</b></span>, you will be who you are physically but you will see and will see you <span class=\"blu-text\"> better, rested and fantastic</b></span> and if you let me, I will guide you to your inner renewal.\r\n    <br><b> Break the Limiting Beliefs </b> </ span>, as far as the outside beauty is concerned, <span class = \"orange-text\"> <b> do not have shame </b></span> to feel good inside your own skin and try to feel more beautiful. <br> <br>\r\n    <span class=\"orange-text\"><b>Do what you feel</b></span> do <span class=\"blu-text\"><b> but feel it with your heart </b></span> and you will enjoy <span class=\"orange-text\"><b>external harmony and interior renewal</b></span> that you could achieve if you really want it.<br><br><span class=\"blu-text text-24\"><b>Everything depends on you!</b></span>', '', '', '', 'Hello, I\'m Orlena Zotti, aesthetic, creator of <span class=\"orange-text\"><b>\"Sei essenzialmente bella\"</b></span>.<br/>Helping women find stability and strengthen love through physical harmony. <br> I love to provide tools to women who want a change and seek to connect with the essence of beauty.', '<span class=\"text-24\"><b>What is aesthetic medicine?</b></span><br>\r\nI would define it as all that is alternative to plastic surgery to harmonize physical beauty, through treatments carried out with little or no invasive procedures.', '“Aesthetic Medicine is a journey, a journey that takes place step by step.<br/>\r\nThe goal is to make you feel more beautiful by believing others that you simply rested.”', '1950_500.png', '580_700.png', 2),
(3, 'HOME', 'Home', 'tiracconto3.jpg', 'Te cuento...<br><br>Una gran parte de tus miedos a la medicina estética se alimenta de la sociedad en la que vives, que te hace ver los resultados más inapropiados provocados por quien no tiene la capacidad y/o preparacion en esta disciplina o quien te hace creer que la belleza física no cuenta.<br><br>\r\n    Para mí cuenta (y seamos sinceras, vives en un mundo que es materia), pero cuenta cuando se tiene en <span class=\"blu-text\"><b>equilibrio la belleza externa y la belleza interna.</b></span><br>\r\n    Quizás no conoces la Medicina Estética y si la “conoces” (esto no significa que sabes verdaderamente de que se trata), sientes miedo porque piensas que te inflamarás, te transformarás o te convertirás en otra.<br><br>\r\n    Yo <span class=\"orange-text\"><b>te ofrezco armonía y equilibrio</b></span>, serás quien eres fisicamente, pero te verás y te verán <span class=\"blu-text\"><b>mejor, reposada, fresca y fantástica </b></span> y si me lo permites te guiaré a tu renacer interior.\r\n    <br><span class=\"orange-text\"><b>Rompe las creencias limitantes</b></span>, sobre la bellezza externa,<span class=\"blu-text\"><b>no te averguences </b></span> de sentirte bien dentro de tu propia piel y de buscar ayuda para sentirte más bonita.<br><br>\r\n    <span class=\"orange-text\"><b>Haz aquello que sientes</b></span> que es mejor para tí, <span class=\"blu-text\"><b>pero siéntelo con el corazon</b></span> y vive con placer la <span class=\"orange-text\"><b>armonía externa e il renovamiento interno</b></span> que podrías lograr si así lo quieres.<br><br><span class=\"blu-text text-24\"><b>¡Todo depende de tí!</b></span>', '', '', '', 'Hola, soy Orlena Zotti, medico estético, mi negocio se llama <span class=\"orange-text\"><b>\"Sei Essenzialmente Bella\"</b></span>.<br>Yo ayudo a las mujeres a encontrar su estabilidad y reforzar su amor propio a través de la armonía física. Me encanta ofrecer herramientas a las personas que quieren un cambio y buscan conectar con la esencia de la belleza.', '<span class=\"text-24\"><b>¿Qué es la medicina estética para mi?</b></span><br>\r\nLa definiría como un conjunto de tratamientos médicos, realizados con procedimientos poco o para nada invasivos, que son alternativos a la cirugía plástica, logrando así armonizar la belleza física.', '“La Medicina Estética es un viaje, un recorrido, que se realiza paso a paso.\r\nEl objetivo es hacerte sentir más bella haciendole creer a los demás que simplemente te has reposado”', '1950_500.png', '580_700.png', 3),
(4, 'SUDIME', 'Su di me', 'sudime.jpg', '<p align=\"justify\">Qui mi ha portato la solitudine, il sentirmi come un pesce fuor d\'acqua, la sensazione di non appartenere al luogo in cui mi trovavo né alla maniera di fare la medicina estetica come la facevo. Ho pensato che cambiare ambiente poteva far migliorare la situazione, ma questo non è successo.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                            Allora ho iniziato la mia ricerca di tutto ciò che mi avrebbe aiutato a capire cosa volevo veramente, cosa mi rendeva felice e, soprattutto, cosa sono venuta a fare in questo mondo.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Prima di tutto, ho deciso di ampliare le mie competenze come medico estetico ed estendere la conoscenza ad altri orizzonti del benessere globale, così ho intrapreso la lettura di molti libri sull’evoluzione spirituale, ho scoperto il mondo della meditazione, ho frequentato diversi corsi di crescita personale e su come nutrire il mio corpo e la mia anima. Tutto questo si è aggiunto alla mia passione, la Medicina Estetica.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Così mi è sorto il desiderio di applicare alla medicina estetica questo viaggio interiore e ho cominciato ad unificare tutti gli strumenti che ho sperimentato in programmi specifici. Questo mi ha permesso di creare un modo per offrire aiuto alle donne, armonizzando la bellezza esteriore ed equilibrando la bellezza interiore, accorpando due mondi che la società ha venduto come diversi, ma che per me sono inseparabili perché entrambi si nutrono equilibratamente.\r\n                        </p>', 'Mini biografia', '', '<p>\r\n                       Ho conseguito la laurea di Medico Chirurgo in Venezuela (il mio paese di nascita) nel 2005, subito dopo ho conosciuto il mondo della medicina estetica e me ne sono innamorata, tanto che ne ho fatto la mia unica professione.\r\n                    </p>\r\n                    <p>\r\n                        Sono arrivata in Italia nel 2010 e ho seguito la procedura per il riconoscimento della mia laurea in questo paese, che porto nel mio sangue grazie a mio nonno.\r\n                    </p>\r\n                    <p>\r\n                        Per perfezionare le mie conoscenze in questo campo ho seguito un percorso quadriennale di studi conseguendo il Diploma di Medicina Estetica della Fondazione Internazionale Fatebenefratelli a Roma.\r\n                    </p>\r\n                    <p>\r\n                        Ora mi trovo in Sardegna, dove, grazie al mio patrimonio di conoscenze, svolgo il mio lavoro offrendo un concetto di medicina estetica unico ed equilibrato. \r\n                    </p>', '', '', '', '', '', 1),
(5, 'SUDIME', 'About me', 'sudime.jpg', '<br/><p align=\"justify\">Here loneliness has been brought to me, feeling like a fish out of water, feeling not to belong to the place where I was or how to do aesthetic medicine as I did. I thought that changing the environment could improve the situation, but that did not happen.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                            Then I started my search for everything that would help me to understand what I really wanted, what made me happy and, above all, what I came to do in this world.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	First of all, I decided to expand my skills as an aesthetic doctor and extend knowledge to other horizons of global well-being, so I started reading many books about spiritual evolution, I discovered the world of meditation, I attended several courses of personal growth and how to nourish my body and soul. All this has been added to my passion, Aesthetic Medicine.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	So I had the desire to apply this inner journey to aesthetic medicine and I began to unify all the tools I experienced in specific programs. This has allowed me to create a way of offering women\'s help, harmonizing the exterior beauty and balancing the inner beauty, by merging two worlds that the society has sold as different, but which for me are inseparable because they both nourish equilibrally.\r\n                        </p>', 'Mini Biografy', '', '<p>\r\n                       I graduated from the Medical Surgeon in Venezuela (my birth country) in 2005, soon after I met the world of aesthetic medicine and I fell in love with him so much that I did my only job.\r\n                    </p>\r\n                    <p>\r\n                        I arrived in Italy in 2010 and followed the procedure for the recognition of my degree in this country, which I bring in my blood thanks to my grandfather.\r\n                    </p>\r\n                    <p>\r\n                        To refine my knowledge in this field I followed a four-year course of study by completing the Diploma of Aesthetic Medicine of the Fatebenefratelli International Foundation in Rome.\r\n                    </p>\r\n                    <p>\r\n                        Now I am in Sardinia, where, thanks to my knowledge of wealth, I do my job by offering a concept of unique and balanced aesthetic medicine.\r\n                    </p>', '', '', '', '', '', 2),
(6, 'SUDIME', 'Sobre mi', 'sudime.jpg', '<p align=\"justify\"><br>Aquí me trajo la soledad, el sentirme un pez fuera del agua, el sentir que no pertenecía al lugar donde me encontraba, ni a la manera de hacer la medicina estética que hacía, pensé que cambiando de ambiente podría mejorar el como me sentía, pero esto non sucedió.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                            Entonces comencé a buscar lo que fuera que me ayudara para comprender que quería de verdad, que me hacía feliz y sobre todo que vine a hacer en este mundo.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Inicié perfeccionando mis conocimientos como medico estético, leí muchos libros sobre evolución espiritual, conocí el mundo de la meditación, hice muchos cursos de crecimiento personal y de como nutrir mi cuerpo y todo esto se sumó a mi pasión: la medicina estética.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Así me invadieron las ganas de aplicar a la medicina estética este recorrido interno y fue cuando empecé a unificar en programas todas las herramientas que he experimentado.<br><br>Esto me ha permitido crear una forma de ofrecer ayuda a las mujeres, armonizando la belleza externa y equilibrando la belleza interna, acomplando dos mundos que la sociedad a vendido como diferentes pero que para mí son inseparables porque los dos se nutren equilibradamente.\r\n                        </p>', 'Mini biografía', '', '<p>\r\n                      Me gradué de Médico Cirujano en Venezuela (mi país de nacimiento) en el 2005, casi inmediatamente después, conocí el mundo de la medicina estética y me enamoré de ella, tanto que la trasformé en mi única profesión.\r\n                    </p>\r\n                    <p>\r\n                        Legue a Italia en el 2010 y realicé todo el procedimiento para validar mi título en este país, que llevo en mi sangre gracias a mi abuelo.\r\n                    </p>\r\n                    <p>\r\n                        Para perfeccionar mis conocientos en este campo seguí un curso de cuatro años opteniendo un Diploma de Medicina Estetica de la Fondazione Internazionale Fatebenefratelli en Roma.\r\n                    </p>\r\n                    <p>\r\n                      Ahora me encuentro en Cerdeña, donde, gracias a mi patrimonio de conocimientos, desarrollo mi trabajo ofreciendo un concepto de medicina estetica único y equilibrado.\r\n                    </p>\r\n                    <p>\r\n                        Now I am in Sardinia, where, thanks to my knowledge of wealth, I do my job by offering a concept of unique and balanced aesthetic medicine.\r\n                    </p>', '', '', '', '', '', 3),
(7, 'MIOLAVORO', 'Il mio lavoro', 'ilmiolavoro.jpg', '<p align=\"justify\">\r\n                           Integrare la bellezza fisica e la crescita interna è una vera e propria missione, perché in questo risveglio alla consapevolezza che stiamo avendo, \"il nostro corpo è il nostro tempio\", ma attualmente, in questo risveglio, questa frase serve solo se ti prendi cura del tuo corpo attraverso l’alimentazione e l\'attività fisica, in questo modo ti puoi mantenere in equilibrio.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                            Ma come si può includere la bellezza in questo momento di presa di consapevolezza?\r\nPer molti anni la bellezza fisica è stata stigmatizzata perché per il pensiero comune non importa come ti vedi (brutta o carina) o ti senti, se non chi sei e come sei. Per questo quello che io propongo è armonizzare la tua bellezza, ad esempio far diventare le tue rughe meno evidenti e far diventare il tuo volto più riposato e fresco. Non avere vergogna di volerti guardare meglio e di sentirti bene dentro la tua pelle.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Regala al tuo corpo e al tuo volto l\'aria fresca che ha perso nel corso degli anni, ed è solo questo, aria fresca, non è una ricostruzione, non è magia, non è diventare qualcun altro. Non significa smettere di essere chi sei. Ti aiuterò a ritrovare la bellezza che non vedi o che pensi di aver perso. Ti aiuterò ad iniziare a vedere con occhi diversi, con occhi d’amore per te stessa.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	La bellezza che per molte persone si sprigiona da dentro verso fuori per la maggior parte si riflette dall\'esterno verso l\'interno; perché molte volte guardare allo specchio quello che si può ottenere fa risplendere la tua luce interiore con più intensità e ti aiuta a vedere ciò che era nascosto, dal passare degli anni, dalla mancanza d’amore per te stessa,  dalla mancanza di tempo.\r\n                        </p>', '', '', '', 'Ricordati che il tuo corpo è il tuo tempio e tante volte l\'attività fisica e il mangiare sano non è sufficiente, è anche importante per il tuo tempio un soffio d’aria fresca.', '', '', '', '', 1),
(8, 'MIOLAVORO', 'My work', 'ilmiolavoro.jpg', '<br/><p align=\"justify\">\r\n                           Integrating physical beauty and internal growth is a real mission, because in this awakening to the awareness that we are having, \"our body is our temple,\" but in this awakening, this phrase only serves if you take care of your body through nutrition and physical activity, in this way you can keep it in balance.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                            But how can beauty be included in this awareness-raising moment?\r\nFor many years, physical beauty has been stigmatized because for common thinking it does not matter how you see (ugly or pretty) or you feel, if you are not who you are and how are you. For this, what I propose is to harmonize your beauty, for example to make your wrinkles less noticeable and make your face more rested and cool. Do not be ashamed of wanting to look better and feel good inside your skin.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Give your body and your face fresh air that has lost over the years, and that\'s just this, fresh air, it\'s not a reconstruction, it\'s no magic, it\'s not becoming someone else. It does not mean to stop being who you are. I will help you find the beauty you do not see or think you\'ve lost. I will help you begin to see with different eyes, with eyes of love for yourself.\r\n                        </p>\r\n						<br/>\r\n                        <p align=\"justify\">\r\n                        	The beauty that for many people emanates from the inside out is mostly reflected from the outside inward; because many times you look in the mirror what you can get brings your inner light shining with more intensity and helps you to see what was hidden from years of lack of love for yourself by the lack of time.\r\n                        </p>', '', '', '', 'Remember that your body is your temple and many times physical activity and healthy eating is not enough, it is also important for your temple to breathe fresh air.', '', '', '', '', 2),
(9, 'MIOLAVORO', 'Mi trabajo', 'ilmiolavoro.jpg', '<p align=\"justify\">\r\n                            Es una verdadera misión  integrar belleza física y crecimiento interno,  porque en este despertar de consciencia que estamos teniendo,  \"nuestro cuerpo es nuestro templo\". Pero actualmente en este despertar este frase sirve solo si tú cuidas tu cuerpo desde la alimentación y la actividad física, de esta manera te mantendrás en equilibrio contigo misma.\r\n                        </p>\r\n                        <p align=\"justify\"> \r\nPero cómo uno puede incluir la bellezza en este tiempo de tomar consciencia?  \r\nPor muchos años la belleza física ha sido atacada y ha sido tomada como superficial, porque no importa el como te veas (fea o bonita) o te sientas,  si no quien eres y como eres. Por eso lo que yo propongo, por ejemplo, es armonizar tu belleza,  que las arrugas sean menos evidentes y que tu rostro se vea más reposado y fresco. No te averguences por querer verte mejor y sentirte bien dentro de tu propia piel.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Regala a tu cuerpo y a tu rostro ese aire fresco que ha perdido con los años y solo es eso,  aire fresco,  no es una reconstrucción,  no es magia,  no es convertirte en otra persona. No es que dejes de ser quien eres. Yo te ayudo a encontrar esa belleza que no ves o esa belleza que piensas haber perdido. Te ayudo a que te empieces a ver con otros ojos,  con ojos de amor por ti misma.\r\n                        </p>\r\n                        <p align=\"justify\">\r\n                        	Lo que a muchos le funciona de adentro hacia fuera a otros les funciona de manera increíble de afuera hacia adentro; porque muchas veces viendo al espejo lo que se puede obtener eso hace que la luz interna brille  con más intensidad y que empieces a ver lo que estaba escondido,  por el desgaste de los años,  por la falta de amor a tí misma,  por falta de tiempo.\r\n                        </p>', '', '', '', 'Recuerda que tu cuerpo es tu templo y que muchas veces la actividad física y la alimentación más saludable no basta,  también es importante para tu templo el aire fresco.', '', '', '', '', 3),
(10, 'PRIVACY', 'Privacy', '', '<p align=\"justify\">Ai sensi del decreto legislativo 30 giugno 2003 n. 196, Sei essenzialmente bella, in qualità di Titolare autonomo del trattamento, è tenuta a fornire alcune informazioni riguardanti l\'utilizzo dei dati personali da Lei forniti, ovvero altrimenti acquisiti nell\'ambito della rispettiva attività.\r\nFonte dei dati personali.\r\nI dati personali cui ci si riferisce sono raccolti direttamente da questo sito web. Tutti questi dati verranno trattati nel rispetto della citata legge e degli obblighi di riservatezza cui si è sempre ispirata l\'attività dello studio.</p>\r\n                    <p align=\"justify\">\r\n                       <b>Verranno raccolti i seguenti dati:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                        <li>indirizzo IP dell\'utente;</li>\r\n                        <li>indirizzo e-mail personale;</li>\r\n                        <li>indirizzo URL di provenienza;</li>\r\n                        <li>numero di telefono</li>\r\n                        <li>nome</li>\r\n                        <li>cognome</li>\r\n                       </ul>\r\n                    </p>\r\n                    <p align=\"left\">\r\n                       <b>Finalità del trattamento cui sono destinati i dati I dati personali da Lei forniti:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                       	<li>completamento e supporto dell\'accesso;</li>\r\n                        <li>per eseguire obblighi di legge;</li>\r\n                        <li>per esigenze di tipo operativo e gestionale</li>\r\n                        <li>per inziative e comunicazioni promozionali riservate agli iscritti al sito</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align=\"justify\">Modalità di trattamento dei dati\r\nIn relazione alle indicate finalità,il trattamento dei dati personali avviene mediante strumenti manuali, informatici e telematici con logiche strettamente correlate alle finalità stesse e, comunque, in modo da garantire la sicurezza e la riservatezza dei dati stessi. Il trattamento dei dati avverrà mediante strumenti idonei a garantirne la sicurezza e la riservatezza e potrà essere effettuato anche attraverso strumenti automatizzati atti a memorizzare, gestire e trasmettere i dati stessi. I dati forniti non verranno ceduti e/o rivenduti a soggetti terzi.<br><br>\r\nDiritti di cui all\'art.7 La informiamo, infine, che l\'art. 7 del decreto legislativo 196/2003 conferisce agli interessati l\'esercizio di specifici diritti. In particolare, l\'interessato può ottenere dal Titolare la conferma dell\'esistenza o no di propri dati personali e che tali dati vengano messi a sua disposizione in forma intelligibile. L\'interessato può altresì chiedere di conoscere l\'origine dei dati personali, la finalità e modalità del trattamento; la logica applicata in caso di trattamento effettuato con l\'ausilio di strumenti elettronici; gli estremi identificativi del titolare e del responsabile; di ottenere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge nonché l\'aggiornamento, la rettificazione o, se vi è interesse, l\'integrazione dei dati; di opporsi, per motivi legittimi, al trattamento di dati che lo riguardano anche ai fini di invio di materiale pubblicitario o di vendita diretta.<br><br>I diritti in oggetto potranno essere esercitati, anche per il tramite di un incaricato, mediante richiesta rivolta al responsabile nominato con lettera. Nell\'esercizio dei diritti, l\'interessato può conferire per iscritto, delega o procura a persone fisiche, enti, associazioni od organismi. L\'interessato può, altresì, farsi assistere da una persona di fiducia. Ulteriori informazioni potranno essere rischieste, per iscritto, presso la sede dello studio medico, sita in Roma, Via delle Belle Arti, 7 - 00196\r\n                    </p>\r\n					<p align=\"justify\">\r\n					<b>Informazioni sui cookie</b><br/>\r\n					I cookie sono stringhe di testo (piccole porzioni di informazioni) che vengono memorizzati su computer, tablet, smartphone, notebook, da riutilizzare nel corso della medesima visita (cookie di sessione) o per essere ritrasmessi agli stessi siti in una visita successiva.<br/><br/>Ai sensi dell\'art. 13 del D.l.vo n. 196/2003 questo sito utilizza unicamente cookie tecnici od a questi assimilati, che non richiedono un preventivo consenso; si tratta di cookie necessari, indispensabili per il corretto funzionamento del sito, servono per effettuare la navigazione.\r\n					</p>\r\n', '', '', '', '', '', '', '', '', 1),
(11, 'PRIVACY', 'Privacy', '', '<p align=\"justify\">Pursuant to Legislative Decree no. 196, You are essentially beautiful, as a sole proprietor of the treatment, you are required to provide some information regarding the use of the personal data you provide, or otherwise acquired in the course of your business. Source of personal data.\r\nThe personal data we refer to is collected directly from this website. All of these data will be processed in compliance with the said law and the confidentiality requirements that have always been inspired by the activity of the study.</p>\r\n					<p align=\"justify\">\r\n                       <b>The following data will be collected:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                        <li>user IP address</li>\r\n                        <li>ersonal email address</li>\r\n                        <li>source URL</li>\r\n                        <li>phone number</li>\r\n                        <li>name</li>\r\n                        <li>surname</li>\r\n                       </ul>\r\n                    </p>\r\n					\r\n					<p align=\"left\">\r\n                       <b>Purpose of the treatment to which the data is intended The personal data you provide:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                       	<li>completion and Access Support;</li>\r\n                        <li>to enforce legal obligations;</li>\r\n                        <li>for operational and management needs</li>\r\n                        <li>for promotions and promotional communications reserved for members of the site</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align=\"justify\">How data is processed in relation to the aforementioned purposes, the processing of personal data is done through manual, computer and telematic tools with logic strictly related to the purposes themselves and, in any case, in order to guarantee the security and confidentiality of the data themselves. Data processing will be carried out by means of appropriate tools to ensure its security and confidentiality, and it can also be carried out by means of automated tools for storing, managing and transmitting the data itself. The provided data will not be transferred and / or resold to third parties\r\nThe rights referred to in art. 7 We inform you, finally, that art. 7 of Legislative Decree 196/2003 grants the persons concerned the exercise of specific rights. In particular, the data subject may obtain from the Registrar the confirmation of the existence or not of his / her personal data and that such data shall be made available to him in an intelligible form. The person concerned may also ask to know the origin of personal data, the purpose and the manner of processing; the logic applied in the case of processing carried out with the aid of electronic instruments; the identification details of the holder and the manager; to obtain the cancellation, transformation into anonymous form or the blocking of the data processed in violation of the law as well as the updating, correction or, if there is interest, the integration of the data; to oppose, for legitimate reasons, the processing of data concerning him also for the purpose of sending advertising material or direct sale. <br> <br> The rights in question may also be exercised, through an agent as well request addressed to the person named by letter. In the exercise of rights, the person concerned may confer in writing, delegate or procure to natural persons, bodies, associations or bodies. The person concerned can also be assisted by a trusted person. Further information can be obtained, in writing, at the headquarters of the medical office, located in Rome, Via delle Belle Arti, 7 - 00196\r\n                    </p>\r\n					<p align=\"justify\">\r\n					<b>About cookies</b><br/>\r\n					Cookies are text strings (small portions of information) that are stored on computers, tablets, smartphones, notebooks, reusable during the same visit (session cookies) or retransmitted to the same sites on a subsequent.<br/><br/>Visit. art. 13 of Legislative Decree no. 196/2003 this site uses only technical cookies or similar assimilates, which do not require prior consent; these are the necessary cookies, essential for the proper functioning of the site, they serve to make navigation.\r\n					</p>', '', '', '', '', '', '', '', '', 2),
(12, 'PRIVACY', 'Politica de privacidad', '', '<p align=\"justify\">De conformidad con el Decreto Legislativo no. 196, Sei essenzialmente Bella,  es autorizado por el usuario al uso de la información personal que proporcione, dentro de la actividad. Fuente de datos personales. Los datos personales a los que nos referimos se recogen directamente en este sitio web. Todos estos datos serán procesados en cumplimiento de dicha ley y los requisitos de confidencialidad que siempre se han inspirado en la actividad del estudio.</p>\r\n                    <p align=\"justify\">\r\n                       <b>Se recogerán los siguientes datos:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                        <li>dirección IP del usuario;</li>\r\n                        <li>dirección de correo electrónico personal;</li>\r\n                        <li>URL de origen;</li>\r\n                        <li>número de teléfono</li>\r\n                        <li>nombre</li>\r\n                        <li>apellido</li>\r\n                       </ul>\r\n                    </p>\r\n                    <p align=\"left\">\r\n                       <b>Finalidad del tratamiento al que se destinan los datos personales que usted proporciona:</b>\r\n                       <ul class=\"privacy-ul\">\r\n                       	<li>apoyo y complemento del acceso;</li>\r\n                        <li>para hacer cumplir las obligaciones legales;</li>\r\n                        <li>para las necesidades operacionales y de gestión;</li>\r\n                        <li>para promociones y comunicaciones promocionales reservadas a los miembros del sitio;</li>\r\n					  </ul>\r\n                    </p>\r\n                    <p align=\"justify\">MMétodos de procesamiento de datos en relación con las finalidades indicadas, el tratamiento de datos personales son tratados con herramientas manuales, equipo y datos con lógica estrictamente relacionados con los mismos fines y, de todos modos, a fin de garantizar la seguridad y confidencialidad de los datos. Los datos se procesan utilizando medios adecuados para garantizar la seguridad y la confidencialidad y se pueden realizar usando herramientas automatizadas para almacenar, gestionar y transmitir los datos. Los datos proporcionados no serán cedidos y / o revendidos a terceros.<br><br>\r\nLos derechos a que se refiere el artículo 7. Le informamos, que el art. 7 del Decreto Legislativo 196/2003 otorga a los interesados el ejercicio de derechos específicos. En particular, el interesado podrá obtener del registrador la confirmación de la existencia o no de sus datos personales y que dichos datos se pondrán a su disposición de forma inteligible. El interesado también puede solicitar conocer el origen de los datos personales, la finalidad y la forma de tratamiento; la lógica aplicada en el caso de la transformación realizada con ayuda de instrumentos electrónicos; los datos de identificación del titular y del administrador; para obtener la cancelación, la transformación en forma anónima o el bloqueo de los datos tratados en violación de la ley y la actualización, corrección o, si está interesado, la integración de los datos; para oponerse, por motivos legítimos, al tratamiento de los datos que le conciernen también con el fin de enviar material publicitario o ventas directas.<br><br>Los derechos en cuestión también podrán ejercerse, incluso a través de una persona encargada, mediante una solicitud dirigida a la persona designada por carta. En el ejercicio de los derechos, la persona interesada podrá conferir por escrito, delegar o procurar a personas físicas, organismos, asociaciones u organismos.\r\n                    </p>\r\n					<p align=\"justify\">\r\n					<b>Acerca de las cookies</b><br/>\r\n					Las cookies son cadenas de texto (pequeñas porciones de información) almacenadas en computadoras, tabletas, smartphones, portátiles, reutilizables durante la misma visita (cookies de sesión) o retransmitidas en los mismos sitios en una visita posterior.<br/><br/>Según el art. 13 del Decreto Legislativo núm. 196/2003 este sitio usa solamente cookies tecnicos o relacionadas a estos que no requieren consentimiento previo; son necesarios para el buen funcionamiento del web, sirven para hacer la navegación.\r\n					</p>', '', '', '', '', '', '', '', '', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE `prodotti` (
  `id_prodotti` int(11) NOT NULL,
  `id_tipo_prodotto` int(11) DEFAULT NULL,
  `codice` varchar(100) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `prezzo` double DEFAULT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) DEFAULT NULL,
  `url_img_grande` varchar(250) DEFAULT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL,
  `id_colori_prodotti` int(11) NOT NULL DEFAULT '0',
  `ordine` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id_prodotti`, `id_tipo_prodotto`, `codice`, `nome`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `id_colori_prodotti`, `ordine`) VALUES
(1, 5, 'house_design_chair', 'Design chair', 45, 0, '5551c-item04.jpg', 'd70bd-th11.jpg', '', 1, 0, 1),
(2, 3, 'clocks_hand_man', 'Hand man clock', 128, 0, '92ea0-item03.jpg', '60325-th14.jpg', '', 1, 0, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_categorie`
--

CREATE TABLE `prodotti_categorie` (
  `id_prodotti_categorie` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_categorie`
--

INSERT INTO `prodotti_categorie` (`id_prodotti_categorie`, `id_prodotto`, `id_categoria`) VALUES
(3, 2, 2),
(4, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_traduzioni`
--

CREATE TABLE `prodotti_traduzioni` (
  `id_prodotti_traduzioni` int(11) NOT NULL,
  `id_prodotti` int(11) NOT NULL,
  `descrizione` text NOT NULL,
  `descrizione_breve` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_traduzioni`
--

INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(1, 1, 'House design chair', 'Design chair', 2),
(2, 1, 'Sedia di design da casa', 'Sedia di design', 1),
(3, 2, 'Hand man clock', 'Hand man clock special material', 2),
(4, 2, 'Orologio da polso uomo materiale speciale', 'Orologio da polso da uomo', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_taglia`
--

CREATE TABLE `prodotto_taglia` (
  `fk_prodotto` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_coupon`
--

CREATE TABLE `stato_coupon` (
  `id_stato_coupon` int(11) NOT NULL,
  `desc_stato_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_coupon`
--

INSERT INTO `stato_coupon` (`id_stato_coupon`, `desc_stato_coupon`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO'),
(3, 'UTILIZZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_ordine`
--

CREATE TABLE `stato_ordine` (
  `id_stato_ordine` int(11) NOT NULL,
  `desc_stato_ordine` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_ordine`
--

INSERT INTO `stato_ordine` (`id_stato_ordine`, `desc_stato_ordine`) VALUES
(1, 'IN LAVORAZIONE'),
(2, 'IN CONSEGNA'),
(3, 'CONSEGNATO'),
(4, 'ANNULLATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_pagamento`
--

CREATE TABLE `stato_pagamento` (
  `id_stato_pagamento` int(11) NOT NULL,
  `desc_stato_pagamento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_pagamento`
--

INSERT INTO `stato_pagamento` (`id_stato_pagamento`, `desc_stato_pagamento`) VALUES
(1, 'ACCETTATO'),
(2, 'SOSPESO'),
(3, 'RIFIUTATO'),
(4, 'ANNULLATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_prodotti`
--

CREATE TABLE `stato_prodotti` (
  `stato_prodotti_id` int(11) NOT NULL,
  `stato_prodotti_desc` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_prodotti`
--

INSERT INTO `stato_prodotti` (`stato_prodotti_id`, `stato_prodotti_desc`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_carrello`
--

CREATE TABLE `storico_carrello` (
  `id_storico_carrello` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `data_storicizzazione` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` int(11) NOT NULL,
  `tipo_prodotto` varchar(250) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `prezzo` double NOT NULL DEFAULT '0',
  `prezzo_scontato` double NOT NULL DEFAULT '0',
  `url_immagine` varchar(250) NOT NULL,
  `colore_prodotto_id` int(11) DEFAULT NULL,
  `colore_prodotto` varchar(150) DEFAULT NULL,
  `colore_prodotto_codice` varchar(150) DEFAULT NULL,
  `is_variante` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_clienti`
--

CREATE TABLE `storico_clienti` (
  `id_storico_clienti` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `indirizzo_fatturazione` varchar(250) DEFAULT NULL,
  `indirizzo_spedizione` varchar(250) NOT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `taglie`
--

CREATE TABLE `taglie` (
  `id_taglia` int(11) NOT NULL,
  `codice` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `descrizione` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `separatore` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taglie`
--

INSERT INTO `taglie` (`id_taglia`, `codice`, `descrizione`, `separatore`) VALUES
(1, 'XS', 'Extra Small', '-'),
(2, 'S', 'Small', '-'),
(3, 'M', 'Medium', '-'),
(4, 'L', 'Large', '-'),
(5, 'XL', 'Extra Large', '-'),
(6, '2XL', '2 Extra Large', '-'),
(7, 'One Size', 'Taglia unica', '-');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags`
--

CREATE TABLE `tags` (
  `id_tag` int(11) NOT NULL,
  `nome_tag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags`
--

INSERT INTO `tags` (`id_tag`, `nome_tag`) VALUES
(1, 'Design'),
(2, 'Estate'),
(3, 'Elegante'),
(4, 'Manica corta'),
(5, 'Smanicato');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags_prodotti`
--

CREATE TABLE `tags_prodotti` (
  `id_tags_prodotti` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags_prodotti`
--

INSERT INTO `tags_prodotti` (`id_tags_prodotti`, `id_tag`, `id_prodotto`) VALUES
(1, 1, 1),
(2, 3, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_coupon`
--

CREATE TABLE `tipo_coupon` (
  `id_tipo_coupon` int(11) NOT NULL,
  `desc_tipo_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_coupon`
--

INSERT INTO `tipo_coupon` (`id_tipo_coupon`, `desc_tipo_coupon`) VALUES
(1, 'Sconto sul carrello utilizzo singolo'),
(2, 'Sconto % sul carrello utilizzo singolo'),
(3, 'Sconto sul carrello utilizzo multiplo'),
(4, 'Sconto % sul carrello utilizzo multiplo');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_pagamento`
--

CREATE TABLE `tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL,
  `desc_tipo_pagamento` varchar(100) NOT NULL,
  `icon_tipo_pagamento` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_pagamento`
--

INSERT INTO `tipo_pagamento` (`id_tipo_pagamento`, `desc_tipo_pagamento`, `icon_tipo_pagamento`) VALUES
(1, 'Paypal', 'fa-paypal'),
(2, 'Stripe', 'fa-cc-stripe');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_prodotto`
--

CREATE TABLE `tipo_prodotto` (
  `id_tipo_prodotto` int(11) NOT NULL,
  `descrizione_tipo_prodotto` varchar(30) NOT NULL,
  `css_class` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_prodotto`
--

INSERT INTO `tipo_prodotto` (`id_tipo_prodotto`, `descrizione_tipo_prodotto`, `css_class`) VALUES
(1, 'LABEL_TP_SALE', 'text-danger'),
(2, 'LABEL_TP_BESTSELLER', 'text-warning'),
(3, 'LABEL_TP_TOPRATED', 'top-rated'),
(4, 'LABEL_TP_STANDARD', ''),
(5, 'LABEL_TP_NEW', 'text-success');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template`
--

CREATE TABLE `tipo_template` (
  `id_tipo_template` int(11) NOT NULL,
  `desc_tipo_template` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template`
--

INSERT INTO `tipo_template` (`id_tipo_template`, `desc_tipo_template`) VALUES
(1, 'CONTATTO'),
(2, 'NEWSLETTER'),
(3, 'CUSTOM');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1516054947, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(2, '127.0.0.1', 'info@machlo.com', '$2y$08$VYAfiskG1BgPgTUswBIoaejDT6w6OCenxk9duml7x/hP6ArsaAdb2', NULL, 'info@machlo.com', NULL, NULL, NULL, NULL, 1505121761, 1509533886, 1, 'Stefania', 'Laconi', 'machlo', '060606'),
(5, '93.34.88.220', 'maurizio.maui@gmail.com', '$2y$08$IJj7OXsyviRMup4oDOUIIehrno/frnZSVfmRrOJjPC7yiZBHRKuUy', NULL, 'maurizio.maui@gmail.com', NULL, NULL, NULL, NULL, 1507237096, 1507237125, 1, 'Maurizio', 'Custodi', '0', '0'),
(9, '::1', 'posta@buongiornoamore.it', '$2y$08$GVu.Q9xhp338.ut.XC5MpexrCzBnucEetu.oMzzjTqHKtazC7d3Tm', NULL, 'posta@buongiornoamore.it', NULL, NULL, NULL, NULL, 1515403341, 1515410201, 1, 'Buongiorno', 'Amore', '0', '0');

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(6, 5, 2),
(10, 9, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `variante_taglia`
--

CREATE TABLE `variante_taglia` (
  `fk_variante` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `varianti_prodotti`
--

CREATE TABLE `varianti_prodotti` (
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_colore` int(11) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `prezzo` double NOT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) NOT NULL,
  `url_img_grande` varchar(250) NOT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`id_carrello`);

--
-- Indici per le tabelle `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Indici per le tabelle `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  ADD PRIMARY KEY (`id_categoria_gallery`);

--
-- Indici per le tabelle `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  ADD PRIMARY KEY (`id_categorie_gallery_traduzioni`);

--
-- Indici per le tabelle `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indici per le tabelle `colori_classi`
--
ALTER TABLE `colori_classi`
  ADD PRIMARY KEY (`id_colore_classe`);

--
-- Indici per le tabelle `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  ADD PRIMARY KEY (`id_colori_prodotti`);

--
-- Indici per le tabelle `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indici per le tabelle `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indici per le tabelle `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id_coupon`);

--
-- Indici per le tabelle `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indici per le tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`id_ig`);

--
-- Indici per le tabelle `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  ADD PRIMARY KEY (`id_immagini_gallery_traduzioni`);

--
-- Indici per le tabelle `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  ADD PRIMARY KEY (`id_indirizzo_fatturazione`);

--
-- Indici per le tabelle `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  ADD PRIMARY KEY (`id_indirizzo_spedizione`),
  ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indici per le tabelle `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indici per le tabelle `lingue_labels`
--
ALTER TABLE `lingue_labels`
  ADD PRIMARY KEY (`id_lingue_labels`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id_ordine`),
  ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indici per le tabelle `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indici per le tabelle `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`id_prodotti`),
  ADD KEY `id_tipo_prodotto` (`id_tipo_prodotto`);

--
-- Indici per le tabelle `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  ADD PRIMARY KEY (`id_prodotti_categorie`);

--
-- Indici per le tabelle `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  ADD PRIMARY KEY (`id_prodotti_traduzioni`);

--
-- Indici per le tabelle `prodotto_taglia`
--
ALTER TABLE `prodotto_taglia`
  ADD PRIMARY KEY (`fk_prodotto`,`fk_taglia`);

--
-- Indici per le tabelle `stato_coupon`
--
ALTER TABLE `stato_coupon`
  ADD PRIMARY KEY (`id_stato_coupon`);

--
-- Indici per le tabelle `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indici per le tabelle `stato_ordine`
--
ALTER TABLE `stato_ordine`
  ADD PRIMARY KEY (`id_stato_ordine`);

--
-- Indici per le tabelle `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  ADD PRIMARY KEY (`id_stato_pagamento`);

--
-- Indici per le tabelle `stato_prodotti`
--
ALTER TABLE `stato_prodotti`
  ADD PRIMARY KEY (`stato_prodotti_id`);

--
-- Indici per le tabelle `storico_carrello`
--
ALTER TABLE `storico_carrello`
  ADD PRIMARY KEY (`id_storico_carrello`);

--
-- Indici per le tabelle `storico_clienti`
--
ALTER TABLE `storico_clienti`
  ADD PRIMARY KEY (`id_storico_clienti`);

--
-- Indici per le tabelle `taglie`
--
ALTER TABLE `taglie`
  ADD PRIMARY KEY (`id_taglia`);

--
-- Indici per le tabelle `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indici per le tabelle `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  ADD PRIMARY KEY (`id_tags_prodotti`);

--
-- Indici per le tabelle `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  ADD PRIMARY KEY (`id_tipo_coupon`);

--
-- Indici per le tabelle `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  ADD PRIMARY KEY (`id_tipo_pagamento`);

--
-- Indici per le tabelle `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  ADD PRIMARY KEY (`id_tipo_prodotto`);

--
-- Indici per le tabelle `tipo_template`
--
ALTER TABLE `tipo_template`
  ADD PRIMARY KEY (`id_tipo_template`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indici per le tabelle `variante_taglia`
--
ALTER TABLE `variante_taglia`
  ADD PRIMARY KEY (`fk_variante`,`fk_taglia`);

--
-- Indici per le tabelle `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  ADD PRIMARY KEY (`id_variante`),
  ADD KEY `fk_prodotti` (`id_prodotto`),
  ADD KEY `fk_colore` (`id_colore`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `id_carrello` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  MODIFY `id_categoria_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  MODIFY `id_categorie_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `clienti`
--
ALTER TABLE `clienti`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `colori_classi`
--
ALTER TABLE `colori_classi`
  MODIFY `id_colore_classe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  MODIFY `id_colori_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT per la tabella `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la tabella `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id_coupon` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `id_ig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  MODIFY `id_immagini_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la tabella `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  MODIFY `id_indirizzo_fatturazione` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  MODIFY `id_indirizzo_spedizione` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `lingue_labels`
--
ALTER TABLE `lingue_labels`
  MODIFY `id_lingue_labels` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id_ordine` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT per la tabella `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `id_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  MODIFY `id_prodotti_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  MODIFY `id_prodotti_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `stato_coupon`
--
ALTER TABLE `stato_coupon`
  MODIFY `id_stato_coupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `stato_ordine`
--
ALTER TABLE `stato_ordine`
  MODIFY `id_stato_ordine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  MODIFY `id_stato_pagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `storico_carrello`
--
ALTER TABLE `storico_carrello`
  MODIFY `id_storico_carrello` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `storico_clienti`
--
ALTER TABLE `storico_clienti`
  MODIFY `id_storico_clienti` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `taglie`
--
ALTER TABLE `taglie`
  MODIFY `id_taglia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la tabella `tags`
--
ALTER TABLE `tags`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  MODIFY `id_tags_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  MODIFY `id_tipo_coupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la tabella `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  MODIFY `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  MODIFY `id_tipo_prodotto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `tipo_template`
--
ALTER TABLE `tipo_template`
  MODIFY `id_tipo_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  MODIFY `id_variante` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  ADD CONSTRAINT `fk_tipo_prodotto` FOREIGN KEY (`id_tipo_prodotto`) REFERENCES `tipo_prodotto` (`id_tipo_prodotto`);

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
