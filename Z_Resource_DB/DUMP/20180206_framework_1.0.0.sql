-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2018 alle 17:30
-- Versione del server: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE IF NOT EXISTS `carrello` (
  `id_carrello` int(11) NOT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_sessione_utente` varchar(250) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_creazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `taglia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL,
  `url_categorie` varchar(25) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `immagine` varchar(150) NOT NULL,
  `label_color_class` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `url_categorie`, `nome`, `descrizione`, `immagine`, `label_color_class`, `stato`, `ordine`) VALUES
(1, 'house', 'House', 'House products', 'c82f6-th11.jpg', 3, 1, 1),
(2, 'clocks', 'Clocks', 'Clocks types', 'd537b-th14.jpg', 1, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery`
--

CREATE TABLE IF NOT EXISTS `categorie_gallery` (
  `id_categoria_gallery` int(11) NOT NULL,
  `nome_categoria_gallery` varchar(250) NOT NULL,
  `stato_categoria_gallery` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery`
--

INSERT INTO `categorie_gallery` (`id_categoria_gallery`, `nome_categoria_gallery`, `stato_categoria_gallery`) VALUES
(1, 'Design', 1),
(2, 'Clothes', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery_traduzioni`
--

CREATE TABLE IF NOT EXISTS `categorie_gallery_traduzioni` (
  `id_categorie_gallery_traduzioni` int(11) NOT NULL,
  `id_categoria_gallery` int(11) NOT NULL,
  `descrizione_categoria_gallery` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery_traduzioni`
--

INSERT INTO `categorie_gallery_traduzioni` (`id_categorie_gallery_traduzioni`, `id_categoria_gallery`, `descrizione_categoria_gallery`, `id_lingua`) VALUES
(1, 1, 'Design', 2),
(2, 1, 'Design', 1),
(3, 2, 'Clothes', 2),
(4, 2, 'Abbigliamento', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE IF NOT EXISTS `clienti` (
  `id_cliente` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `punti` bigint(20) NOT NULL DEFAULT '0',
  `id_lingua` int(11) NOT NULL DEFAULT '1',
  `id_indirizzo_fatturazione` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`id_cliente`, `user_id`, `nome`, `cognome`, `email`, `telefono`, `partita_iva`, `codice_fiscale`, `newsletter`, `punti`, `id_lingua`, `id_indirizzo_fatturazione`) VALUES
(4, 9, 'Buongiorno', 'Amore', 'posta@buongiornoamore.it', '4242342342', '', '', 0, 11, 1, 2),
(5, 0, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', '3207753626', '', '', 1, 0, 2, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_classi`
--

CREATE TABLE IF NOT EXISTS `colori_classi` (
  `id_colore_classe` int(11) NOT NULL,
  `colore_classe` varchar(100) NOT NULL,
  `colore_classe_nome` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_classi`
--

INSERT INTO `colori_classi` (`id_colore_classe`, `colore_classe`, `colore_classe_nome`) VALUES
(1, 'text-danger', 'Rosso'),
(2, 'text-success', 'Verde'),
(3, 'text-warning', 'Arancio');

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_prodotti`
--

CREATE TABLE IF NOT EXISTS `colori_prodotti` (
  `id_colori_prodotti` int(11) NOT NULL,
  `nome_colore` varchar(50) NOT NULL,
  `codice_colore` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_prodotti`
--

INSERT INTO `colori_prodotti` (`id_colori_prodotti`, `nome_colore`, `codice_colore`) VALUES
(1, 'Black', '000000'),
(2, 'White', 'ffffff'),
(3, 'Aqua', '43a9d1'),
(4, 'Berry', 'd2528f'),
(5, 'Blue', '6680b3'),
(6, 'Coral', 'ce474d'),
(7, 'Creamy', 'e5ded8'),
(8, 'Navy', '26273b'),
(9, 'Purple', '523756'),
(10, 'Pink', 'eba3b9'),
(11, 'Raspberry', 'a91671'),
(12, 'Jade', '9cc3c0'),
(13, 'Red', 'ae2f38'),
(14, 'Yellow', 'f3dc5d'),
(15, 'Green', '357249'),
(16, 'Darkgrey', '45413e'),
(17, 'Hotpink', 'db2c77'),
(18, 'Royal', '3e609d'),
(19, 'Strawberry', 'a11927'),
(20, 'Silver', 'c6c6c6'),
(21, 'Darkgreen', '35734a'),
(22, 'Grey', '474340');

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE IF NOT EXISTS `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.framework.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Framework', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit18.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'sei@seiessenzialmentebella.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'oz17@Sei', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.118', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'seiessen', 'Utente del DB'),
(8, 'DB_PASSWORD', 'sei17@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'seiessen_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Seiessenzialmentebella', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'sei@seiessenzialmentebella.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', '&copy; 2018 Framework', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3923576272', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Sardegna', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/dottoressaorlenazotti/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/dottoressaorlenazotti/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', 'UA-108374307-1', 'UID Google Analitycs'),
(22, 'PERC_PUNTI_RETURN', '10', 'Percentuale di ritorno punti cliente'),
(23, 'COMNINGSOON_LOGO', 'logo.png', 'File logo del COMING SOON'),
(24, 'COMNINGSOON_BACK_IMAGE', 'default.jpg', 'File immagine di sfondo Coming soon'),
(25, 'COMNINGSOON_BTN_COLOR', 'EE2D20', 'Colore del pulsante Coming soon'),
(26, 'PAYPAL_EMAIL', '', 'Email di Paypal per i pagamenti'),
(27, 'PAYPAL_ENV', '', 'Environment di paypal [sandbox]'),
(28, 'PAYPAL_SANDBOX_CLIENT_ID', '', 'Cliend ID sandbox di Paypal'),
(29, 'PAYPAL_LIVE_CLIENT_ID', '', 'Cliend ID sandbox di Paypal LIVE'),
(30, 'STRIPE_PK', '', 'Stipe Public Key'),
(31, 'STRIPE_SK', '', 'Stripe Secret Key');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE IF NOT EXISTS `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(2, 'Roberto', 'roberto.rossi77@gmail.com', '0', 'Ciao volevo sapere quanto costa una spedizione espressa e se è possibile.\r\nGrazie', '2018-01-08 10:14:04', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE IF NOT EXISTS `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(6, 'roberto.rossi77@gmail.com', '2018-01-07 23:00:00', 1, '2018-01-08 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `id_coupon` int(11) NOT NULL,
  `data_scadenza_coupon` datetime DEFAULT NULL,
  `importo_coupon` double NOT NULL,
  `percentuale_coupon` int(5) NOT NULL,
  `codice_coupon` varchar(250) NOT NULL,
  `stato_coupon` int(11) NOT NULL,
  `tipo_coupon` int(11) NOT NULL,
  `utilizzatore_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `subject_template`, `titolo_template`, `testo_template`, `id_tipo_template`, `lingua_traduzione_id`) VALUES
(1, 'Prova ', 'Newsletter da Ma Chlò', 'La mia prima newsletter', 'Questo è il body della mia priam newsletter <br><br>\r\nGrazie\r\n<a href="buongiornoamore.it">ba</a>', 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE IF NOT EXISTS `immagini_gallery` (
  `id_ig` int(11) NOT NULL,
  `id_categoria_ig` int(11) NOT NULL,
  `nome_ig` varchar(250) NOT NULL,
  `stato_ig` tinyint(4) NOT NULL,
  `immagine_thumb_ig` varchar(250) NOT NULL,
  `immagine_ig` varchar(250) NOT NULL,
  `ordine_ig` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`id_ig`, `id_categoria_ig`, `nome_ig`, `stato_ig`, `immagine_thumb_ig`, `immagine_ig`, `ordine_ig`) VALUES
(1, 2, 'Socks', 1, '09714-th05.jpg', '781e1-05.jpg', 1),
(2, 2, 'T-shirt', 1, '6ac21-th09.jpg', 'd54bb-09.jpg', 2),
(3, 1, 'Phone cover', 1, '4f48a-th07.jpg', '8c24a-07.jpg', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery_traduzioni`
--

CREATE TABLE IF NOT EXISTS `immagini_gallery_traduzioni` (
  `id_immagini_gallery_traduzioni` int(11) NOT NULL,
  `titolo_ig` varchar(250) NOT NULL,
  `testo_ig` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_ig` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery_traduzioni`
--

INSERT INTO `immagini_gallery_traduzioni` (`id_immagini_gallery_traduzioni`, `titolo_ig`, `testo_ig`, `id_lingua`, `id_ig`) VALUES
(1, 'Cover telefono', 'Una cover di design', 1, 3),
(2, 'Phone cover', 'A design phone cover', 2, 3),
(3, 'T-shirt', 'Maglietta estiva', 1, 2),
(4, 'T-shirt', 'Summer t-short', 2, 2),
(5, 'Socks', 'Printed socks', 2, 1),
(6, 'Calzini', 'Calzini stampati', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_fatturazione`
--

CREATE TABLE IF NOT EXISTS `indirizzo_fatturazione` (
  `id_indirizzo_fatturazione` bigint(20) NOT NULL,
  `indirizzo_fatt` varchar(255) DEFAULT NULL,
  `civico_fatt` varchar(15) DEFAULT NULL,
  `cap_fatt` varchar(10) DEFAULT NULL,
  `nazione_fatt` varchar(50) DEFAULT NULL,
  `citta_fatt` varchar(50) DEFAULT NULL,
  `riferimento_fatt` varchar(250) DEFAULT NULL,
  `note_fatt` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_spedizione`
--

CREATE TABLE IF NOT EXISTS `indirizzo_spedizione` (
  `id_indirizzo_spedizione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_sped` varchar(255) DEFAULT NULL,
  `civico_sped` varchar(15) DEFAULT NULL,
  `cap_sped` varchar(10) DEFAULT NULL,
  `nazione_sped` varchar(50) DEFAULT NULL,
  `citta_sped` varchar(50) DEFAULT NULL,
  `riferimento_sped` varchar(250) DEFAULT NULL,
  `note_sped` varchar(100) DEFAULT NULL,
  `flag_predefinito_sped` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE IF NOT EXISTS `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels`
--

CREATE TABLE IF NOT EXISTS `lingue_labels` (
  `id_lingue_labels` int(11) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `prodotti` varchar(50) NOT NULL,
  `fronte` varchar(20) NOT NULL,
  `retro` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels`
--

INSERT INTO `lingue_labels` (`id_lingue_labels`, `id_lingua`, `prodotti`, `fronte`, `retro`) VALUES
(1, 1, 'prodotti', 'FRONTE', 'RETRO'),
(2, 2, 'products', 'FRONT', 'BACK');

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels_lang`
--

CREATE TABLE IF NOT EXISTS `lingue_labels_lang` (
  `id_lingue_labels_lang` int(11) NOT NULL,
  `lingue_labels_lang_label` varchar(250) NOT NULL,
  `lingue_labels_lang_value` text NOT NULL,
  `lingue_labels_lang_desc` varchar(250) NOT NULL,
  `lingue_labels_lang_type` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=371 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels_lang`
--

INSERT INTO `lingue_labels_lang` (`id_lingue_labels_lang`, `lingue_labels_lang_label`, `lingue_labels_lang_value`, `lingue_labels_lang_desc`, `lingue_labels_lang_type`, `id_lingua`) VALUES
(1, 'HOME_CATEGORY_TITLE', 'Scopri i nostri prodotti', '', 'frontend', 1),
(2, 'HOME_SHOW_ALL_PRODUCTS', 'Vedi tutti i prodotti', '', 'frontend', 1),
(3, 'HOME_FEATURE_SHIPPING_TITLE', 'Spedizione in tutto il mondo', '', 'frontend', 1),
(4, 'HOME_FEATURE_SHIPPING_DESC', 'Spedizione gratuita in 7/10 giorni', '', 'frontend', 1),
(5, 'HOME_FEATURE_QUALITY_TITLE', 'Qualità garantita', '', 'frontend', 1),
(6, 'HOME_FEATURE_QUALITY_DESC', 'La garanzia dei nostri prodotti', '', 'frontend', 1),
(7, 'HOME_FEATURE_SUPPORT_TITLE', 'Supporto Online', '', 'frontend', 1),
(8, 'HOME_FEATURE_SUPPORT_DESC', 'Supporto clienti Online', '', 'frontend', 1),
(9, 'HOME_FEATURE_PAYMENTS_TITLE', 'Pagamenti sicuri', '', 'frontend', 1),
(10, 'HOME_FEATURE_PAYMENTS_DESC', 'Pagamenti sicuri con protocollo SSL', '', 'frontend', 1),
(11, 'FOOTER_HELP', 'Hai bisogno di aiuto? Contattaci', '', 'frontend', 1),
(12, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', '', 'frontend', 1),
(13, 'FOOTER_NEWSLTTER_DESC', 'Ricevi le offerte e gli sconti riservati ai clienti.', '', 'frontend', 1),
(14, 'FOOTER_NEWSLTTER_INPUT', 'Inserisci la tua e-mail', '', 'frontend', 1),
(15, 'FOOTER_PAYMENTS_TITLE', 'METODI DI PAGAMENTO', '', 'frontend', 1),
(16, 'FOOTER_PAYMENTS_DESC', 'Con noi puoi pagare con i principali metodi di pagamento.', '', 'frontend', 1),
(17, 'LABEL_FILTER', 'Filtra', '', 'frontend', 1),
(18, 'LABEL_SEARCH', 'Cerca', '', 'frontend', 1),
(19, 'LABEL_ORDER', 'Ordina per', '', 'frontend', 1),
(20, 'LABEL_DEFAULT', 'Default', '', 'frontend', 1),
(21, 'LABEL_SALE', 'Offerta', '', 'frontend', 1),
(22, 'LABEL_NEW', 'Novità', '', 'frontend', 1),
(23, 'LABEL_FEEDBACK', 'Voto medio', '', 'frontend', 1),
(24, 'LABEL_BEST', 'Best seller', '', 'frontend', 1),
(25, 'LABEL_ALPHA', 'Alfabetico', '', 'frontend', 1),
(26, 'LABEL_PRICE', 'Prezzo', '', 'frontend', 1),
(27, 'LABEL_TAGS', 'Tags', '', 'frontend', 1),
(28, 'LABEL_SEE_MORE', 'Vedi altri', '', 'frontend', 1),
(29, 'LABEL_ADD_TO_CART', 'Aggiungi', '', 'frontend', 1),
(30, 'LABEL_ADD_TO_WHISH', 'Aggiungi alla Whishlist', '', 'frontend', 1),
(31, 'LABEL_ALL', 'Tutti', '', 'frontend', 1),
(32, 'LABEL_MY_ACCOUNT', 'Il mio profilo', '', 'frontend', 1),
(33, 'LABEL_USER_ACCOUNT', 'Account utente', '', 'frontend', 1),
(34, 'LABEL_USER_REGISTER', 'Registrazione', '', 'frontend', 1),
(35, 'LABEL_USER_SIGN_UP', 'Iscriviti', '', 'frontend', 1),
(36, 'LABEL_USER_LOGIN', 'Effettua il Login', '', 'frontend', 1),
(37, 'LABEL_UPDATE', 'Salva', '', 'frontend', 1),
(38, 'LABEL_GOT', 'Hai', '', 'frontend', 1),
(39, 'LABEL_POINTS', 'punti', '', 'frontend', 1),
(40, 'LABEL_POINTS_DESC', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', '', 'frontend', 1),
(41, 'LABEL_MANAGE_POINTS', 'Gestisci i tuoi punti', '', 'frontend', 1),
(42, 'LABEL_BACK_SHOP', 'Torna allo Shop', '', 'frontend', 1),
(43, 'LABEL_LOGOUT', 'Logout', '', 'frontend', 1),
(44, 'LABEL_PROFILE', 'Profilo', '', 'frontend', 1),
(45, 'LABEL_ORDERS', 'Ordini', '', 'frontend', 1),
(46, 'LABEL_ADDRESSES', 'Indirizzi di spedizione', '', 'frontend', 1),
(47, 'LABEL_WHISHLIST', 'Whishlist', '', 'frontend', 1),
(48, 'LABEL_NAME', 'Nome', '', 'frontend', 1),
(49, 'LABEL_SURNAME', 'Cognome', '', 'frontend', 1),
(50, 'LABEL_EMAIL', 'Email', '', 'frontend', 1),
(51, 'LABEL_PHONE', 'Telefono', '', 'frontend', 1),
(52, 'LABEL_COUNTRY', 'Stato/Paese', '', 'frontend', 1),
(53, 'LABEL_CITY', 'Città', '', 'frontend', 1),
(54, 'LABEL_ADDRESS', 'Indirizzo', '', 'frontend', 1),
(55, 'LABEL_ADDRESS_REF', 'Riferimento spedizione c/o (es Mario Rossi)', '', 'frontend', 1),
(56, 'LABEL_ADDRESS_REF_FATT', 'Riferimento fatturazione (persona o azienda)', '', 'frontend', 1),
(57, 'LABEL_CIVICO', 'Civico', '', 'frontend', 1),
(58, 'LABEL_POSTAL_CODE', 'CAP', '', 'frontend', 1),
(59, 'LABEL_ADDRESS_NOTES', 'Note indirizzo', '', 'frontend', 1),
(60, 'LABEL_ORDER_NOTES', 'Note ordine', '', 'frontend', 1),
(61, 'LABEL_TOTAL_ORDER', 'Totale ordine', '', 'frontend', 1),
(62, 'LABEL_TOTAL', 'Totale', '', 'frontend', 1),
(63, 'LABEL_QTY', 'Quantità', '', 'frontend', 1),
(64, 'LABEL_SUBTOTAL_ORDER', 'Subtotale', '', 'frontend', 1),
(65, 'LABEL_TOTAL_CART', 'Totale carrello', '', 'frontend', 1),
(66, 'LABEL_TOTAL_ORDER_NOTES', '* Note: il totale include i costi di spedizione', '', 'frontend', 1),
(67, 'LABEL_TOTAL_CART_NOTES', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', '', 'frontend', 1),
(68, 'LABEL_CONFIRM', 'CONFERMA', '', 'frontend', 1),
(69, 'LABEL_REMOVE', 'ELIMINA', '', 'frontend', 1),
(70, 'LABEL_BACK_TO_CART', 'RITORNA AL CARRELLO', '', 'frontend', 1),
(71, 'LABEL_UPDATE_CART', 'AGGIORNA IL CARRELLO', '', 'frontend', 1),
(72, 'LABEL_BACK_TO_SHOP', 'TORNA ALLO SHOP', '', 'frontend', 1),
(73, 'LABEL_CART', 'Carrello', '', 'frontend', 1),
(74, 'LABEL_CART_EMPTY', 'Il tuo carrello è vuoto !', '', 'frontend', 1),
(75, 'LABEL_CHECKOUT', 'Checkout', '', 'frontend', 1),
(76, 'LABEL_PAYMENT_METHOD', 'Modalità di pagamento', '', 'frontend', 1),
(77, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', '', 'frontend', 1),
(78, 'LABEL_PAYMENT_METHOD_CC', 'Carta di credito / Prepagata', '', 'frontend', 1),
(79, 'LABEL_MESSAGE', 'Messaggio', '', 'frontend', 1),
(80, 'LABEL_SEND', 'INVIA', '', 'frontend', 1),
(81, 'LABEL_PRODUCTS', 'prodotti', '', 'frontend', 1),
(82, 'LABEL_CART_INFO_ACTUALLY_1', 'Attualmente ci sono', '', 'frontend', 1),
(83, 'LABEL_CART_INFO_ACTUALLY_2', 'nel carrello', '', 'frontend', 1),
(84, 'LABEL_SIZE', 'Tg', '', 'frontend', 1),
(85, 'LABEL_COLOR', 'Colore', '', 'frontend', 1),
(86, 'LABEL_CATEGORY', 'Categoria', '', 'frontend', 1),
(87, 'LABEL_DESCRIPTION', 'Descrizione', '', 'frontend', 1),
(88, 'LABEL_REVIEWS', 'Commenti', '', 'frontend', 1),
(89, 'LABEL_ALSO_LIKE', 'Ti potrebbero piacere', '', 'frontend', 1),
(90, 'LABEL_REFCODE', 'Rif', '', 'frontend', 1),
(91, 'LABEL_DETAIL', 'Dettaglio', '', 'frontend', 1),
(92, 'LABEL_SHOPPING_CART', 'Carrello', '', 'frontend', 1),
(93, 'LABEL_FRONT', 'FRONTE', '', 'frontend', 1),
(94, 'LABEL_BACK', 'RETRO', '', 'frontend', 1),
(95, 'LABEL_AVAILABILITY', 'Disponibilità', '', 'frontend', 1),
(96, 'LABEL_AVAILABILITY_HIGH', 'Alta', '', 'frontend', 1),
(97, 'LABEL_AVAILABILITY_LOW', 'Bassa', '', 'frontend', 1),
(98, 'LABEL_404_MESSAGE', 'Oops.... la pagina richiesta non esiste !', '', 'frontend', 1),
(99, 'LABEL_404_BTN', 'TORNA ALLA HOME', '', 'frontend', 1),
(100, 'LABEL_ORDER', 'Ordine', '', 'frontend', 1),
(101, 'LABEL_YOUR_ACCOUNT', 'Il tuo account', '', 'frontend', 1),
(102, 'LABEL_USER_ALREADY_REGISTERED', 'Sei già registrato ?', '', 'frontend', 1),
(103, 'LABEL_USER_NOTREGISTERED', 'Altrimenti puoi ordinare compilando i dati', '', 'frontend', 1),
(104, 'LABEL_USER_NOTREGISTERED_POINTS', 'non accumulerai punti e bonus riservati ai clienti registrati', '', 'frontend', 1),
(105, 'LABEL_COUPON', 'COUPON', '', 'frontend', 1),
(106, 'LABEL_COUPON_HAVE', 'Hai un codice sconto?', '', 'frontend', 1),
(107, 'LABEL_COUPON_INSERT', 'Inserisci il codice coupon', '', 'frontend', 1),
(108, 'LABEL_COUPON_APPLY', 'Applica coupon', '', 'frontend', 1),
(109, 'LABEL_DISCOUNT', 'Sconto', '', 'frontend', 1),
(110, 'LABEL_SHIPPING_THIS', 'Spedisci a questo indirizzo', '', 'frontend', 1),
(111, 'LABEL_SHIPPING_OTHER', 'Spedisci ad un altro indirizzo', '', 'frontend', 1),
(112, 'LABEL_NEW_ADDRESS', 'Nuovo indirizzo', '', 'frontend', 1),
(113, 'LABEL_BILLING_ADDRESS', 'Indirizzo di fatturazione', '', 'frontend', 1),
(114, 'LABEL_SHIPPING_ADDRESS', 'Indirizzo di spedizione', '', 'frontend', 1),
(115, 'LABEL_SHIPPING', 'Spedizione', '', 'frontend', 1),
(116, 'LABEL_CHANGE_PASSWORD', 'Cambio Password', '', 'frontend', 1),
(117, 'LABEL_ORDER_DATE', 'Data ordine', '', 'frontend', 1),
(118, 'LABEL_STRIPE_DESC', 'Paga in tutta sicurezza con Stripe', '', 'frontend', 1),
(119, 'MSG_SEARCH_INSERT', 'Inserisci il testo da ricercare', '', 'frontend', 1),
(120, 'MSG_SAVE_NEWSLETTER', 'Iscrivimi alla newsletter', '', 'frontend', 1),
(121, 'MSG_SEND_CONTACT_US', 'Richiesta di contatto inviata con successo!<br/>Grazie.', '', 'frontend', 1),
(122, 'MSG_PAYPAL_NOTETOPAYER', 'L''indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', '', 'frontend', 1),
(123, 'MSG_PAYPAL_CANCEL', 'Riprova ad effettuare il pagamemto !', '', 'frontend', 1),
(124, 'MSG_PAYPAL_ERROR', 'Errore durante il pagamento:', '', 'frontend', 1),
(125, 'MSG_NO_RESULT', 'Nessun risultato per ', '', 'frontend', 1),
(126, 'MSG_NO_RESULT_FILTER', 'Nessun risultato presente per i filtri selezionati.', '', 'frontend', 1),
(127, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', '', 'frontend', 1),
(128, 'MSG_ORDER_SUCCESS', 'inserito con successo ! Grazie.', '', 'frontend', 1),
(129, 'MSG_ORDER_PAYMENT_ERROR', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', '', 'frontend', 1),
(130, 'MSG_SUCCESS_CONTACT', 'Il tuo messaggio è stato inviato correttamente. Grazie !', '', 'frontend', 1),
(131, 'MSG_FAILURE_CONTACT', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', '', 'frontend', 1),
(132, 'MSG_SUCCESS_NEWSLETTER', 'Adesso sei iscritto alla newsletter. Grazie !', '', 'frontend', 1),
(133, 'MSG_UNIQUE_NEWSLETTER', 'L''indirizzo email è già iscritto alla newsletter', '', 'frontend', 1),
(134, 'MSG_UNSUBSCRIBE_DONE', 'La tua email/iscrizione è stata rimossa. Grazie!', '', 'frontend', 1),
(135, 'MSG_UNSUBSCRIBE_NOTFOUND', 'Il contatto richiesto non è attualmente registrato', '', 'frontend', 1),
(136, 'MSG_CART_REMOVED', 'Prodotto rimosso dal carrello', '', 'frontend', 1),
(137, 'MSG_CART_ADDED', 'Prodotto inserito nel carrello', '', 'frontend', 1),
(138, 'MSG_CART_UPDATED', 'Prodotto aggiornato nel carrello', '', 'frontend', 1),
(139, 'MSG_SERVICE_FAILURE', 'Si è verificato un errore. Riprova!', '', 'frontend', 1),
(140, 'MSG_BILLING_ADDRESS_NECESSARY', 'L''indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', '', 'frontend', 1),
(141, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', '', 'frontend', 1),
(142, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'inserisci un nuovo indirizzo per la spedizione', '', 'frontend', 1),
(143, 'MSG_COUPON_INVALID', 'Coupon non valido o scaduto', '', 'frontend', 1),
(144, 'MSG_COUPON_INVALID_OVER', 'Il valore del Coupon inserito è maggiore del carrello!', '', 'frontend', 1),
(145, 'LABEL_TP_SALE', 'Offerta', '', 'frontend', 1),
(146, 'LABEL_TP_BESTSELLER', 'Più venduti', '', 'frontend', 1),
(147, 'LABEL_TP_TOPRATED', 'Più votati', '', 'frontend', 1),
(148, 'LABEL_TP_STANDARD', 'Standard', '', 'frontend', 1),
(149, 'Standard', 'Nuovo', '', 'frontend', 1),
(150, 'SEND_AREYOUSURE_BTN', 'SEI SICURO ?', '', 'frontend', 1),
(151, 'LABEL_UNSUBSCRIBE', 'Cancellati', '', 'frontend', 1),
(152, 'LABEL_MY_ACCOUNT_EMAIL', 'Il mio profilo', '', 'email', 1),
(153, 'LABEL_SEE_EMAIL_ONLINE', 'Vedi email online', 'Vedi email online', 'email', 1),
(154, 'LABEL_EMAIL_SALES_TITLE', 'Controlla le nostre ultime offerte!', 'Ultime offerte', 'email', 1),
(155, 'LABEL_DETAIL_EMAIL', 'Vedi i dettagli', 'Vedi i dettagli', 'email', 1),
(156, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'cancellati', 'Cancellati footer', 'email', 1),
(157, 'TEXT_EMAIL_FOOTER_RESERVED', 'Tutti i diritti riservati', 'Tutti i diritti riservati', 'email', 1),
(158, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'Se non vuoi più ricevere queste email per favore', 'Non ricevere più email', 'email', 1),
(159, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contatto dal sito', 'Contatto dal sito', 'email', 1),
(160, 'LABEL_EMAIL_CONTACT_TITLE', 'Ti ringraziamo per averci contattato!', 'Ti ringraziamo per il contatto', 'email', 1),
(161, 'LABEL_EMAIL_CONTACT_TEXT', 'Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!', 'Comunicazione ricevuta', 'email', 1),
(162, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Iscrizione alla Newsletter', 'Iscrizione alla Newsletter', 'email', 1),
(163, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Grazie per esserti iscritto alla nostra newsletter!', 'Grazie newsletter', 'email', 1),
(164, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.', 'Riceverai gli aggiornamenti', 'email', 1),
(165, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Benvenuto su Ma Chlò', 'Benvenuto su', 'email', 1),
(166, 'LABEL_EMAIL_WELCOME_TITLE', 'Benvenuto su Ma Chlò!', 'Benvenuto su TITOLO', 'email', 1),
(167, 'LABEL_EMAIL_WELCOME_TEXT', 'Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.', 'Grazie per esserti registrato', 'email', 1),
(168, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'Nuovo ordine', 'Nuovo ordine', 'email', 1),
(169, 'LABEL_TITLE_INVOICE', 'Riepilogo ordine', 'Riepilogo ordine', 'email', 1),
(170, 'LABEL_INVOICE_THANKS', 'Ti ringraziamo per il tuo ordine!', 'Ti ringraziamo per il tuo ordine!', 'email', 1),
(171, 'LABEL_INVOICE_THANKS_TEXT', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere USER', 'email', 1),
(172, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di creare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere NO USER', 'email', 1),
(173, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'email', 1),
(174, 'LABEL_ORDER_EMAIL', 'Ordine', 'Ordine', 'email', 1),
(175, 'LABEL_ORDER_DATE_EMAIL', 'Data ordine', 'Data ordine', 'email', 1),
(176, 'LABEL_ADDRESS_NOTES_EMAIL', 'Note indirizzo', 'Note indirizzo', 'email', 1),
(177, 'LABEL_ORDER_NOTES_EMAIL', 'Note ordine', 'Note ordine', 'email', 1),
(178, 'LABEL_DESCRIPTION_EMAIL', 'Descrizione', 'Descrizione', 'email', 1),
(179, 'LABEL_QTY_EMAIL', 'Quantità', 'Quantità', 'email', 1),
(180, 'LABEL_SIZE_EMAIL', 'Taglia', 'Taglia', 'email', 1),
(181, 'LABEL_COLOR_EMAIL', 'Colore', 'Colore', 'email', 1),
(182, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotale', 'Subtotale', 'email', 1),
(183, 'LABEL_COUPON_APPLY_EMAIL', 'Applica coupon', 'Applica coupon', 'email', 1),
(184, 'LABEL_SHIPPING_EMAIL', 'Spedizione', 'Spedizione', 'email', 1),
(185, 'LABEL_TOTAL_EMAIL', 'Totale', 'Totale', 'email', 1),
(186, 'HOME_CATEGORY_TITLE', 'Scopri i nostri prodotti', '', 'frontend', 2),
(187, 'HOME_SHOW_ALL_PRODUCTS', 'Vedi tutti i prodotti', '', 'frontend', 2),
(188, 'HOME_FEATURE_SHIPPING_TITLE', 'Spedizione in tutto il mondo', '', 'frontend', 2),
(189, 'HOME_FEATURE_SHIPPING_DESC', 'Spedizione gratuita in 7/10 giorni', '', 'frontend', 2),
(190, 'HOME_FEATURE_QUALITY_TITLE', 'Qualità garantita', '', 'frontend', 2),
(191, 'HOME_FEATURE_QUALITY_DESC', 'La garanzia dei nostri prodotti', '', 'frontend', 2),
(192, 'HOME_FEATURE_SUPPORT_TITLE', 'Supporto Online', '', 'frontend', 2),
(193, 'HOME_FEATURE_SUPPORT_DESC', 'Supporto clienti Online', '', 'frontend', 2),
(194, 'HOME_FEATURE_PAYMENTS_TITLE', 'Pagamenti sicuri', '', 'frontend', 2),
(195, 'HOME_FEATURE_PAYMENTS_DESC', 'Pagamenti sicuri con protocollo SSL', '', 'frontend', 2),
(196, 'FOOTER_HELP', 'Hai bisogno di aiuto? Contattaci', '', 'frontend', 2),
(197, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', '', 'frontend', 2),
(198, 'FOOTER_NEWSLTTER_DESC', 'Ricevi le offerte e gli sconti riservati ai clienti.', '', 'frontend', 2),
(199, 'FOOTER_NEWSLTTER_INPUT', 'Inserisci la tua e-mail', '', 'frontend', 2),
(200, 'FOOTER_PAYMENTS_TITLE', 'METODI DI PAGAMENTO', '', 'frontend', 2),
(201, 'FOOTER_PAYMENTS_DESC', 'Con noi puoi pagare con i principali metodi di pagamento.', '', 'frontend', 2),
(202, 'LABEL_FILTER', 'Filtra', '', 'frontend', 2),
(203, 'LABEL_SEARCH', 'Cerca', '', 'frontend', 2),
(204, 'LABEL_ORDER', 'Ordina per', '', 'frontend', 2),
(205, 'LABEL_DEFAULT', 'Default', '', 'frontend', 2),
(206, 'LABEL_SALE', 'Offerta', '', 'frontend', 2),
(207, 'LABEL_NEW', 'Novità', '', 'frontend', 2),
(208, 'LABEL_FEEDBACK', 'Voto medio', '', 'frontend', 2),
(209, 'LABEL_BEST', 'Best seller', '', 'frontend', 2),
(210, 'LABEL_ALPHA', 'Alfabetico', '', 'frontend', 2),
(211, 'LABEL_PRICE', 'Prezzo', '', 'frontend', 2),
(212, 'LABEL_TAGS', 'Tags', '', 'frontend', 2),
(213, 'LABEL_SEE_MORE', 'Vedi altri', '', 'frontend', 2),
(214, 'LABEL_ADD_TO_CART', 'Aggiungi', '', 'frontend', 2),
(215, 'LABEL_ADD_TO_WHISH', 'Aggiungi alla Whishlist', '', 'frontend', 2),
(216, 'LABEL_ALL', 'Tutti', '', 'frontend', 2),
(217, 'LABEL_MY_ACCOUNT', 'Il mio profilo', '', 'frontend', 2),
(218, 'LABEL_USER_ACCOUNT', 'Account utente', '', 'frontend', 2),
(219, 'LABEL_USER_REGISTER', 'Registrazione', '', 'frontend', 2),
(220, 'LABEL_USER_SIGN_UP', 'Iscriviti', '', 'frontend', 2),
(221, 'LABEL_USER_LOGIN', 'Effettua il Login', '', 'frontend', 2),
(222, 'LABEL_UPDATE', 'Salva', '', 'frontend', 2),
(223, 'LABEL_GOT', 'Hai', '', 'frontend', 2),
(224, 'LABEL_POINTS', 'punti', '', 'frontend', 2),
(225, 'LABEL_POINTS_DESC', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', '', 'frontend', 2),
(226, 'LABEL_MANAGE_POINTS', 'Gestisci i tuoi punti', '', 'frontend', 2),
(227, 'LABEL_BACK_SHOP', 'Torna allo Shop', '', 'frontend', 2),
(228, 'LABEL_LOGOUT', 'Logout', '', 'frontend', 2),
(229, 'LABEL_PROFILE', 'Profilo', '', 'frontend', 2),
(230, 'LABEL_ORDERS', 'Ordini', '', 'frontend', 2),
(231, 'LABEL_ADDRESSES', 'Indirizzi di spedizione', '', 'frontend', 2),
(232, 'LABEL_WHISHLIST', 'Whishlist', '', 'frontend', 2),
(233, 'LABEL_NAME', 'Nome', '', 'frontend', 2),
(234, 'LABEL_SURNAME', 'Cognome', '', 'frontend', 2),
(235, 'LABEL_EMAIL', 'Email', '', 'frontend', 2),
(236, 'LABEL_PHONE', 'Telefono', '', 'frontend', 2),
(237, 'LABEL_COUNTRY', 'Stato/Paese', '', 'frontend', 2),
(238, 'LABEL_CITY', 'Città', '', 'frontend', 2),
(239, 'LABEL_ADDRESS', 'Indirizzo', '', 'frontend', 2),
(240, 'LABEL_ADDRESS_REF', 'Riferimento spedizione c/o (es Mario Rossi)', '', 'frontend', 2),
(241, 'LABEL_ADDRESS_REF_FATT', 'Riferimento fatturazione (persona o azienda)', '', 'frontend', 2),
(242, 'LABEL_CIVICO', 'Civico', '', 'frontend', 2),
(243, 'LABEL_POSTAL_CODE', 'CAP', '', 'frontend', 2),
(244, 'LABEL_ADDRESS_NOTES', 'Note indirizzo', '', 'frontend', 2),
(245, 'LABEL_ORDER_NOTES', 'Note ordine', '', 'frontend', 2),
(246, 'LABEL_TOTAL_ORDER', 'Totale ordine', '', 'frontend', 2),
(247, 'LABEL_TOTAL', 'Totale', '', 'frontend', 2),
(248, 'LABEL_QTY', 'Quantità', '', 'frontend', 2),
(249, 'LABEL_SUBTOTAL_ORDER', 'Subtotale', '', 'frontend', 2),
(250, 'LABEL_TOTAL_CART', 'Totale carrello', '', 'frontend', 2),
(251, 'LABEL_TOTAL_ORDER_NOTES', '* Note: il totale include i costi di spedizione', '', 'frontend', 2),
(252, 'LABEL_TOTAL_CART_NOTES', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', '', 'frontend', 2),
(253, 'LABEL_CONFIRM', 'CONFERMA', '', 'frontend', 2),
(254, 'LABEL_REMOVE', 'ELIMINA', '', 'frontend', 2),
(255, 'LABEL_BACK_TO_CART', 'RITORNA AL CARRELLO', '', 'frontend', 2),
(256, 'LABEL_UPDATE_CART', 'AGGIORNA IL CARRELLO', '', 'frontend', 2),
(257, 'LABEL_BACK_TO_SHOP', 'TORNA ALLO SHOP', '', 'frontend', 2),
(258, 'LABEL_CART', 'Carrello', '', 'frontend', 2),
(259, 'LABEL_CART_EMPTY', 'Il tuo carrello è vuoto !', '', 'frontend', 2),
(260, 'LABEL_CHECKOUT', 'Checkout', '', 'frontend', 2),
(261, 'LABEL_PAYMENT_METHOD', 'Modalità di pagamento', '', 'frontend', 2),
(262, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', '', 'frontend', 2),
(263, 'LABEL_PAYMENT_METHOD_CC', 'Carta di credito / Prepagata', '', 'frontend', 2),
(264, 'LABEL_MESSAGE', 'Messaggio', '', 'frontend', 2),
(265, 'LABEL_SEND', 'INVIA', '', 'frontend', 2),
(266, 'LABEL_PRODUCTS', 'prodotti', '', 'frontend', 2),
(267, 'LABEL_CART_INFO_ACTUALLY_1', 'Attualmente ci sono', '', 'frontend', 2),
(268, 'LABEL_CART_INFO_ACTUALLY_2', 'nel carrello', '', 'frontend', 2),
(269, 'LABEL_SIZE', 'Tg', '', 'frontend', 2),
(270, 'LABEL_COLOR', 'Colore', '', 'frontend', 2),
(271, 'LABEL_CATEGORY', 'Categoria', '', 'frontend', 2),
(272, 'LABEL_DESCRIPTION', 'Descrizione', '', 'frontend', 2),
(273, 'LABEL_REVIEWS', 'Commenti', '', 'frontend', 2),
(274, 'LABEL_ALSO_LIKE', 'Ti potrebbero piacere', '', 'frontend', 2),
(275, 'LABEL_REFCODE', 'Rif', '', 'frontend', 2),
(276, 'LABEL_DETAIL', 'Dettaglio', '', 'frontend', 2),
(277, 'LABEL_SHOPPING_CART', 'Carrello', '', 'frontend', 2),
(278, 'LABEL_FRONT', 'FRONTE', '', 'frontend', 2),
(279, 'LABEL_BACK', 'RETRO', '', 'frontend', 2),
(280, 'LABEL_AVAILABILITY', 'Disponibilità', '', 'frontend', 2),
(281, 'LABEL_AVAILABILITY_HIGH', 'Alta', '', 'frontend', 2),
(282, 'LABEL_AVAILABILITY_LOW', 'Bassa', '', 'frontend', 2),
(283, 'LABEL_404_MESSAGE', 'Oops.... la pagina richiesta non esiste !', '', 'frontend', 2),
(284, 'LABEL_404_BTN', 'TORNA ALLA HOME', '', 'frontend', 2),
(285, 'LABEL_ORDER', 'Ordine', '', 'frontend', 2),
(286, 'LABEL_YOUR_ACCOUNT', 'Il tuo account', '', 'frontend', 2),
(287, 'LABEL_USER_ALREADY_REGISTERED', 'Sei già registrato ?', '', 'frontend', 2),
(288, 'LABEL_USER_NOTREGISTERED', 'Altrimenti puoi ordinare compilando i dati', '', 'frontend', 2),
(289, 'LABEL_USER_NOTREGISTERED_POINTS', 'non accumulerai punti e bonus riservati ai clienti registrati', '', 'frontend', 2),
(290, 'LABEL_COUPON', 'COUPON', '', 'frontend', 2),
(291, 'LABEL_COUPON_HAVE', 'Hai un codice sconto?', '', 'frontend', 2),
(292, 'LABEL_COUPON_INSERT', 'Inserisci il codice coupon', '', 'frontend', 2),
(293, 'LABEL_COUPON_APPLY', 'Applica coupon', '', 'frontend', 2),
(294, 'LABEL_DISCOUNT', 'Sconto', '', 'frontend', 2),
(295, 'LABEL_SHIPPING_THIS', 'Spedisci a questo indirizzo', '', 'frontend', 2),
(296, 'LABEL_SHIPPING_OTHER', 'Spedisci ad un altro indirizzo', '', 'frontend', 2),
(297, 'LABEL_NEW_ADDRESS', 'Nuovo indirizzo', '', 'frontend', 2),
(298, 'LABEL_BILLING_ADDRESS', 'Indirizzo di fatturazione', '', 'frontend', 2),
(299, 'LABEL_SHIPPING_ADDRESS', 'Indirizzo di spedizione', '', 'frontend', 2),
(300, 'LABEL_SHIPPING', 'Spedizione', '', 'frontend', 2),
(301, 'LABEL_CHANGE_PASSWORD', 'Cambio Password', '', 'frontend', 2),
(302, 'LABEL_ORDER_DATE', 'Data ordine', '', 'frontend', 2),
(303, 'LABEL_STRIPE_DESC', 'Paga in tutta sicurezza con Stripe', '', 'frontend', 2),
(304, 'MSG_SEARCH_INSERT', 'Inserisci il testo da ricercare', '', 'frontend', 2),
(305, 'MSG_SAVE_NEWSLETTER', 'Iscrivimi alla newsletter', '', 'frontend', 2),
(306, 'MSG_SEND_CONTACT_US', 'Richiesta di contatto inviata con successo!<br/>Grazie.', '', 'frontend', 2),
(307, 'MSG_PAYPAL_NOTETOPAYER', 'L''indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', '', 'frontend', 2),
(308, 'MSG_PAYPAL_CANCEL', 'Riprova ad effettuare il pagamemto !', '', 'frontend', 2),
(309, 'MSG_PAYPAL_ERROR', 'Errore durante il pagamento:', '', 'frontend', 2),
(310, 'MSG_NO_RESULT', 'Nessun risultato per ', '', 'frontend', 2),
(311, 'MSG_NO_RESULT_FILTER', 'Nessun risultato presente per i filtri selezionati.', '', 'frontend', 2),
(312, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', '', 'frontend', 2),
(313, 'MSG_ORDER_SUCCESS', 'inserito con successo ! Grazie.', '', 'frontend', 2),
(314, 'MSG_ORDER_PAYMENT_ERROR', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', '', 'frontend', 2),
(315, 'MSG_SUCCESS_CONTACT', 'Il tuo messaggio è stato inviato correttamente. Grazie !', '', 'frontend', 2),
(316, 'MSG_FAILURE_CONTACT', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', '', 'frontend', 2),
(317, 'MSG_SUCCESS_NEWSLETTER', 'Adesso sei iscritto alla newsletter. Grazie !', '', 'frontend', 2),
(318, 'MSG_UNIQUE_NEWSLETTER', 'L''indirizzo email è già iscritto alla newsletter', '', 'frontend', 2),
(319, 'MSG_UNSUBSCRIBE_DONE', 'La tua email/iscrizione è stata rimossa. Grazie!', '', 'frontend', 2),
(320, 'MSG_UNSUBSCRIBE_NOTFOUND', 'Il contatto richiesto non è attualmente registrato', '', 'frontend', 2),
(321, 'MSG_CART_REMOVED', 'Prodotto rimosso dal carrello', '', 'frontend', 2),
(322, 'MSG_CART_ADDED', 'Prodotto inserito nel carrello', '', 'frontend', 2),
(323, 'MSG_CART_UPDATED', 'Prodotto aggiornato nel carrello', '', 'frontend', 2),
(324, 'MSG_SERVICE_FAILURE', 'Si è verificato un errore. Riprova!', '', 'frontend', 2),
(325, 'MSG_BILLING_ADDRESS_NECESSARY', 'L''indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', '', 'frontend', 2),
(326, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', '', 'frontend', 2),
(327, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'inserisci un nuovo indirizzo per la spedizione', '', 'frontend', 2),
(328, 'MSG_COUPON_INVALID', 'Coupon non valido o scaduto', '', 'frontend', 2),
(329, 'MSG_COUPON_INVALID_OVER', 'Il valore del Coupon inserito è maggiore del carrello!', '', 'frontend', 2),
(330, 'LABEL_TP_SALE', 'Offerta', '', 'frontend', 2),
(331, 'LABEL_TP_BESTSELLER', 'Più venduti', '', 'frontend', 2),
(332, 'LABEL_TP_TOPRATED', 'Più votati', '', 'frontend', 2),
(333, 'LABEL_TP_STANDARD', 'Standard', '', 'frontend', 2),
(334, 'Standard', 'Nuovo', '', 'frontend', 2),
(335, 'SEND_AREYOUSURE_BTN', 'SEI SICURO ?', '', 'frontend', 2),
(336, 'LABEL_UNSUBSCRIBE', 'Cancellati', '', 'frontend', 2),
(337, 'LABEL_MY_ACCOUNT_EMAIL', 'Il mio profilo', '', 'email', 2),
(338, 'LABEL_SEE_EMAIL_ONLINE', 'Vedi email online', 'Vedi email online', 'email', 2),
(339, 'LABEL_EMAIL_SALES_TITLE', 'Controlla le nostre ultime offerte!', 'Ultime offerte', 'email', 2),
(340, 'LABEL_DETAIL_EMAIL', 'Vedi i dettagli', 'Vedi i dettagli', 'email', 2),
(341, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'cancellati', 'Cancellati footer', 'email', 2),
(342, 'TEXT_EMAIL_FOOTER_RESERVED', 'Tutti i diritti riservati', 'Tutti i diritti riservati', 'email', 2),
(343, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'Se non vuoi più ricevere queste email per favore', 'Non ricevere più email', 'email', 2),
(344, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contatto dal sito', 'Contatto dal sito', 'email', 2),
(345, 'LABEL_EMAIL_CONTACT_TITLE', 'Ti ringraziamo per averci contattato!', 'Ti ringraziamo per il contatto', 'email', 2),
(346, 'LABEL_EMAIL_CONTACT_TEXT', 'Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!', 'Comunicazione ricevuta', 'email', 2),
(347, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Iscrizione alla Newsletter', 'Iscrizione alla Newsletter', 'email', 2),
(348, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Grazie per esserti iscritto alla nostra newsletter!', 'Grazie newsletter', 'email', 2),
(349, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.', 'Riceverai gli aggiornamenti', 'email', 2),
(350, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Benvenuto su Ma Chlò', 'Benvenuto su', 'email', 2),
(351, 'LABEL_EMAIL_WELCOME_TITLE', 'Benvenuto su Ma Chlò!', 'Benvenuto su TITOLO', 'email', 2),
(352, 'LABEL_EMAIL_WELCOME_TEXT', 'Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.', 'Grazie per esserti registrato', 'email', 2),
(353, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'Nuovo ordine', 'Nuovo ordine', 'email', 2),
(354, 'LABEL_TITLE_INVOICE', 'Riepilogo ordine', 'Riepilogo ordine', 'email', 2),
(355, 'LABEL_INVOICE_THANKS', 'Ti ringraziamo per il tuo ordine!', 'Ti ringraziamo per il tuo ordine!', 'email', 2),
(356, 'LABEL_INVOICE_THANKS_TEXT', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere USER', 'email', 2),
(357, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di creare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere NO USER', 'email', 2),
(358, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'email', 2),
(359, 'LABEL_ORDER_EMAIL', 'Ordine', 'Ordine', 'email', 2),
(360, 'LABEL_ORDER_DATE_EMAIL', 'Data ordine', 'Data ordine', 'email', 2),
(361, 'LABEL_ADDRESS_NOTES_EMAIL', 'Note indirizzo', 'Note indirizzo', 'email', 2),
(362, 'LABEL_ORDER_NOTES_EMAIL', 'Note ordine', 'Note ordine', 'email', 2),
(363, 'LABEL_DESCRIPTION_EMAIL', 'Descrizione', 'Descrizione', 'email', 2),
(364, 'LABEL_QTY_EMAIL', 'Quantità', 'Quantità', 'email', 2),
(365, 'LABEL_SIZE_EMAIL', 'Taglia', 'Taglia', 'email', 2),
(366, 'LABEL_COLOR_EMAIL', 'Colore', 'Colore', 'email', 2),
(367, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotale', 'Subtotale', 'email', 2),
(368, 'LABEL_COUPON_APPLY_EMAIL', 'Applica coupon', 'Applica coupon', 'email', 2),
(369, 'LABEL_SHIPPING_EMAIL', 'Spedizione', 'Spedizione', 'email', 2),
(370, 'LABEL_TOTAL_EMAIL', 'Totale', 'Totale', 'email', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE IF NOT EXISTS `ordini` (
  `id_ordine` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_ordine` date DEFAULT NULL,
  `totale_ordine` double DEFAULT NULL,
  `note_ordine` text NOT NULL,
  `tipo_pagamento` int(11) NOT NULL,
  `stato_pagamento` int(11) NOT NULL,
  `token_pagamento` varchar(250) NOT NULL,
  `stato_ordine` tinyint(4) NOT NULL DEFAULT '1',
  `id_indirizzo_spedizione` int(11) DEFAULT NULL,
  `id_indirizzo_fatturazione_spedizione` int(11) DEFAULT NULL,
  `punti` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(250) NOT NULL,
  `coupon_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE IF NOT EXISTS `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `label_page_url` varchar(250) NOT NULL,
  `ordine_menu` int(5) NOT NULL,
  `nome_menu` varchar(255) NOT NULL,
  `testo_menu` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `label_page_url`, `ordine_menu`, `nome_menu`, `testo_menu`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', '', '', 0, '', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(3, 'Negozio', 'it/negozio', 1, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'Negozio'),
(4, 'Negozio', 'it/negozio/(:any)', 1, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(5, 'Chi siamo', 'it/chisiamo', 1, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'Chi siamo'),
(6, 'Spedizioni', 'it/spedizioni', 1, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, 'MENU_SHIPPING', 'Spedizioni'),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contatti'),
(8, 'Regolamento', 'it/regolamento', 1, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, 'MENU_RULES', 'Regolamento'),
(9, 'Prodotti', 'it/prodotti/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(10, 'Varianti', 'it/prodotti/(:any)/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(11, 'Account', 'it/account', 1, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(12, 'Logout', 'it/logout', 1, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(13, 'Login', 'it/login', 1, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(14, 'Registrati', 'it/registrati', 1, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(15, 'Salva account', 'it/salva_account', 1, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(16, 'Carrello', 'it/carrello', 1, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(17, 'Checkout', 'it/checkout', 1, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(18, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(19, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, '', ''),
(20, 'Shop', 'en/shop', 2, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, '', ''),
(21, 'Shop', 'en/shop/(:any)', 2, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(22, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, '', ''),
(23, 'Shipping', 'en/shipping', 2, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, '', ''),
(24, 'Contacts', 'en/contacts', 2, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, '', ''),
(25, 'Rules', 'en/rules', 2, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, '', ''),
(26, 'Products', 'en/products/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(27, 'Variants', 'en/products/(:any)/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(28, 'Account', 'en/account', 2, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(29, 'Logout', 'en/logout', 2, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(30, 'Login', 'en/login', 2, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(31, 'Register', 'en/register', 2, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(32, 'Save account', 'en/save_account', 2, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(33, 'Shopping cart', 'en/cart', 2, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(34, 'Checkout', 'en/checkout', 2, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(35, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, '', ''),
(36, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(37, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, '', ''),
(38, 'Prodotti', 'it/prodotti', 1, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', ''),
(39, 'Products', 'en/products', 2, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE IF NOT EXISTS `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `code`, `title`, `meta_description`, `description`, `image`, `id_lingua`) VALUES
(1, 'HOME', 'Abbigliamento artistico', 'Benvenuti sul sito di Ma Chlò vendita di abbigliamento di qualità basato su lavori artistici con spedizione in tutto il mondo.', '', '', 1),
(2, 'SHOP', 'Negozio', 'Qui puoi scoprire tutti i nostri prodotti, le taglie e i colori disponibili per portare le nostre opere a casa tua.', '', '', 1),
(3, 'ABOUT', 'Chi siamo', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte.', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte. Ma Chlò, infatti, non propone delle stampe qualsiasi ma delle vere e proprie opere d’arte uniche e originali che con i loro soggetti e fantasie raffigurano la natura in tutte le sue forme e colori, qualcosa di unico che prende ispirazione anche dai profumi, dalla magia e dai paesaggi della Sardegna. Ogni quadro è ispirato ad un viaggio, un vissuto, un’emozione, un’esperienza che l’artista ha voluto esprimere su tela e nella quale ogni donna può immedesimarsi.<br><br>\r\nOltre alla natura sotto forma di animali, alberi, fiori, Ma Chlò propone come linea principale “Le maschere” che rappresentano le varie tappe di una storia d’amore. «L’idea delle maschere» racconta l’artista «nasce da una passeggiata lungo l’ultimo chilometro e mezzo rimasto in piedi del muro di Berlino.<br><br>Rimasi affascinata da un murales che rappresentava due facce divise da qualcosa di importante, decisi di riportarle su tela giocando con i colori e dopo il primo disegno capii che quella doveva diventare una storia, la storia di un amore che nessun muro avrebbe più potuto fermare». Una storia, dunque, articolata in diverse fasi, l’incontro, lo sguardo, il bacio, la separazione, il silenzio, la rottura, l’infinito, un nuovo inizio che Ma Chlò vuole far indossare ad ogni donna come espressione del proprio vissuto.<br><br>\r\nPer iniziare Ma Chlò propone nella sua linea di abbigliamento t-shirt e vestiti con l’obiettivo di ampliare la sua gamma di prodotti in un’ottica di crescita continua.<h3>Mission</h3>La nostra mission è creare un punto di incontro tra moda e arte.<br>Ciò che ci contraddistingue è l’originalità ed il significato delle stampe.', 'about.jpg', 1),
(4, 'SHIPPING', 'Spedizioni e consegne', 'La spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine.', '<h3>Regole di spedizione e costi</h3>\r\nLa spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine, se invece si desidera una spedizione espressa i tempi di consegna sono di 3/5 giorni lavorativi con un costo a carico del cliente di 20 euro.', '99fc4-blog.jpg', 1),
(5, 'RULES', 'Regolamento e condizioni', 'Ma Chlò è marchio registrato, scopri come funziona e quale è il regolamento del servizio.', 'Ma Chlò è marchio registrato, tutti i suoi contenuti, quali, a titolo esemplificativo, le immagini, le fotografie, le opere, i disegni, le figure, i loghi ed ogni altro materiale, in qualsiasi formato, pubblicato su machlo.com , compresi i menu, le pagine web, la grafica, i colori, gli schemi, gli strumenti, i caratteri ed il design del sito web, il layouts, i metodi, i processi, le funzioni ed il software che fanno parte di Ma Chlò , sono protetti dal diritto d\\''autore e da ogni altro diritto di proprietà intellettuale del Gestore e degli altri titolari dei diritti. È vietata la riproduzione, in tutto o in parte, in qualsiasi forma, di Ma Chlò senza il consenso espresso in forma scritta del Gestore.<br><br> Ma Chlò garantisce un utilizzo dei dati personali strettamente legato all’erogazione dei propri servizi, alla gestione del sito e all’evasione degli ordini e non verranno in alcun modo venduti a terzi.<br><br> Ma Chlò non può garantire ai propri utenti che le misure adottate per la sicurezza del sito e della trasmissione dei dati e delle informazioni sul sito siano in grado di limitare o escludere qualsiasi rischio di accesso non consentito o di dispersione dei dati da parte di dispositivi di pertinenza dell’utente. Per tale motivo, suggeriamo agli utenti del sito di assicurarsi che il proprio computer sia dotato di software adeguati per la protezione della trasmissione in rete di dati (ad esempio antivirus aggiornati) e che il proprio Internet provider abbia adottato misure idonee per la sicurezza della trasmissione di dati in rete.<br><br>\r\n<h3>Pagamenti</h3>\r\nIl cliente puo’ scegliere il metodo di pagamento indicato al momento dell’acquisto nel modulo d’ordine.<br> Tutti i nostri prodotti sono made in Usa, i prezzi e le transazioni sono in Euro, eventuali cambi valuta, dazi doganali, commissioni su pagamenti con carta di credito e Paypal sono a carico del cliente.<br><br> Le informazioni relative all’esecuzione della transazione saranno inviate agli enti responsabili ( Circuito Visa/Mastercard, PayPal )  tramite protocollo crittografato, senza che terzi possano aver accesso in alcun modo. Le informazioni non saranno mai visualizzate o memorizzate da parte di Ma Chlò.<br><br>\r\n<h3>Reso</h3>\r\nIl cliente ha 14 giorni per esercitare il diritto di recesso dalla data di ricezione della merce e la spedizione di restituzione  sarà a carico del cliente.<br><br> Non è  possibile cambiare il prodotto scelto con un altro.<br> Per richiedere l’autorizzazione al reso, accedere alla sezione contatti e scrivere un messaggio di posta elettronica  all’indirizzo info@machlo.com  con indicazioni del prodotto.<br><br> Il Diritto di Recesso si intende esercitato correttamente qualora siano interamente rispettate anche le seguenti condizioni:<br> •	I prodotti non devono essere stati danneggiati, indossati, lavati e non devono presentare nessun segno d’uso.<br> •	I resi devono essere spediti all’interno della confezione Ma Chlò entro 14 giorni dalla data di comunicazione, da parte del cliente, del diritto di recesso.<br><br> Il rimborso sarà eseguito con lo stesso mezzo da te utilizzato per il pagamento dopo aver ricevuto il prodotto reso.<br> Ma Chlò si riserva inoltre il diritto di rifiutare resi non autorizzati o comunque non conformi a tutte le condizioni previste.', '', 1),
(6, 'PRIVACY', 'Privacy', 'Scopri in che modo vengo trattati i tuoi dati sensibili, l''utilizzo dei cookie e dei dati di sessione.', '<h3>Cookie policy</h3>\r\nIn questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio già richiesto dall''utente come il carrello degli acquisti. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell''utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l\\''utente all\\''interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l\\''efficienza del nostro portale.<br><br>Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br> In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L\\''anonimato dell\\''utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br><br> Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br> Non è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.<br><br> Tutti i dati inseriti dai nostri clienti all\\''interno di moduli, carrello, ordini e procedure di pagamento verranno utilizzati esclusivamente per gli ordini e le consegne degli stessi. Per questo motivo i clienti sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sarà possibile utilizzare il nostro servizio.<br> In particolare i dati utilizzati per i pagamenti online non verranno assolutamente trattati e/o memorizzati sui nostri sistemi in quanto passati direttamente ai servizi di pagamento Stripe e/o Paypal tramite transazione sicura SSL (HTTPS).<br>Al momento dell\\''ordine, della richiesta di preventivo o informazioni, l\\''indirizzo email dell\\''utente verrà inserito nel nostro sistema di newsletter e informazioni ai clienti da cui sarà sempre possibile disiscriversi facilmente tramite il link presente in ogni comunicazione inviata.<br> In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.', '', 1),
(7, 'CONTACTS', 'Contatti e recapiti', 'Puoi contattare Ma Chlò in tantissimi modi diversi: email, telefono e socials.', '', '', 1),
(8, 'GALLERY', 'Galleria immagini', 'Ecco una galleria delle nostre immagini più belle.', '', '', 1),
(9, 'HOME', 'Artistic apparel', 'Welcome to Ma Chlò\\''s website, selling quality garments based on artwork with worldwide shipping.', '', '', 2),
(10, 'SHOP', 'Our products', 'Here you can find all of our products, sizes and colors available to bring our works to your home.', '', '', 2),
(11, 'ABOUT', 'About us', 'Ma Chlò was born in Sardinia in 2017 by the idea and collaboration of Stefania, Simona and Francesca who wanted to reproduce on dresses the paintings of one of the founders who have always been fond of drawing and art.', 'Ma Chlò was founded in Sardinia in 2017 by the collaboration of Stefania, Simona and Francesca. One of our founders has always been interested in art and we wanted to display her paintings on our clothing. Ma Chlò doesn’t use prints but original and real works of art which represent nature in all its shapes and colors and include different subjects and their imaginations. Our unique offering also takes inspiration from the scents, magic and landscape of Sardinia. Each picture is inspired by a trip, an emotion or an experience that the artist wants to express in her art and which every woman who wears our clothes can feel.<br><br>Apart from nature in the form of animals, trees and flowers, Ma Chlò offers our customers the clothing line: "The Masks" that represents the evolution of a love story. "The idea of the masks," the artist tells us, “arose during a walk along the last kilometre and a half of the remaining section of the Berlin Wall. I was fascinated by a painting on this famous wall that represented two faces which had been divided from each other. After playing with the colours I understood after the first sketch that this was a story, a love story, that no barrier could ever stop." Different phases of the story are represented and include “the meeting”, ”the look”, “the kiss”, “the separation”, “the silence”, “the breakup”, “eternity” and “a new beginning” that Ma Chlò wants every woman to wear as an expression of her experiences. Ma Chlò currently offers t-shirts and dresses and plans to expand its range of products as we continue to grow.<h3>Mission</h3>We would like to create a meeting point between fashion and art.  We believe that our originality and the meaning of our paintings distinguishes us from other clothing companies.', 'about.jpg', 2),
(12, 'SHIPPING', 'Shipping and delivery', 'Welcome to Ma Chlò\\''s website, selling quality garments based on artwork with worldwide shipping.', '<h3>Shipping rules and price</h3>\r\nMa Chlò shipping is free, the delivery time is within 7/10 business days once the order will be fulfilled, or you can choose an express shipping for 20€', '99fc4-blog.jpg', 2),
(13, 'RULES', 'Rules and conditions', 'Ma Chlò is a registered trademark, find out how it works and what the service policy is.', 'Ma Chlò is a registered trade mark, all of its contents, like images, photos, works, sketches, figures, logos and every other material, in any format, published on machlo.com, included menus, web pages, graphics, colors, schemes, tools, characters and the design of the web site, layouts, methods, trials, functions and the software that make part of Ma Chlò, are protected by copyright and by all other intellectual rights of the Owner and the other holders of the rights. All reproduction, in whole or in part, in any form, of Ma Chlò, without written permission of the Owner, is forbidden.<br/><br/> Ma Chlò tightly guarantees an use of the personal data tied up to its own services, to the management of the site and the escape of the orders and they won''t come in some way sold to third.<br/><br/> Ma Chlò cannot guarantee to its own consumers that the measures adopted for the safety of the site and the transmission of the data and the information on the site is able to limit or to exclude any risk of access not allowed or of dispersion of the data from devices of pertinence of the consumer. For such motive, we suggest to the consumers of the site to make sure that his/her own computer both endowed with suitable software for the protection of the transmission online of data (for instance adjourned antivirus) and that the proper Internet provider has adopted online fit measures for the safety of the data transmission.<br>\r\n<h3>Payments</h3>\r\nThe consumer is able to choose the method of suitable payment during the purchase in the form of order. All of our products are made in Usa, the prices and the transactions are in Euro, possible changes currency, customs, errands on payments with credit card and Paypal are to load of the consumer. The financial information (Encircled Visa / Mastercard PayPal) will be cryptographically forwarded , without third parties being able to access said information in any way . Information will never be visualized or you memorize from Ma Chlò.<br/><br/>\r\n<h3>Refund</h3>\r\nThe consumer has the right to withdraw from the contract concluded with the Vendor, within 14 working days from the day of receiving the products purchased on “Ma Chlò”.<br/> Return will be a customer''s charge.<br/> An item cannot be exchanged for another one.<br/><br/> To ask for the authorization to theproduct, you can contact us by our web site on the section “contact” writing an e-mail message to the address info@machlo.com with indications of the product.<br/> The Right of Return is considered correctly followed when the following conditions are also completely met:<br/><br/> •	The products must not have been damaged, worn, washed and must not show any sign of use.<br/> •	the products must be returned in their original packaging to Ma Chlò within 14 days from the date of communication, from the consumer, of the right of recess.<br/><br/> The refund will be performed with the same metod used for the payment after having received the product.<br/><br/> Ma Chlò reserves the right to refuse non authorized refunds or however you doesn''t conform to all the anticipated conditions.', '', 2),
(14, 'PRIVACY', 'Privacy', 'Find out how we treat your sensitive data, use cookies, and sessions data.', '<h3>Cookie policy</h3>\r\nThis site uses some technical cookies that are used to navigate and provide a service already required by the user as the shopping cart. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the user\\''s device that visits a website. The cookie does not contain personal data and can not be used to identify the user inside other websites, including the analyst\\''s website. Cookies can also be used to store your favorite settings, such as your language and country, so that you can make them available immediately to your next visit. We do not use IP addresses or cookies to personally identify users. We use the web analytics system to increase the efficiency of our portal. <br> <br> For more information on cookies, please visit www.allaboutcookies.org to provide you with directions on how to handle your cookies Your preferences, and possibly delete cookies as a browser you are using. <br> On this website we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browsers, and user computers. This information is only examined for statistical purposes. The user\\''s anonymity is respected. About the operation of the open source Google Analytics web analytics software. <br> <br> We reiterate that technical cookies (such as those listed above) are required to navigate and essential, such as authentication, validation, management of a session of Navigation and fraud prevention and allow, for example: to identify whether the user has regularly accessed areas of the site that require prior authentication or user validation and session management for various services and applications or the retention of Data for secure access or the control and prevention of fraud. <br> It is not compulsory to acquire the consent of operating only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial of their operation will result in the inability to properly navigate the Site and / or the inability to access the services, pages, features or content available there. The data entered by our customers inside forms, shopping carts, orders and payment procedures will only be used for orders and deliveries. For this reason, customers are required to provide the data required to complete the required forms, otherwise we will not be able to use our service. In particular, the data used for online payments will not be treated and / or stored on Our systems as they have passed directly to the Stripe and / or Paypal payment services via Secure SSL Transaction (HTTPS). <br> At the time of ordering the quote or information, the user\\''s email address will come Inserted in our newsletter system and customer information from which it will always be possible to easily disagree with the link in any communication sent. <br> In any case, we confirm that the data used and stored for the purpose of service will never and No case has been transferred to third parties for any purpose or use.', '', 2),
(15, 'CONTACTS', 'Contacts and infos', 'You can contact Ma Chlò in many different ways: email, phone and socials.', '', '', 2),
(16, 'GALLERY', 'Images gallery', 'This is our best images gallery.', '', '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE IF NOT EXISTS `prodotti` (
  `id_prodotti` int(11) NOT NULL,
  `id_tipo_prodotto` int(11) DEFAULT NULL,
  `codice` varchar(100) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `prezzo` double DEFAULT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) DEFAULT NULL,
  `url_img_grande` varchar(250) DEFAULT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL,
  `id_colori_prodotti` int(11) NOT NULL DEFAULT '0',
  `ordine` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id_prodotti`, `id_tipo_prodotto`, `codice`, `nome`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `id_colori_prodotti`, `ordine`) VALUES
(1, 5, 'house_design_chair', 'Design chair', 45, 0, '5551c-item04.jpg', 'd70bd-th11.jpg', '', 1, 0, 1),
(2, 3, 'clocks_hand_man', 'Hand man clock', 128, 0, '92ea0-item03.jpg', '60325-th14.jpg', '', 1, 0, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_categorie`
--

CREATE TABLE IF NOT EXISTS `prodotti_categorie` (
  `id_prodotti_categorie` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_categorie`
--

INSERT INTO `prodotti_categorie` (`id_prodotti_categorie`, `id_prodotto`, `id_categoria`) VALUES
(3, 2, 2),
(4, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_traduzioni`
--

CREATE TABLE IF NOT EXISTS `prodotti_traduzioni` (
  `id_prodotti_traduzioni` int(11) NOT NULL,
  `id_prodotti` int(11) NOT NULL,
  `descrizione` text NOT NULL,
  `descrizione_breve` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_traduzioni`
--

INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(1, 1, 'House design chair', 'Design chair', 2),
(2, 1, 'Sedia di design da casa', 'Sedia di design', 1),
(3, 2, 'Hand man clock', 'Hand man clock special material', 2),
(4, 2, 'Orologio da polso uomo materiale speciale', 'Orologio da polso da uomo', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_taglia`
--

CREATE TABLE IF NOT EXISTS `prodotto_taglia` (
  `fk_prodotto` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_coupon`
--

CREATE TABLE IF NOT EXISTS `stato_coupon` (
  `id_stato_coupon` int(11) NOT NULL,
  `desc_stato_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_coupon`
--

INSERT INTO `stato_coupon` (`id_stato_coupon`, `desc_stato_coupon`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO'),
(3, 'UTILIZZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE IF NOT EXISTS `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_ordine`
--

CREATE TABLE IF NOT EXISTS `stato_ordine` (
  `id_stato_ordine` int(11) NOT NULL,
  `desc_stato_ordine` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_ordine`
--

INSERT INTO `stato_ordine` (`id_stato_ordine`, `desc_stato_ordine`) VALUES
(1, 'IN LAVORAZIONE'),
(2, 'IN CONSEGNA'),
(3, 'CONSEGNATO'),
(4, 'ANNULLATO'),
(5, 'SPEDITO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_pagamento`
--

CREATE TABLE IF NOT EXISTS `stato_pagamento` (
  `id_stato_pagamento` int(11) NOT NULL,
  `desc_stato_pagamento` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_pagamento`
--

INSERT INTO `stato_pagamento` (`id_stato_pagamento`, `desc_stato_pagamento`) VALUES
(1, 'ACCETTATO'),
(2, 'SOSPESO'),
(3, 'RIFIUTATO'),
(4, 'ANNULLATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_prodotti`
--

CREATE TABLE IF NOT EXISTS `stato_prodotti` (
  `stato_prodotti_id` int(11) NOT NULL,
  `stato_prodotti_desc` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_prodotti`
--

INSERT INTO `stato_prodotti` (`stato_prodotti_id`, `stato_prodotti_desc`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_carrello`
--

CREATE TABLE IF NOT EXISTS `storico_carrello` (
  `id_storico_carrello` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `data_storicizzazione` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` int(11) NOT NULL,
  `tipo_prodotto` varchar(250) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `prezzo` double NOT NULL DEFAULT '0',
  `prezzo_scontato` double NOT NULL DEFAULT '0',
  `url_immagine` varchar(250) NOT NULL,
  `colore_prodotto_id` int(11) DEFAULT NULL,
  `colore_prodotto` varchar(150) DEFAULT NULL,
  `colore_prodotto_codice` varchar(150) DEFAULT NULL,
  `is_variante` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_clienti`
--

CREATE TABLE IF NOT EXISTS `storico_clienti` (
  `id_storico_clienti` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `indirizzo_fatturazione` varchar(250) DEFAULT NULL,
  `indirizzo_spedizione` varchar(250) NOT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `taglie`
--

CREATE TABLE IF NOT EXISTS `taglie` (
  `id_taglia` int(11) NOT NULL,
  `codice` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `descrizione` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `separatore` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taglie`
--

INSERT INTO `taglie` (`id_taglia`, `codice`, `descrizione`, `separatore`) VALUES
(1, 'XS', 'Extra Small', '-'),
(2, 'S', 'Small', '-'),
(3, 'M', 'Medium', '-'),
(4, 'L', 'Large', '-'),
(5, 'XL', 'Extra Large', '-'),
(6, '2XL', '2 Extra Large', '-'),
(7, 'One Size', 'Taglia unica', '-');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id_tag` int(11) NOT NULL,
  `nome_tag` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags`
--

INSERT INTO `tags` (`id_tag`, `nome_tag`) VALUES
(1, 'Design'),
(2, 'Estate'),
(3, 'Elegante'),
(4, 'Manica corta'),
(5, 'Smanicato');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags_prodotti`
--

CREATE TABLE IF NOT EXISTS `tags_prodotti` (
  `id_tags_prodotti` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags_prodotti`
--

INSERT INTO `tags_prodotti` (`id_tags_prodotti`, `id_tag`, `id_prodotto`) VALUES
(1, 1, 1),
(2, 3, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_coupon`
--

CREATE TABLE IF NOT EXISTS `tipo_coupon` (
  `id_tipo_coupon` int(11) NOT NULL,
  `desc_tipo_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_coupon`
--

INSERT INTO `tipo_coupon` (`id_tipo_coupon`, `desc_tipo_coupon`) VALUES
(1, 'Sconto sul carrello utilizzo singolo'),
(2, 'Sconto % sul carrello utilizzo singolo'),
(3, 'Sconto sul carrello utilizzo multiplo'),
(4, 'Sconto % sul carrello utilizzo multiplo');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_pagamento`
--

CREATE TABLE IF NOT EXISTS `tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL,
  `desc_tipo_pagamento` varchar(100) NOT NULL,
  `icon_tipo_pagamento` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_pagamento`
--

INSERT INTO `tipo_pagamento` (`id_tipo_pagamento`, `desc_tipo_pagamento`, `icon_tipo_pagamento`) VALUES
(1, 'Paypal', 'fa-paypal'),
(2, 'Stripe', 'fa-cc-stripe');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_prodotto`
--

CREATE TABLE IF NOT EXISTS `tipo_prodotto` (
  `id_tipo_prodotto` int(11) NOT NULL,
  `descrizione_tipo_prodotto` varchar(30) NOT NULL,
  `css_class` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_prodotto`
--

INSERT INTO `tipo_prodotto` (`id_tipo_prodotto`, `descrizione_tipo_prodotto`, `css_class`) VALUES
(1, 'LABEL_TP_SALE', 'text-danger'),
(2, 'LABEL_TP_BESTSELLER', 'text-warning'),
(3, 'LABEL_TP_TOPRATED', 'top-rated'),
(4, 'LABEL_TP_STANDARD', ''),
(5, 'LABEL_TP_NEW', 'text-success');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template`
--

CREATE TABLE IF NOT EXISTS `tipo_template` (
  `id_tipo_template` int(11) NOT NULL,
  `desc_tipo_template` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template`
--

INSERT INTO `tipo_template` (`id_tipo_template`, `desc_tipo_template`) VALUES
(1, 'CONTATTO'),
(2, 'NEWSLETTER'),
(3, 'CUSTOM');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', 'rgH4-1JUJTBA.3ZhyB48Re2936ab16c301663644', 1516286626, NULL, 1268889823, 1517922022, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(2, '127.0.0.1', 'info@machlo.com', '$2y$08$VYAfiskG1BgPgTUswBIoaejDT6w6OCenxk9duml7x/hP6ArsaAdb2', NULL, 'info@machlo.com', NULL, NULL, NULL, NULL, 1505121761, 1509533886, 1, 'Stefania', 'Laconi', 'machlo', '060606'),
(5, '93.34.88.220', 'maurizio.maui@gmail.com', '$2y$08$IJj7OXsyviRMup4oDOUIIehrno/frnZSVfmRrOJjPC7yiZBHRKuUy', NULL, 'maurizio.maui@gmail.com', NULL, NULL, NULL, NULL, 1507237096, 1507237125, 1, 'Maurizio', 'Custodi', '0', '0'),
(9, '::1', 'posta@buongiornoamore.it', '$2y$08$GVu.Q9xhp338.ut.XC5MpexrCzBnucEetu.oMzzjTqHKtazC7d3Tm', NULL, 'posta@buongiornoamore.it', NULL, NULL, NULL, NULL, 1515403341, 1515410201, 1, 'Buongiorno', 'Amore', '0', '0');

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(6, 5, 2),
(10, 9, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `variante_taglia`
--

CREATE TABLE IF NOT EXISTS `variante_taglia` (
  `fk_variante` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `varianti_prodotti`
--

CREATE TABLE IF NOT EXISTS `varianti_prodotti` (
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_colore` int(11) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `prezzo` double NOT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) NOT NULL,
  `url_img_grande` varchar(250) NOT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`id_carrello`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Indexes for table `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  ADD PRIMARY KEY (`id_categoria_gallery`);

--
-- Indexes for table `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  ADD PRIMARY KEY (`id_categorie_gallery_traduzioni`);

--
-- Indexes for table `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `colori_classi`
--
ALTER TABLE `colori_classi`
  ADD PRIMARY KEY (`id_colore_classe`);

--
-- Indexes for table `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  ADD PRIMARY KEY (`id_colori_prodotti`);

--
-- Indexes for table `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indexes for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indexes for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id_coupon`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`id_ig`);

--
-- Indexes for table `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  ADD PRIMARY KEY (`id_immagini_gallery_traduzioni`);

--
-- Indexes for table `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  ADD PRIMARY KEY (`id_indirizzo_fatturazione`);

--
-- Indexes for table `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  ADD PRIMARY KEY (`id_indirizzo_spedizione`), ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indexes for table `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indexes for table `lingue_labels`
--
ALTER TABLE `lingue_labels`
  ADD PRIMARY KEY (`id_lingue_labels`);

--
-- Indexes for table `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  ADD PRIMARY KEY (`id_lingue_labels_lang`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id_ordine`), ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indexes for table `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indexes for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indexes for table `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`id_prodotti`), ADD KEY `id_tipo_prodotto` (`id_tipo_prodotto`);

--
-- Indexes for table `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  ADD PRIMARY KEY (`id_prodotti_categorie`);

--
-- Indexes for table `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  ADD PRIMARY KEY (`id_prodotti_traduzioni`);

--
-- Indexes for table `prodotto_taglia`
--
ALTER TABLE `prodotto_taglia`
  ADD PRIMARY KEY (`fk_prodotto`,`fk_taglia`);

--
-- Indexes for table `stato_coupon`
--
ALTER TABLE `stato_coupon`
  ADD PRIMARY KEY (`id_stato_coupon`);

--
-- Indexes for table `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indexes for table `stato_ordine`
--
ALTER TABLE `stato_ordine`
  ADD PRIMARY KEY (`id_stato_ordine`);

--
-- Indexes for table `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  ADD PRIMARY KEY (`id_stato_pagamento`);

--
-- Indexes for table `stato_prodotti`
--
ALTER TABLE `stato_prodotti`
  ADD PRIMARY KEY (`stato_prodotti_id`);

--
-- Indexes for table `storico_carrello`
--
ALTER TABLE `storico_carrello`
  ADD PRIMARY KEY (`id_storico_carrello`);

--
-- Indexes for table `storico_clienti`
--
ALTER TABLE `storico_clienti`
  ADD PRIMARY KEY (`id_storico_clienti`);

--
-- Indexes for table `taglie`
--
ALTER TABLE `taglie`
  ADD PRIMARY KEY (`id_taglia`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indexes for table `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  ADD PRIMARY KEY (`id_tags_prodotti`);

--
-- Indexes for table `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  ADD PRIMARY KEY (`id_tipo_coupon`);

--
-- Indexes for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  ADD PRIMARY KEY (`id_tipo_pagamento`);

--
-- Indexes for table `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  ADD PRIMARY KEY (`id_tipo_prodotto`);

--
-- Indexes for table `tipo_template`
--
ALTER TABLE `tipo_template`
  ADD PRIMARY KEY (`id_tipo_template`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `variante_taglia`
--
ALTER TABLE `variante_taglia`
  ADD PRIMARY KEY (`fk_variante`,`fk_taglia`);

--
-- Indexes for table `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  ADD PRIMARY KEY (`id_variante`), ADD KEY `fk_prodotti` (`id_prodotto`), ADD KEY `fk_colore` (`id_colore`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carrello`
--
ALTER TABLE `carrello`
  MODIFY `id_carrello` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  MODIFY `id_categoria_gallery` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  MODIFY `id_categorie_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clienti`
--
ALTER TABLE `clienti`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `colori_classi`
--
ALTER TABLE `colori_classi`
  MODIFY `id_colore_classe` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  MODIFY `id_colori_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id_coupon` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `id_ig` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  MODIFY `id_immagini_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  MODIFY `id_indirizzo_fatturazione` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  MODIFY `id_indirizzo_spedizione` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lingue_labels`
--
ALTER TABLE `lingue_labels`
  MODIFY `id_lingue_labels` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  MODIFY `id_lingue_labels_lang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=371;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id_ordine` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `id_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  MODIFY `id_prodotti_categorie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  MODIFY `id_prodotti_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stato_coupon`
--
ALTER TABLE `stato_coupon`
  MODIFY `id_stato_coupon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stato_ordine`
--
ALTER TABLE `stato_ordine`
  MODIFY `id_stato_ordine` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  MODIFY `id_stato_pagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `storico_carrello`
--
ALTER TABLE `storico_carrello`
  MODIFY `id_storico_carrello` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `storico_clienti`
--
ALTER TABLE `storico_clienti`
  MODIFY `id_storico_clienti` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taglie`
--
ALTER TABLE `taglie`
  MODIFY `id_taglia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  MODIFY `id_tags_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  MODIFY `id_tipo_coupon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  MODIFY `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  MODIFY `id_tipo_prodotto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipo_template`
--
ALTER TABLE `tipo_template`
  MODIFY `id_tipo_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  MODIFY `id_variante` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `prodotti`
--
ALTER TABLE `prodotti`
ADD CONSTRAINT `fk_tipo_prodotto` FOREIGN KEY (`id_tipo_prodotto`) REFERENCES `tipo_prodotto` (`id_tipo_prodotto`);

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
