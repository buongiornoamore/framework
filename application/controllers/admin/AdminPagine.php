<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminPagine extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('pagine_contenuti');
			$crud->order_by('id_pc', 'asc');
			// file upload
			$crud->set_field_upload('image', 'assets/assets-frontend/img/featured-image');
			// nome in tabella
			$crud->display_as('code', 'Pagina');
			$crud->display_as('title', 'Titolo');
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('description', 'Contenuto');
			$crud->display_as('meta_description', 'Meta tag');
			$crud->display_as('image', 'Immagine');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('code', 'title', 'id_lingua');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_texteditor('description');
			$crud->unset_texteditor('meta_description');
			// custom action
			//callbacks
			$crud->callback_after_update(array($this, 'update_lang_callback'));
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('code', 'readonly');
			} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PAGES';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/pages',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function update_lang_callback($post_array, $primary_key)
	{
		// @TODO caricare tutte le pagine di quella lingua
		// pagine_contenuti
		//$this->db->where('id_lingua', $post_array['id_lingua']);
		//$this->db->join('lingue', 'lingue.id_lingue = pagine_contenuti.id_lingua');
	//	$this->db->from('pagine_contenuti');
	//	$query = $this->db->get();
	//	$pagine = $query->result();
		//print_r($this->db->last_query());	
		
		// current row
		$this->db->select('*');
		$this->db->from('pagine_contenuti');
		$this->db->where('id_pc', $primary_key);
		$curr_row = $this->db->get()->row();
		
		// load lingua	
		$lang_id = $curr_row->id_lingua;
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('id_lingue', $lang_id);
		$query_lang = $this->db->get();
		
		// load pagine menu		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $lang_id);
		$this->db->where('nome_menu !=', "");
		$this->db->order_by("ordine_menu", 'asc');	
		$query_pages = $this->db->get();
		
		// pagine_contenuti
		$this->db->select('*');
		$this->db->from('pagine_contenuti');
		$this->db->where('id_lingua', $lang_id);
		$query_contents = $this->db->get();
		
		// chiama una procedura parametrica passando i dati di interesse e il tipo di labels da generare
		update_lang_file($query_contents->result(), $query_lang->row(), $query_pages->result(), "pages");
		return true;
	}
	
}