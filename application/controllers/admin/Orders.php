<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('ordini');
			// nome in tabella
			$crud->display_as('id_ordine', '# Ordine');
			$crud->display_as('id_cliente', 'Cliente');
			$crud->display_as('data_ordine', 'Data ordine');
			$crud->display_as('totale_ordine', 'Totale ordine');
			$crud->display_as('tipo_pagamento', 'Pagamento');
			$crud->display_as('stato_ordine', 'Stato ordine');
			$crud->display_as('id_indirizzo_spedizione', 'Indirizzo spedizione');
			$crud->display_as('id_indirizzo_fatturazione_spedizione', 'Spedisci a indirizzo di fatturazione');
		
			// file upload
			/*$crud->set_field_upload('url_img_piccola', 'assets/assets-frontend/img/cart');
			$crud->set_field_upload('url_img_grande', 'assets/assets-frontend/img/shop');
			$crud->set_field_upload('url_img_grande_retro', 'assets/assets-frontend/img/shop');*/
			// realazioni join
			$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');
			$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
			// campi obbligatori
		//	$crud->required_fields('codice', 'prezzo', 'nome', 'id_tipo_prodotto', 'stato');
			// regole validazione campi
		//	$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
		//	$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// campi per add
			$crud->add_fields('id_cliente', 'id_indirizzo_spedizione', 'stato_ordine', 'data_ordine', 'totale_ordine', 'note_ordine', 'tipo_pagamento', 'stato_pagamento', 'punti');
		
			$crud->edit_fields('id_ordine', 'id_cliente', 'id_indirizzo_spedizione', 'id_indirizzo_fatturazione_spedizione', 'stato_ordine', 'data_ordine', 'totale_ordine', 'note_ordine', 'tipo_pagamento', 'stato_pagamento', 'token_pagamento', 'punti', 'coupon');
			
			$crud->required_fields('id_cliente', 'data_ordine', 'tipo_pagamento', 'stato_pagamento', 'stato_ordine');
			if($crud->getState() == 'add')
    		{
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('id_indirizzo_spedizione', 'indirizzo_spedizione', 'note_sped');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
				$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');		
			} 
			else if($crud->getState() == 'edit')
			{
				// carica ordine dal db
				$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
				$this->db->where('id_ordine', $crud->getStateInfo()->primary_key);
				$this->db->from('ordini');
				$query_if = $this->db->get();
				$ordine_if = $query_if->row();

				$crud->change_field_type('id_ordine', 'readonly');
				$crud->set_relation('id_indirizzo_spedizione', 'indirizzo_spedizione', '{riferimento_sped} | {indirizzo_sped}, {civico_sped} - {cap_sped} {citta_sped} [{nazione_sped}]');
				$crud->set_relation('id_indirizzo_fatturazione_spedizione', 'indirizzo_fatturazione', '{riferimento_fatt} | {indirizzo_fatt}, {civico_fatt} - {cap_fatt} {citta_fatt} [{nazione_fatt}]', array('id_indirizzo_fatturazione' => $ordine_if->id_indirizzo_fatturazione));
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->change_field_type('token_pagamento', 'readonly');
				$crud->change_field_type('coupon', 'readonly');
			//	$crud->change_field_type('id_indirizzo_fatturazione_spedizione', 'true_false');
				//$crud->change_field_type('punti', 'readonly');
			}
			else if($crud->getState() == 'read')
			{
				$crud->display_as('token_pagamento', 'Carrello');
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
				$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');		
				$crud->set_relation_n_n('carrello', 'storico_carrello', 'prodotti', 'id_ordine', 'id_prodotto', '{prodotti.nome} x {qty}', 'qty');	
				$crud->change_field_type('coupon', 'readonly');
			//	$crud->fields('token_pagamento');
			//	$crud->callback_field('token_pagamento', array($this, '_callback_read_carrello'));	
			}
			// text editor
			$crud->unset_texteditor('note_ordine');
			// colonne da mostrare
			$crud->columns('id_ordine', 'id_cliente', 'data_ordine', 'totale_ordine', 'tipo_pagamento', 'stato_ordine');
			// unset action
			//$crud->unset_delete();
			$crud->unset_read();
			// custom action
			$crud->add_action('Invia email al cliente', '', '', 'fa-envelope', array($this, 'load_email_page'));
			$crud->add_action('Carrello', '', '', 'fa-shopping-cart', array($this, 'load_carrello'));
			$crud->add_action('Dettaglio', '', '', 'fa-print', array($this, 'dettaglio_ordine'));
			// callbacks
			$crud->callback_column('id_cliente', array($this, '_callback_cliente_info'));
			$crud->callback_column('totale_ordine', array($this, '_callback_value_to_euro'));
			$crud->callback_column('tipo_pagamento', array($this, '_callback_pagamento'));
			$crud->callback_after_delete(array($this,'_delete_order_cart'));
				
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Ordini';
			$data['collapseParentMenu'] = 'ordini';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function email($order_id) {
		$this->checkUserPermissions();
		//CRUD contatti email
		try {
			// load cliente da ordine
		//	$this->db->from('contatti_moduli');
		//	$this->db->join('lingue', 'lingue.id_lingue = contatti_moduli.id_lingua');
		//	$this->db->where('id_contatto', $cont_id);
		//	$query = $this->db->get();
		//	$cont = $query->row();
			
			// recupera la mail del cliente con una query e poi passala come href
		    // verificare se user_id allora prendi email da users altrimenti prendi da clienti
			$this->db->select('ordini.*, lingue.*, clienti.email AS email_cliente, users.email AS email_user, clienti.nome AS nome');
			$this->db->from('ordini');
			$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
			$this->db->join('lingue', 'lingue.id_lingue = clienti.id_lingua');
			$this->db->join('users', 'users.id = clienti.user_id', 'LEFT');
			$this->db->where('ordini.id_ordine', $order_id);
			$query_cliente = $this->db->get();
			$cliente = $query_cliente->row();
			$email_to = ($cliente->email_cliente != '' ? $cliente->email_cliente : $cliente->email_user);
			
		//	$this->cont_id = $cont->id_contatto;
		//    $this->cont_email = $cont->email;
		//	$this->cont_nome = $cont->nome;
		//	$this->cont_lingua = $cont->id_lingua;
			
			$crud = new grocery_CRUD();

			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates');
			$crud->where('lingua_traduzione_id', $cliente->id_lingue);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('nome_template', 'lingua_traduzione_id');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_read();
			$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'send_email_templates'));
			$crud->add_action('Preview email', '', '', 'fa-html5', array($this, 'preview_email_templates'));
		//	$crud->field_type('id_contatto', 'hidden', $cont_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Ordini';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Email per <b>' . $email_to . '</b> - ' . $cliente->nome_lingue;
			$data['curr_customer_email'] = $email_to;
			$data['curr_customer_id'] = 0;
			$data['curr_customer_name'] = $cliente->nome;
			$data['curr_customer_lang'] = $cliente->id_lingue;
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders_email',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function send_email_templates($primary_key, $row)
	{
		return site_url('admin/send_email_template/'.$row->id_template);
	}
	
	function preview_email_templates($primary_key, $row)
	{
		return site_url('email_template/'.$row->id_template.'/testmail');
	}
	
	function load_email_page($primary_key, $row)
	{	
		return site_url('admin/orders/email/'.$row->id_ordine);
	}
	
	function load_carrello($primary_key , $row)
	{
		return site_url('admin/orders/cart/'.$row->id_ordine);
	}
	
	function dettaglio_ordine($primary_key , $row)
	{
		return site_url('admin/orders/detail/'.$row->id_ordine);
	}
	
	function _callback_value_to_euro($value, $row)
	{
		return stampaValutaHtml($value, true, true);
	}
	
	function _callback_pagamento($value, $row)
	{
		// carica pagamento dal db
		$this->db->where('id_tipo_pagamento', $value);
		$this->db->from('tipo_pagamento');
		$query = $this->db->get();
		$pagamento = $query->row();
		return strtoupper($pagamento->desc_tipo_pagamento).' <i class="fa '.$pagamento->icon_tipo_pagamento.'"></i>';
	}
	
	function _callback_cliente_info($value, $row)
	{
		// carica cliente dal db
		$this->db->where('id_cliente', $value);
		$this->db->from('clienti');
		$query = $this->db->get();
		$cliente = $query->row();
		// mostra link per vedere i dati del cliente corrente se esistente
		if(isset($cliente))
			return "<a href='".site_url('admin/customers/'.$cliente->id_cliente)."' title=\"Vedi scheda cliente\">".$cliente->cognome. " " . $cliente->nome ."</a>";
		else 
			return "Consultare storico";
	}
	
	function _delete_order_cart($primary_key) {
		// cancella strico_carrello collegato e immagini dello storico
		$query_storico = $this->db->where('id_ordine', $primary_key)->get('storico_carrello');
		foreach ($query_storico->result() as $storico)
		{
		unlink('assets/assets-frontend/img/shop/storico/'.$storico->url_immagine); 
		$this->db->delete('storico_carrello', array('id_storico_carrello' => $storico->id_storico_carrello)); 
		}
		return true;	
	}
	
	public function loadIndirizziSpedizioneAjax($cliente_id) { 
       $result = $this->db->where("id_cliente", $cliente_id)->get("indirizzo_spedizione")->result();
       echo json_encode($result);
    }
	
	public function cart($order_id)
	{
		$this->checkUserPermissions();
		//CRUD cart
		try {
			$this->order_id_cart = $order_id;
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('storico_carrello');
			$crud->where('id_ordine', $order_id);
			// nome in tabella
			$crud->display_as('url_immagine', 'Immagine');
			$crud->display_as('prezzo_scontato', 'Scontato');
			$crud->display_as('qty', 'Q.tà');
			$crud->display_as('colore_prodotto_id', 'Colore');
			// upload
			$crud->set_field_upload('url_immagine', 'assets/assets-frontend/img/shop/storico');
			// realazioni join
			$crud->set_relation('taglia', 'taglie', 'codice');
			$crud->set_relation('colore_prodotto_id', 'colori_prodotti', 'nome_colore');
			// campi
			$crud->add_fields('id_ordine','id_prodotto', 'id_variante', 'qty', 'taglia');
			$crud->edit_fields('id_prodotto', 'id_variante', 'nome', 'codice', 'prezzo', 'prezzo_scontato', 'qty', 'colore_prodotto_id', 'taglia', 'url_immagine');
			$crud->required_fields('id_prodotto', 'qty', 'taglia');
			// colonne da mostrare
			$crud->columns('url_immagine', 'nome', 'codice', 'prezzo', 'prezzo_scontato', 'qty', 'colore_prodotto_id', 'taglia');
			// unset actions
			//$crud->unset_edit();
			$crud->unset_read();
			// callbacks
			$crud->callback_after_insert(array($this, '_update_storico_ordine'));
			$crud->callback_before_update(array($this,'_delete_img_file'));
			$crud->callback_after_update(array($this, '_update_storico_ordine'));
			$crud->callback_before_delete(array($this,'_delete_img_file_by_id'));
			$crud->callback_column('prezzo', array($this, '_callback_value_to_euro'));
			$crud->callback_column('prezzo_scontato', array($this, '_callback_value_to_euro'));
			
			if($crud->getState() == 'add')
    		{
				$crud->field_type('id_ordine', 'hidden', $order_id);
				$crud->set_relation('id_prodotto', 'prodotti', 'codice');
				$crud->set_relation('id_variante', 'varianti_prodotti', 'codice');	
			}
			else if ($crud->getState() == 'edit')
			{
				$crud->set_relation('id_prodotto', 'prodotti', 'codice');
				$crud->set_relation('id_variante', 'varianti_prodotti', 'codice');
				$crud->change_field_type('nome', 'readonly');
				$crud->change_field_type('codice', 'readonly');
				$crud->change_field_type('prezzo', 'readonly');
				$crud->change_field_type('colore_prodotto_id', 'readonly');
				$crud->change_field_type('prezzo', 'readonly');
			//	$crud->change_field_type('url_immagine', 'readonly', 'active');
			}
				
			$output = $crud->render();
			
			$data['curr_state'] = $crud->getState();
			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Carrello';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Carrello ordine <b>#' . $order_id . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders_cart',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _delete_img_file($post_array, $primary_key) {
		// cancella immagine corrente prima della sovrascrittura o della cancellazione
		unlink('assets/assets-frontend/img/shop/storico/'.$post_array['url_immagine']); 
		return $post_array;	
	}
	
	function _delete_img_file_by_id($primary_key) {
		// cancella immagine corrente prima della cancellazione
		$query_storico = $this->db->where('id_storico_carrello', $primary_key)->get('storico_carrello');
		$storico = $query_storico->row();
		unlink('assets/assets-frontend/img/shop/storico/'.$storico->url_immagine); 
		return true;	
	}
	
	function _update_storico_ordine($post_array, $primary_key) {
		// carica i dati dal db e popola tutti i campi
		$url_image_to_copy = '';
		if(isset($post_array['id_variante']) && $post_array['id_variante'] != NULL && $post_array['id_variante'] > 0) 
		{		
			// carica variante
			$this->db->select('*');
			$this->db->where('id_variante', $post_array['id_variante']);	
			$this->db->join('colori_prodotti', 'varianti_prodotti.id_colore = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->from('varianti_prodotti');
			$query = $this->db->get();
			$variante = $query->row();
			
			$prezzo_scontato_input = (isset($post_array['prezzo_scontato']) && $post_array['prezzo_scontato'] != NULL && $post_array['prezzo_scontato'] > 0 ? $post_array['prezzo_scontato'] : $variante->prezzo_scontato);
				
			// carica prodotto
			$this->db->select('*');
			$this->db->where('id_prodotti', $post_array['id_prodotto']);	
			$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
			$this->db->from('prodotti');
			$query = $this->db->get();
			$prodotto = $query->row();
			
			$data = array(
			   'data_storicizzazione' => date('Y-m-d H:i:s'),
			   'tipo_prodotto' => $prodotto->descrizione_tipo_prodotto,
			   'codice' => $variante->codice,
			   'nome' => $prodotto->nome,
			   'prezzo' => $variante->prezzo,
			   'prezzo_scontato' => $prezzo_scontato_input,
			   'url_immagine' => $variante->url_img_grande,
			   'colore_prodotto_id' => $variante->id_colori_prodotti,
			   'colore_prodotto' => $variante->nome_colore,
			   'colore_prodotto_codice' => $variante->codice_colore,
			   'is_variante' => 1
			);
			
			$url_image_to_copy = $variante->url_img_grande;
		}
		else 
		{
			// carica prodotto
			$this->db->select('*');
			$this->db->where('id_prodotti', $post_array['id_prodotto']);	
			$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
			$this->db->from('prodotti');
			$query = $this->db->get();
			$prodotto = $query->row();
			
			$prezzo_scontato_input = (isset($post_array['prezzo_scontato']) && $post_array['prezzo_scontato'] != NULL && $post_array['prezzo_scontato'] > 0 ? $post_array['prezzo_scontato'] : $prodotto->prezzo_scontato);
				
			$data = array(
			   'data_storicizzazione' => date('Y-m-d H:i:s'),
			   'tipo_prodotto' => $prodotto->descrizione_tipo_prodotto,
			   'codice' => $prodotto->codice,
			   'nome' => $prodotto->nome,
			   'prezzo' => $prodotto->prezzo,
			   'prezzo_scontato' => $prezzo_scontato_input,
			   'url_immagine' => $prodotto->url_img_grande,
			   'colore_prodotto_id' => $prodotto->id_colori_prodotti,
			   'colore_prodotto' => $prodotto->nome_colore,
			   'colore_prodotto_codice' => $prodotto->codice_colore,
			   'is_variante' => 0
			);
			
			$url_image_to_copy = $prodotto->url_img_grande;
		}

		$this->db->where('id_storico_carrello', $primary_key);
		$this->db->update('storico_carrello', $data);
		
		// copia immagine del prodotto in folder storico ASSETS_ROOT_FOLDER_FRONTEND_IMG
		copy('assets/assets-frontend/img/shop/'.$url_image_to_copy, 'assets/assets-frontend/img/shop/storico/'.$url_image_to_copy);
			
		// modifica il totale ordine
		$this->_update_totale_ordine($post_array, $primary_key);
	}
	
	function _update_totale_ordine($post_array, $primary_key)
	{
		// ricalcola il totale ordine in base allo storico_carrello modificato e assegna i punti
		$totale_ordine = 0;
		
		$this->db->select('*');
		$this->db->from('storico_carrello');
		$this->db->where('id_ordine', $this->order_id_cart);
		$query = $this->db->get();
		$carrello_list = $query->result();
		
		foreach($carrello_list as $item) {
			if($item->prezzo_scontato != NULL && $item->prezzo_scontato > 0)
				$totale_ordine += ($item->prezzo_scontato * $item->qty);
			else 
				$totale_ordine += ($item->prezzo * $item->qty);
		}
		
		// calcola punti per il cliente e metti anche in ordine
		$points = round($totale_ordine / PERC_PUNTI_RETURN);
		
		$data = array(
		   'totale_ordine' => $totale_ordine,
		   'punti' => $points
		);

		$this->db->where('id_ordine', $this->order_id_cart);
		$this->db->update('ordini', $data);

		return true;
	}
	
	public function loadVariantiAjax($prod_id) { 
       $result = $this->db->where("id_prodotto", $prod_id)->get("varianti_prodotti")->result();
       echo json_encode($result);
    }
   
    public function detail($order_id)
	{
		$this->checkUserPermissions();
		// NO-CRUD dettaglio ordine
		try {
			$output = array();
			
			// @TODO gestire indirizzo fatturazione e indirizzo spedizione selezionato
			$this->db->select('*');
			$this->db->from('ordini');
			$this->db->join('clienti', 'ordini.id_cliente = clienti.id_cliente');
			$this->db->join('stato_pagamento', 'ordini.stato_pagamento = stato_pagamento.id_stato_pagamento');
			$this->db->join('stato_ordine', 'ordini.stato_ordine = stato_ordine.id_stato_ordine');
			$this->db->join('tipo_pagamento', 'ordini.tipo_pagamento = tipo_pagamento.id_tipo_pagamento');
			$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = clienti.id_indirizzo_fatturazione');
			$this->db->join('indirizzo_spedizione', 'indirizzo_spedizione.id_indirizzo_spedizione = ordini.id_indirizzo_spedizione', 'LEFT');
			$this->db->where('id_ordine', $order_id);
			$query = $this->db->get();
			$ordine = $query->row();
			$data['ordine'] = $ordine;
			
			// id_indirizzo_fatturazione_spedizione
			/*if($ordine->id_indirizzo_fatturazione_spedizione == 0 && $ordine->id_indirizzo_spedizione > 0) {
				// indirizzo spedizione
				$this->db->select('*');
				$this->db->from('indirizzo_spedizione');
				$this->db->where('id_indirizzo_spedizione', $ordine->id_indirizzo_spedizione);
				$query_sped = $this->db->get();
				$ind_sped = $query_sped->row();
				$data['indirizzo_spedizione'] = $ind_sped;
			}
			*/
		/*	if($ordine->user_id > 0) {
				// indirizzo fatturazione
				$this->db->select('*');
				$this->db->from('indirizzo_fatturazione');
				$this->db->where('id_cliente', $ordine->id_cliente);
				$query_fatt = $this->db->get();
				$data['indirizzo_fatturazione'] = $query_fatt->row();
				// indirizzo spedizione
				$this->db->select('*');
				$this->db->from('indirizzo_spedizione');
				$this->db->join('indirizzo', 'indirizzo_spedizione.id_indirizzo = indirizzo.id_indirizzo');
				$this->db->where('id_indirizzo_spedizione', $ordine->id_indirizzo_spedizione);
				$query_sped = $this->db->get();
				$data['indirizzo_spedizione'] = $query_sped->row();
			} else {
				$data['indirizzo_fatturazione'] = '';
				$data['indirizzo_spedizione'] = '';

			}*/
			
			// caricare prodotti
			$this->db->select('storico_carrello.*, prodotti.*, taglie.codice as taglia_codice');
			$this->db->from('storico_carrello');
			$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
			$this->db->join('taglie', 'taglie.id_taglia = storico_carrello.taglia');
			$this->db->where('id_ordine', $order_id);
			$this->db->where('id_variante', 0);
			$query_prod = $this->db->get();
			$data['carrello_prodotti'] = $query_prod->result();
			
			// caricare varianti
			//$this->db->select('*');
			$this->db->select('storico_carrello.*, varianti_prodotti.*, taglie.codice as taglia_codice');
			$this->db->from('storico_carrello');
			$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = storico_carrello.id_variante');
			$this->db->join('taglie', 'taglie.id_taglia = storico_carrello.taglia');
			$this->db->where('id_ordine', $order_id);
			$query_var = $this->db->get();
			$data['carrello_varianti'] = $query_var->result();
			
			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Dettaglio';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Dettaglio ordine <b>#' . $order_id . '</b>';
			$data['resourcetype'] = 'NO-CRUD';
			$output['data'] = $data;
			$this->load->view('admin/orders_detail',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
