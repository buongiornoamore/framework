<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			
			$crud->set_subject('Prodotto');
			
			// tabella
			$crud->set_table('prodotti');
			$crud->order_by('ordine', 'desc');
			// nome in tabella
			$crud->display_as('id_tipo_prodotto', 'Tipo prodotto');
			$crud->display_as('id_colori_prodotti', 'Colore');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('prezzo_scontato', 'Scontato');
			$crud->display_as('url_img_piccola', 'Imm. carrello');
			$crud->display_as('url_img_grande', 'Imm. fronte');
			$crud->display_as('url_img_grande_retro', 'Imm. retro');
			$crud->display_as('taglie', 'Taglie');
			$crud->display_as('tags', 'Tags');
			// file upload
			$crud->set_field_upload('url_img_piccola', 'assets/assets-frontend/img/cart');
			$crud->set_field_upload('url_img_grande', 'assets/assets-frontend/img/shop');
			$crud->set_field_upload('url_img_grande_retro', 'assets/assets-frontend/img/shop');
			// realazioni join
			$crud->set_relation('id_tipo_prodotto', 'tipo_prodotto', 'descrizione_tipo_prodotto');
			$crud->set_relation('id_colori_prodotti', 'colori_prodotti', 'nome_colore');
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation_n_n('taglie', 'prodotto_taglia', 'taglie', 'fk_prodotto', 'fk_taglia', 'codice');
			$crud->set_relation_n_n('tags', 'tags_prodotti', 'tags', 'id_prodotto', 'id_tag', 'nome_tag');
			$crud->set_relation_n_n('categorie', 'prodotti_categorie', 'categorie', 'id_prodotto', 'id_categoria', 'nome');
			// campi obbligatori
			$crud->required_fields('codice', 'prezzo', 'nome', 'id_tipo_prodotto', 'stato');
			// regole validazione campi
			$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
			$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// campi per add
			$crud->add_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine');
			$crud->edit_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine', 'url_img_piccola', 'url_img_grande', 'url_img_grande_retro');
			//if($crud->getState() == 'add')
    	//	{
		//		$crud->field_type('stato', 'dropdown', 3); // stato sospeso
		//	} 
			// callbacks
			$crud->callback_before_insert(array($this,'formatProdottoCodice'));
			$crud->callback_before_update(array($this,'formatProdottoCodice'));
			// colonne da mostrare
			$crud->columns('url_img_grande', 'codice', 'categorie', 'nome', 'prezzo', 'prezzo_scontato', 'ordine', 'stato');
			// unset delete action
			$crud->unset_delete();
			// custom action
			$crud->add_action('Traduzioni prodotto', '', '', 'fa-file-text', array($this, 'load_traduzioni'));
			$crud->add_action('Varianti prodotto', '', '', 'fa-shopping-bag', array($this, 'load_varianti'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	function formatProdottoCodice($post_array) {
		$post_array['codice'] = formattaUrlCodice($post_array['codice'], '_');
		return $post_array;
	}
	function load_varianti($primary_key , $row)
	{
		return site_url('admin/products/variants/'.$row->id_prodotti.'/'.$row->codice);
	}
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/products/trad/'.$row->id_prodotti.'/'.$row->codice);
	}
	public function variants($prod_id, $prod_code)
	{
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('varianti_prodotti');
			$crud->where('id_prodotto', $prod_id);
			// nome in tabella
		//	$crud->display_as('id_categoria', 'Categoria');
		//	$crud->display_as('id_tipo_prodotto', 'Tipo prodotto');
			$crud->display_as('id_colore', 'Colore');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('prezzo_scontato', 'Scontato');
			$crud->display_as('url_img_piccola', 'Imm. carrello');
			$crud->display_as('url_img_grande', 'Imm. fronte');
			$crud->display_as('url_img_grande_retro', 'Imm. retro');
			$crud->display_as('taglie', 'Taglie');
			// file upload
			$crud->set_field_upload('url_img_piccola', 'assets/assets-frontend/img/cart');
			$crud->set_field_upload('url_img_grande', 'assets/assets-frontend/img/shop');
			$crud->set_field_upload('url_img_grande_retro', 'assets/assets-frontend/img/shop');
			// realazioni join
			$crud->set_relation('id_colore', 'colori_prodotti', 'nome_colore');
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation_n_n('taglie', 'variante_taglia', 'taglie', 'fk_variante', 'fk_taglia', 'codice');
			// campi obbligatori
			$crud->required_fields('id_colore', 'codice', 'prezzo', 'stato');
			$crud->edit_fields('id_colore', 'stato', 'codice', 'prezzo', 'prezzo_scontato', 'url_img_piccola', 'url_img_grande', 'url_img_grande_retro', 'taglie');
			$crud->add_fields('id_prodotto', 'id_colore', 'stato', 'codice', 'prezzo', 'prezzo_scontato', 'taglie');
			// regole validazione campi
			$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
			$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// colonne da mostrare
			$crud->columns('url_img_grande', 'codice', 'prezzo', 'prezzo_scontato', 'taglie', 'stato');
			// unset delete action
			$crud->unset_delete();
			$crud->field_type('id_prodotto', 'hidden', $prod_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Varianti per <b>' . $prod_code . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/variants',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function traductions($prod_id, $prod_code)
	{
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('prodotti_traduzioni');
			$crud->where('id_prodotti', $prod_id);
			// nome in tabella
			$crud->display_as('descrizione', 'Descrizione');
			$crud->display_as('descrizione_breve', 'Descrizione breve');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			$crud->edit_fields('descrizione','descrizione_breve', 'lingua_traduzione_id');
			$crud->add_fields('id_prodotti', 'descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// colonne da mostrare
			$crud->columns('descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_texteditor('descrizione');
			$crud->field_type('id_prodotti', 'hidden', $prod_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Traduzioni per <b>' . $prod_code . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
