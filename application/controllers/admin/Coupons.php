<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/CouponsGen.php');

class Coupons extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('admin/coupons');
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD 
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('coupon');
			// join
			$crud->set_relation('stato_coupon', 'stato_coupon', 'desc_stato_coupon');
			$crud->set_relation('tipo_coupon', 'tipo_coupon', 'desc_tipo_coupon');
			// nome in tabella
			$crud->display_as('codice_coupon', 'Codice');
			$crud->display_as('data_scadenza_coupon', 'Scadenza');
			$crud->display_as('importo_coupon', 'Importo');
			$crud->display_as('percentuale_coupon', '%');
			$crud->display_as('stato_coupon', 'Stato');
			$crud->display_as('tipo_coupon', 'Tipo');
			// campi obbligatori
			$crud->required_fields('data_scadenza_coupon', 'data_scadenza_coupon', 'tipo_coupon', 'stato_coupon');
			// regole validazione campi
			$crud->field_type('data_scadenza_coupon', 'date');
			// colonne da mostrare
			$crud->add_fields('data_scadenza_coupon', 'tipo_coupon', 'importo_coupon', 'percentuale_coupon', 'codice_coupon', 'stato_coupon');
			$crud->edit_fields('data_scadenza_coupon', 'tipo_coupon', 'importo_coupon', 'percentuale_coupon', 'codice_coupon', 'stato_coupon');
			$crud->columns('codice_coupon', 'importo_coupon', 'percentuale_coupon', 'data_scadenza_coupon', 'tipo_coupon', 'stato_coupon');
			if($crud->getState() == 'add') {
				// genera nuovo codice coupon
				//	$crud->callback_add_field('codice_coupon', array($this, '_callback_generate_coupon'));
				$crud->field_type('codice_coupon', 'hidden', coupon::generate(8));
			} else if ($crud->getState() == 'edit') {
				$crud->change_field_type('codice_coupon', 'readonly');
			}
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-COUPONS';
			$data['curr_page_title'] = 'Coupons';
			$data['collapseParentMenu'] = 'coupons';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/coupons',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _callback_generate_coupon($value, $row)
	{
		// chiama la libreria di generazione coupon e crea un nuovo codice ogni volta
		return coupon::generate(8); 	
	}
}