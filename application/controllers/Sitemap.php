<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
	}
	
	public function index()
	{
		$site_url = base_url();
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load languages
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->order_by("id_lingue", "asc");
		$query_lingue = $this->db->get();
		
		foreach ($query_lingue->result() as $lingua)
		{
			array_push($urls, $site_url.strtolower($lingua->abbr_lingue).'/sitemap_pages.xml', $site_url.strtolower($lingua->abbr_lingue).'/sitemap_products.xml', $site_url.strtolower($lingua->abbr_lingue).'/sitemap_images.xml');
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'general',
					  'urls' => $urls
					  );		
					  
		// load general sitemap info			
		$this->load->view('sitemap', $data);
	}
	
	public function pages($lang_code)
	{
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
	//	$this->db->join('lingue_labels_lang', 'lingue.id_lingue = lingue_labels_langs.id_lingua AND lingue_labels_lang.lingue_labels_lang_label = \'LABEL_FRONT\'');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		// load pages		
		$site_url = base_url();
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load pagine		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $curr_lang->id_lingue);
		$this->db->where('nome_menu <>', '');
		$query_pages = $this->db->get();

		foreach ($query_pages->result() as $page)
		{
			$tmp_url = $site_url . $page->url_pagina;
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));		
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function products($lang_code)
	{	
		$site_url = base_url();
		$max_date = date("Y-m-d\TH:i:sP");
		$blank_date = "0000-00-00";
		$change_freq = 'daily';
		$priority = '1';
		$priority_child = '0.8';
		
		$urls = array();
		
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->join('pagine', 'lingue.id_lingue = pagine.id_lingua AND pagine.label_page_url = \'PAGE_PRODUCTS_URL\'');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		$this->db->select('*');
		$this->db->from('prodotti');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
		$this->db->order_by("nome", "asc");
		
		$query = $this->db->get();
		foreach ($query->result() as $prod)
		{
			$tmp_url = $site_url . $curr_lang->url_pagina . '/' . $prod->codice . '/' . cleanString($prod->descrizione_breve . (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : ''), true);
			array_push($urls, $this->createUrl($tmp_url, $max_date, $change_freq, $priority));	
			
			// prodotti varianti
			$this->db->select('*');
			$this->db->from('varianti_prodotti');
			$this->db->where('id_prodotto', $prod->id_prodotti);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
			$this->db->join('colori_prodotti', 'varianti_prodotti.id_colore = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->join('prodotti_traduzioni', 'varianti_prodotti.id_prodotto = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('varianti_prodotti.id_prodotto', "ASC"); 
			$query_child = $this->db->get();
			foreach ($query_child->result() as $child)
			{
				$tmp_url_child = $site_url . $curr_lang->prodotti . '/' . $prod->codice . '/' . $child->codice . '/' . cleanString($prod->descrizione_breve . (isset($child->nome_colore) ? ' ' . $child->nome_colore : ''), true);
				array_push($urls, $this->createUrl($tmp_url_child, $max_date, $change_freq, $priority));	
			
			}
		}
		
		$data = array(
					  'max_date' => $max_date, 
					  'blank_date' => $blank_date,
					  'change_freq' => $change_freq,
					  'priority' => $priority,
					  'priority_child' => $priority_child,
					  'sitemap_urls_type' => 'detail',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	public function images($lang_code)
	{	
		$urls = array();
		
		// load lingua		
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->join('pagine', 'lingue.id_lingue = pagine.id_lingua AND pagine.label_page_url = \'PAGE_PRODUCTS_URL\'');
		$this->db->where('lingue.abbr_lingue', $lang_code);
		$query_lang = $this->db->get();
		$curr_lang = $query_lang->row();
		
		$label_front = "front";
		$label_back = "rear";
		
		$prod_url = $curr_lang->url_pagina;
		
		// load products				
		$this->db->select('prodotti_traduzioni.*, prodotti.*, colori_prodotti.*');
		$this->db->from('prodotti');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
		$this->db->order_by("nome", "asc");
		
		$query = $this->db->get();
		
		foreach ($query->result() as $prod)
		{
			$tmp_url = base_url() . $prod_url . '/' . $prod->codice . '/' . cleanString($prod->descrizione_breve . (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : ''), true);
			array_push($urls, $this->createImageUrl($tmp_url, ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$prod->url_img_grande, $prod->descrizione_breve . (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : ''), SITE_TITLE_NAME.' | '.$prod->nome. (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : '').' '.$label_front));
			// immagine retro se esistente
			if($prod->url_img_grande_retro != '')	
				array_push($urls, $this->createImageUrl($tmp_url, ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$prod->url_img_grande_retro, $prod->descrizione_breve . (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : '') .' '.$label_back, SITE_TITLE_NAME.' | '.$prod->nome. (isset($prod->nome_colore) ? ' ' . $prod->nome_colore : '').' '.$label_back));	
				
			// prodotti varianti
			$this->db->select('*');
			$this->db->from('varianti_prodotti');
			$this->db->where('id_prodotto', $prod->id_prodotti);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $curr_lang->id_lingue);
			$this->db->join('colori_prodotti', 'varianti_prodotti.id_colore = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->join('prodotti_traduzioni', 'varianti_prodotti.id_prodotto = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('varianti_prodotti.id_prodotto', "ASC"); 
			$query_child = $this->db->get();
			foreach ($query_child->result() as $child)
			{
				$tmp_url_child = base_url() . $prod_url . '/' . $prod->codice . '/' . $child->codice . '/' . cleanString($prod->descrizione_breve . (isset($child->nome_colore) ? ' ' . $child->nome_colore : ''), true);	
				array_push($urls, $this->createImageUrl($tmp_url_child, ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$child->url_img_grande, $prod->descrizione_breve . (isset($child->nome_colore) ? ' ' . $child->nome_colore : ''), SITE_TITLE_NAME.' | '.$prod->nome. (isset($child->nome_colore) ? ' ' . $child->nome_colore : '').' '.$label_front));
			// immagine retro se esistente
			if($prod->url_img_grande_retro != '')	
				array_push($urls, $this->createImageUrl($tmp_url_child, ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$child->url_img_grande_retro, $prod->descrizione_breve . (isset($child->nome_colore) ? ' ' . $child->nome_colore : '') .' '.$label_back, SITE_TITLE_NAME.' | '.$prod->nome. (isset($child->nome_colore) ? ' ' . $child->nome_colore : '').' '.$label_back));		
			
			}
		}
		
		$data = array(
					  'sitemap_urls_type' => 'images',
					  'urls' => $urls
					  );		
					  
		$this->load->view('sitemap', $data);
	}
	
	/* stampa url per sitemap */
	public function createUrl($url, $lastmod, $change_freq, $priority) {
	
		return '<url>
				<loc>' . $url . '</loc>
				' . ($lastmod != "0000-00-00" ? '<lastmod>' . date_format(date_create($lastmod), "Y-m-d\TH:i:sP") . '</lastmod>' : '') .  ($change_freq != '' ? '<changefreq>' . $change_freq . '</changefreq>' : '') . ($priority != '' ? '<priority>' . $priority . '</priority>' : '') . '
			  </url>';
		  
	}
	public function createImageUrl($loc, $loc_image, $caption, $title) {
	
		return '<url>
				<loc>' . $loc . '</loc>
				<image:image>
					<image:loc>' . $loc_image . '</image:loc>
					<image:title>' . $this->string_sanitize($title) . '</image:title>
					<image:caption>' . $this->string_sanitize($caption) . '</image:caption>
				</image:image>	
			  </url>';
		  
	}
	
	public function string_sanitize($s) {
		$result = preg_replace("/[^\s\p{L} 0-9|-]/u", "", html_entity_decode($s, ENT_QUOTES));
		return $result;
	}
}