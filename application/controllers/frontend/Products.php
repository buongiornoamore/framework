<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	//	$this->load->view('frontend/shopping-cart');
	}

	// print ajax list of available products
	public function getProducts()
	{
		$category_url = $_POST['category_url'];
		$sort = $_POST['sort'];
		$range_min = $_POST['range_min'];
		$range_max = $_POST['range_max'];
		$tags = $_POST['tags'];

		$load_step = 6;
		//$load_from = (isset($_POST['load_from']) ? (int)$_POST['load_from'] + $load_step : 0);
		$load_from = (isset($_POST['load_from']) ? $_POST['load_from'] : 0);
		//$load_to = $load_from + $load_step;

		$tagsArr = array_map('intval', explode(',', $tags));

		$num_results = $this->countAllRows($category_url, $sort, $range_min, $range_max, $tagsArr, $tags);
		$more_count = $num_results - $load_from - $load_step;
		// retrun load from step
		echo '<input type="hidden" id="load_from" data-morecount="'.$more_count.'" value="'. (($num_results - $load_from - $load_step) > 0 ? $load_from + $load_step : 0) . '" />';

		$this->db->select('prodotti.*, tipo_prodotto.*, tags_prodotti.*, colori_prodotti.*, prodotti_traduzioni.*');

		if($category_url != '')
			$this->db->where('categorie.url_categorie', $category_url);
			
		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('IF(prezzo_scontato > 0, prezzo_scontato, prezzo) >= ' .$range_min, NULL, FALSE);
		$this->db->where('IF(prezzo_scontato > 0, prezzo_scontato, prezzo) <= ' .$range_max, NULL, FALSE);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('tags_prodotti', 'prodotti.id_prodotti = tags_prodotti.id_prodotto', "LEFT");
		$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->group_by("prodotti.id_prodotti");
		$this->db->order_by("prodotti.ordine", 'desc');
		
		// order by filter sort
		if($sort == 'discount')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 1,5,4,3,2)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'alpha')
		{
			$this->db->order_by("prodotti.nome", "asc");
		}
		else if($sort == 'new')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 5,4,3,2,1)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'rate')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 3,5,4,2,1)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'best')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 2,5,4,3,1)');
			$this->db->_protect_identifiers = TRUE;
		}

		// filter for selected tags
		if($tags != '')
		{
			$this->db->where_in('tags_prodotti.id_tag', $tagsArr);
		}

		// limit load more
		if($load_from > 0)
			$this->db->limit($load_step, $load_from);
		else
			$this->db->limit($load_step);

		$query = $this->db->get();

		// print query
		//print_r($this->db->last_query());
	
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$this->getItemDiv($row);
			}
		} else {
			echo '<div class="col-md-12 col-sm-12" align="center"><b>'.lang('MSG_NO_RESULT_FILTER') .'</b></div>';
		}
	}

	public function countAllRows($category_url, $sort, $range_min, $range_max, $tagsArr, $tags)
	{
		if($category_url != '')
			$this->db->where('categorie.url_categorie', $category_url);

		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('IF(prezzo_scontato > 0, prezzo_scontato, prezzo) >= ' .$range_min, NULL, FALSE);
		$this->db->where('IF(prezzo_scontato > 0, prezzo_scontato, prezzo) <= ' .$range_max, NULL, FALSE);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('tags_prodotti', 'prodotti.id_prodotti = tags_prodotti.id_prodotto', "LEFT");
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->group_by("prodotti.id_prodotti");
		$this->db->order_by("prodotti.ordine", 'desc');

		// order by filter sort
		if($sort == 'discount')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 1,5,4,3,2)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'alpha')
		{
			$this->db->order_by("prodotti.nome", "asc");
		}
		else if($sort == 'new')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 5,4,3,2,1)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'rate')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 3,5,4,2,1)');
			$this->db->_protect_identifiers = TRUE;
		}
		else if($sort == 'best')
		{
			$this->db->_protect_identifiers = FALSE;
			$this->db->order_by('FIELD(prodotti.id_tipo_prodotto, 2,5,4,3,1)');
			$this->db->_protect_identifiers = TRUE;
		}

		// filter for selected tags
		if($tags != '')
		{
			$this->db->where_in('tags_prodotti.id_tag', $tagsArr);
		}

		$query = $this->db->get();
		return $query->num_rows();
	}

	// print ajax list of available products serached by text
	public function searchProducts()
	{
		$search_text = $_POST['search_text'];
		$this->db->select('prodotti.*, tipo_prodotto.*, tags_prodotti.*, colori_prodotti.*, prodotti_traduzioni.*, categorie.stato as cat_stato');
		$this->db->where('prodotti.stato', 1);
		//$this->db->where('categorie.stato', 1);
	//	$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->like('prodotti.nome', $search_text);
		$this->db->or_like('prodotti_traduzioni.descrizione', $search_text);
		$this->db->or_like('prodotti_traduzioni.descrizione_breve', $search_text);
		$this->db->or_like('codice', $search_text);
		$this->db->from('prodotti');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('tags_prodotti', 'prodotti.id_prodotti = tags_prodotti.id_prodotto', 'LEFT');
		$this->db->join('colori_prodotti', 'prodotti.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
		$this->db->group_by("prodotti.id_prodotti");
		$this->db->order_by("prodotti.ordine", 'desc');
		$this->db->having('cat_stato', 1);
		$this->db->having('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$query = $this->db->get();

		// print query
		//print_r($this->db->last_query());
		if($query->num_rows() > 0)
		{
			echo '<div class="col-md-12 col-sm-12" align="center" style="margin-bottom:15px;" id="search-msg">Trovati <b>'.$query->num_rows().'</b> risutati per <b>'.$search_text.'</b></div>';
			foreach ($query->result() as $row)
			{
			//	echo $row->nome;
				$this->getItemDiv($row);
			}
		} else {
			echo '<div class="col-md-12 col-sm-12" align="center" id="search-msg">'.lang('MSG_NO_RESULT').'<b>'.$search_text.'</b></div>';
		}
	}
	// print ajax list of available products
	public function getFilterCategories()
	{
		$category_url = $_POST['category_url'];

		// default TUTTI distinct sulla tabella delle categorie_prodotti
		$this->db->select('COUNT(DISTINCT prodotti.id_prodotti) AS numrows', false);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti');
		$queryCount = $this->db->get();
		
		echo '<li class="filter-category-li active"><a href="#" class="filter-category" data-category="">'.lang("LABEL_ALL").'</a> <sup>'.$queryCount->row('numrows').'</sup></li>';
		//print_r($this->db->last_query());
		// other categories
		$this->db->select('categorie.*, COUNT(prodotti.id_prodotti) AS num_prod');
		$this->db->from('categorie');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('prodotti', 'prodotti.id_prodotti = prodotti_categorie.id_prodotto');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti');
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->group_by("categorie.id_categorie");
		$this->db->order_by("categorie.ordine", "ASC");
		$this->db->having('num_prod >', 0);
		
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			if($row->num_prod > 0)
				echo '<li class="filter-category-li"><a href="#" class="filter-category" data-category="'.$row->url_categorie.'">'.$row->nome.'</a> <sup>'.$row->num_prod.'</sup></li>';	
		}
	}
	// print ajax list of available products organized in categories for index page
	public function getProductsCategories()
	{
		$this->db->select('categorie.*, colori_classi.*, COUNT(prodotti.id_prodotti) AS num_prod, COUNT(prodotti_traduzioni.id_prodotti_traduzioni) AS num_trad');
		$this->db->from('categorie');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti', 'prodotti.id_prodotti = prodotti_categorie.id_prodotto', 'LEFT');
		$this->db->join('colori_classi', 'colori_classi.id_colore_classe = categorie.label_color_class', 'LEFT');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti', 'LEFT');
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->group_by("categorie.id_categorie");
		$this->db->order_by("categorie.ordine", "ASC");
		$this->db->having('num_prod >', 0);
		$this->db->having('num_trad >', 0);

		$query = $this->db->get();
		$num_rows = $query->num_rows();
		
		// default classes
		$cat_class_sm = "col-sm-6";
		$cat_class_md = "col-md-4";

		// calculate bootstrap class dinamically
		if($num_rows == 2) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-6";
		} else if($num_rows == 3) {
			$cat_class_sm = "col-sm-4";
			$cat_class_md = "col-md-4";
		} else if($num_rows == 4) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-3";
		} else if($num_rows == 6) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-4";
		} else if($num_rows == 8) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-3";
		}

		// print query
		//print_r($this->db->last_query());
		echo '<h3 align="center">'.lang("HOME_CATEGORY_TITLE").'</h3>';
		foreach ($query->result() as $row)
		{
			//echo $row->nome . ' - ' . $row->immagine;
			echo '<!-- Item -->
			<div class="'.$cat_class_md.' '.$cat_class_sm.'">
			  <div class="shop-item">
				<div class="shop-thumbnail">
					<span class="shop-label '.$row->colore_classe.'">'.$row->nome.'</span>
					<a href="'.base_url().lang('PAGE_SHOP_URL').'/'.$row->url_categorie.'" class="item-link"></a>
					<img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/categories/'.$row->immagine.'" alt="'.SITE_TITLE_NAME.' | '.$row->nome.'">
				</div>
			  </div><!-- .shop-item -->
			</div><!-- .'.$cat_class_md.'.'.$cat_class_sm.' -->';
		}

	}
	// print ajax list of available tags
	public function getFilterTags()
	{
		// header
		echo '<h3 class="widget-title">'. lang("LABEL_TAGS") .'</h3>';
		$this->db->select('*');
		$this->db->from('tags');
		$this->db->join('tags_prodotti', 'tags_prodotti.id_tag = tags.id_tag');
		$this->db->group_by("tags.id_tag");
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			echo '<a href="#" class="tag-a" data-id="'.$row->id_tag.'">'.$row->nome_tag.'</a>';
		}
	}
	/*
		Restituisce la stampa HTML di un prodotto
		css_class: top-rated | text-danger | text-warning | empty ''
	*/
	private function getItemDiv($product)
	{	
		echo '<!-- Item -->
			<div class="col-md-4 col-sm-6">
			  <div class="shop-item">
				<div class="shop-thumbnail" title="'.$product->descrizione.'" >';

		if($product->css_class != '') {
			if($product->css_class != 'top-rated') {
				echo '<span class="shop-label '.$product->css_class.'">'.lang($product->descrizione_tipo_prodotto).'</span>';
			} else {
				// TODO gestire il rating system in base ai commenti degli utenti ?
				echo '<span class="item-rating text-warning">
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star_border"></i>
					  </span>';
			}
		}

	/*	$this->db->select('*');
		$this->db->from('taglie');
		$this->db->join('prodotto_taglia', 'prodotto_taglia.fk_taglia = taglie.id_taglia');
		$this->db->where('fk_prodotto =', $product->id_prodotti);
		$query_taglie = $this->db->get();
			<select class="form-shop" id="size_'.$product->id_prodotti.'">';

						foreach ($query_taglie->result() as $row_taglie)
						{
							echo '<option value="'.$row_taglie->codice.'">'.lang("LABEL_SIZE").': '.$row_taglie->codice.'</option>';
						}

		echo			'</select>
			<a href="#" class="add-to-cart" data-id="'.$product->id_prodotti.'">
					  <em><i class="material-icons add"></i> '.lang("LABEL_ADD_TO_CART").'</em>
					  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
						<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
					  </svg>
					</a>
			*/
			// retrieve available colors
			$this->db->select('nome_colore, codice_colore');
			$this->db->from('varianti_prodotti');
			$this->db->where('id_prodotto', $product->id_prodotti);
			$this->db->where('varianti_prodotti.stato', 1);
			$this->db->join('colori_prodotti', 'varianti_prodotti.id_colore = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->order_by('id_prodotto', "ASC"); 
			$query_colors = $this->db->get();
			
			$num_colors = $query_colors->num_rows();
			if($num_colors > 0) {
				echo '<span class="colors-label"><ul>';
					echo '<li><i class="material-icons fiber_manual_record" style="color:#'.$product->codice_colore.';"></i></li>';
				foreach ($query_colors->result() as $color)
				{
					echo '<li><i class="material-icons fiber_manual_record" style="color:#'.$color->codice_colore.';"></i></li>';
				}
				echo '</ul></span>';
			}
			$detail_link = 	base_url() . lang('PAGE_PRODUCTS_URL') . '/' . $product->codice . '/' . cleanString($product->descrizione_breve . (isset($product->nome_colore) ? ' ' . $product->nome_colore : ''), true);
			echo '<a href="'.$detail_link.'" class="item-link"></a>
				  <img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$product->url_img_grande.'" alt="'.SITE_TITLE_NAME.' | '.$product->nome.' - '.$product->descrizione_breve.'">
				  <div class="shop-item-tools">
					<a href="#" class="add-to-whishlist" data-toggle="tooltip" data-id="'.$product->id_prodotti.'" data-placement="top" title="'.lang("LABEL_ADD_TO_WHISH").'">
					  <i class="material-icons favorite_border"></i>
					</a>
					<a href="'.$detail_link.'" class="add-to-cart show-detail mobile-btn-sm" data-id="'.$product->id_prodotti.'" title="'.lang("LABEL_DETAIL").'">
					  <em><i class="material-icons zoom_in" style="font-size: 22px;"></i> <span class="mobile-hide">'.lang("LABEL_DETAIL").'</span></em>
					  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
						<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
					  </svg>
					</a>
				  </div>
				</div>
				<div class="shop-item-details">
				  <h3 class="shop-item-title"><a href="#">'.$product->nome.'</a></h3>
				  <span class="shop-item-price">';
					echo ($product->prezzo_scontato > 0 ? '<span class="old-price">'.stampaValutaHtml($product->prezzo, true, true).'</span>&nbsp;' . stampaValutaHtml($product->prezzo_scontato, true, true) : stampaValutaHtml($product->prezzo, true, true)).'
				  </span>
				</div>
			  </div><!-- .shop-item -->
			</div><!-- .col-md-4.col-sm-6 -->';
	}
	
	// print detail of single product from SEO URL by prodotti->codice
	public function detailcode($prod_code, $variant_code = NULL)
	{
		// prodotto
		if($variant_code != NULL)
		{
			$this->db->select('p.*,
							tipo_prodotto.*,
							prodotti_traduzioni.*,
							colori_prodotti.*,
							varianti_prodotti.id_variante AS id_variante,
							varianti_prodotti.codice AS codice_variante, 
							varianti_prodotti.url_img_piccola AS url_img_piccola_variante, 
							varianti_prodotti.url_img_grande AS url_img_grande_variante,
							varianti_prodotti.url_img_grande_retro AS url_img_grande_retro_variante,
							varianti_prodotti.id_colore AS id_colore_variante,
							varianti_prodotti.prezzo AS prezzo_variante,
							varianti_prodotti.prezzo_scontato AS prezzo_scontato_variante,
							GROUP_CONCAT(taglie.codice,taglie.separatore,taglie.id_taglia SEPARATOR \'|\') AS taglie');
			$this->db->where('varianti_prodotti.codice', $variant_code);	
			$this->db->where('varianti_prodotti.stato', 1);
			$this->db->join('varianti_prodotti', 'p.id_prodotti = varianti_prodotti.id_prodotto', "LEFT");		
			$this->db->join('colori_prodotti', 'p.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		}
		else
		{
			$this->db->select('p.*, tipo_prodotto.*, colori_prodotti.*, prodotti_traduzioni.*, GROUP_CONCAT(taglie.codice,taglie.separatore,taglie.id_taglia SEPARATOR \'|\') AS taglie');
			$this->db->join('colori_prodotti', 'p.id_colori_prodotti = colori_prodotti.id_colori_prodotti', "LEFT");
		}
		//$this->db->where('categorie.stato', 1);
		$this->db->where('p.stato', 1);
		$this->db->where('p.codice', $prod_code);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		//$this->db->join('categorie', 'p.id_categoria = categorie.id_categorie');
		$this->db->join('tipo_prodotto', 'p.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_traduzioni', 'p.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->join('prodotto_taglia', 'prodotto_taglia.fk_prodotto = p.id_prodotti', "LEFT");
		$this->db->join('taglie', 'taglie.id_taglia = prodotto_taglia.fk_taglia', "LEFT");
		//$this->db->having('taglie !=', '\'\'');
		$this->db->from('prodotti as p');
		$query = $this->db->get();
		
		$product = $query->row();
		
		$curr_id = '';
	
		if (isset($product) && count($product) > 0)
		{
			$curr_id = (isset($product->id_variante) && $product->id_variante > 0 ? $product->codice_variante : $product->codice);
			// categorie
			$this->db->select('*');
			$this->db->from('prodotti_categorie');
			$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
			$this->db->where('categorie.stato', 1);
			$this->db->where('prodotti_categorie.id_prodotto', $product->id_prodotti);
			$query_cat = $this->db->get();
			$data['categories'] = $query_cat->result();
			
			// colore
			$this->db->select('*');
			$this->db->from('colori_prodotti');
			$this->db->where('id_colori_prodotti', (isset($product->id_variante) && $product->id_variante > 0 ? $product->id_colore_variante : $product->id_colori_prodotti));
			$query_color = $this->db->get();
			$color = $query_color->row();
			// prev
			$this->db->select('*');
			$this->db->from('prodotti');
			$this->db->where('prodotti.id_prodotti <', $product->id_prodotti);
			$this->db->where('stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('prodotti.id_prodotti', "DESC"); 
			$this->db->limit(1);
			$query_prev = $this->db->get();
			$prev = $query_prev->row();
			$data['prev'] = $prev;
			// next
			$this->db->select('*');
			$this->db->from('prodotti');
			$this->db->where('prodotti.id_prodotti >', $product->id_prodotti);
			$this->db->where('stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('prodotti.id_prodotti', "ASC"); 
			$this->db->limit(1);
			$query_next = $this->db->get();
			$next = $query_next->row();
			$data['next'] = $next;
			// prodotti figli
			$this->db->select('varianti_prodotti.*, colori_prodotti.*, prodotti_traduzioni.*, GROUP_CONCAT(taglie.codice,taglie.separatore,taglie.id_taglia SEPARATOR \'|\') AS taglie');
			$this->db->from('varianti_prodotti');
			$this->db->where('id_prodotto', $product->id_prodotti);
			$this->db->where('stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->join('colori_prodotti', 'varianti_prodotti.id_colore = colori_prodotti.id_colori_prodotti', "LEFT");
			$this->db->join('prodotti_traduzioni', 'varianti_prodotti.id_prodotto = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->join('variante_taglia', 'variante_taglia.fk_variante = varianti_prodotti.id_variante', "LEFT");
			$this->db->join('taglie', 'taglie.id_taglia = variante_taglia.fk_taglia', "LEFT");
			$this->db->group_by('varianti_prodotti.id_variante'); 
			$this->db->order_by('varianti_prodotti.id_prodotto', "ASC"); 
			$query_child = $this->db->get();
			$data['child_list'] = $query_child->result();
			// print query
			//print_r($this->db->last_query());
			// tags
			$this->db->select('*');
			$this->db->from('tags');
			$this->db->join('tags_prodotti', 'tags_prodotti.id_tag = tags.id_tag');
			$this->db->where('tags_prodotti.id_prodotto =', $product->id_prodotti);
			$query_tags = $this->db->get();
			$data['tags_count'] = $query_tags->num_rows();
			$data['tags'] = $query_tags;
			
			$data['prod'] = $product;
			$data['color'] = $color;
			$data['isvariant'] = (isset($product->id_variante) && $product->id_variante > 0 ? 'true' : 'false');
			$data['prod_curr_id'] = $curr_id;
		//	$data['prod_curr_variant_id'] = (isset($product->id_variante) && $product->id_variante > 0 ? $product->id_variante : '');
			$data['prod_curr_code'] = $product->codice;
			$data['prod_page_title'] = $product->nome;
			$data['prod_page_meta_desc'] = $product->descrizione_breve;
			$this->show_view_with_menu('frontend/detail', $data);	
		} else {
			// se il prodotto richiesto in dettaglio non esiste
			// fullwidth: container-fluid | standard: container	
			//$data = array('productsContainerClass' => 'container-fluid', 'category_url' => '0');		
			//$this->load->view('frontend/shop', $data);
			redirect(site_url(lang('PAGE_SHOP_URL')), 'refresh');
		}	
	}
}

/* End of file Products.php */
/* Location: ./application/controllers/frontend/fe_home.php */
