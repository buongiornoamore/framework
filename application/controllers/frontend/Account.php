<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper(array('url','language'));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth', $this->session->userdata('site_lang'));
		$this->lang->load('ion_auth', $this->session->userdata('site_lang'));
	}

	public function index()
	{

		log_message('info','>>>>>>>>>> Account  index() ' );

		//if (!$this->ion_auth->logged_in() || ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) )
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			//redirect('auth/login', 'refresh');
			log_message('info','>>>>>>>>>> Account  index() > NOT LOGGED ' );
// 			redirect('it/login', 'refresh');
			$data = array();
			$this->show_view_with_menu('frontend/login', $data);
			
		} else {
			//$user = $this->ion_auth->user()->row();
			//echo $user->email;

			$this->load->model('Cliente'); // users (plural) is the table-level model

			$data = array('cliente' => $this->Cliente->get_Cliente_by_User_id($this->ion_auth->user()->row()->id), 
					'ordini' => $this->Cliente->get_Ordini_by_Cliente($this->ion_auth->user()->row()->id),
					'indirizzi_sped' => $this->Cliente->get_Indirizzi_Spedizione_by_Cliente($this->ion_auth->user()->row()->id)
// 					'indirizzi_fatt' => $this->Cliente->get_Indirizzi_Spedizione_by_Cliente($this->ion_auth->user()->row()->id)
			);
			
// 			var_dump($data);
			log_message('info', '>> login : 1 ' );
// 			$this->load->view('frontend/account', $data);
			$this->show_view_with_menu('frontend/account', $data);
		}
	}

	// log the user in
	public function login()
	{
			log_message('info', '>> login : 1 ' );
		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'trim|required');
		$this->form_validation->set_rules('password_login', str_replace(':', '', $this->lang->line('login_password_label')), 'trim|required');

		if ($this->form_validation->run() == true)
		{
			log_message('info', '>> login : 2 ' );
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password_login'), $remember))
			{
				log_message('info', '>> login : 3 $this->ion_auth->get_user_id() ' . $this->ion_auth->get_user_id() );

				// Login corretto carico dati cliente
				$this->db->select('*');
				$this->db->from('clienti');
				$this->db->where('user_id', $this->ion_auth->get_user_id());
				$query_cliente = $this->db->get();
				$this->session->set_userdata('id_cliente', $query_cliente->row()->id_cliente);
				
				// Verifico ed effettuo il merge del carrello temporaneo se esiste
				// recupero uuid dal cookie
				$user_uuid = $this->input->cookie('uuid', TRUE);
				
				$data_merge = array(
				   'id_cliente' => $query_cliente->row()->id_cliente
				);
				$this->db->where('id_sessione_utente', $user_uuid);
				$this->db->update('carrello', $data_merge);
				
				$this->load->model('Cliente');
				$cliente = $this->Cliente->get_Cliente_by_User_id($this->ion_auth->get_user_id());
				
				if($cliente->indirizzo = null || $cliente->indirizzo = ''){
					echo ( '1login' );
				}
				echo ( 'ok' );				
			}
			else
			{
				log_message('info', '>> login : 4 ' );
				// Se la login fallisce torna alla pagina login
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				echo (  $this->ion_auth->errors() );
			}
		}
		else
		{
			log_message('info', '>> login : 5 ' );
			$this->session->set_flashdata('message', $this->lang->line('login_subheading'));
			
			if( $this->lang->line('login_subheading') != '' ){
				echo (  $this->lang->line('login_subheading') );
			}else{
				$data = array();
				//				redirect('login');
				$this->show_view_with_menu('frontend/login', $data);
			}
			
// 			echo (  $this->lang->line('login_subheading') );
		}
	}

	// log the user out
	public function logout()
	{
		// keep language info
		$site_lang = $this->session->userdata('site_lang');
		// log the user out
		$logout = $this->ion_auth->logout();

		$data = array();
		
		$this->session->unset_userdata('id_cliente');
		$this->session->set_userdata('site_lang', $site_lang);
		$this->show_view_with_menu('frontend/login', $data);
	}

	// create a new user
	public function register()
    {
    	log_message('info','>> Account extends CI_Controller >> register() - IN ');
		
        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required|min_length[3]|max_length[15]');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required|min_length[3]|max_length[15]');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('email',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }

				$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

				$form_validated = false;
        if ($this->form_validation->run() == true)
        {
			log_message('info', '>>> Account >> register : validazione ok!');
			$form_validated = true;
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('email');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
				'nome' => $this->input->post('first_name'),
				'cognome' => $this->input->post('last_name'),
            );
        }

				//log_message('info','>> Account extends CI_Controller >> register() : 1 '.($this->ion_auth->register($identity, $password, $email, $additional_data)));

        if ($form_validated == true)
        {
					$return_id = $this->ion_auth->register($identity, $password, $email, $additional_data);
					if($return_id){

					log_message('info','>> Account extends CI_Controller >> register() : ' . $return_id . ' - ID');

						//$this->ion_auth->set_hook('post_register', 'insert_cliente', $this, '_insert_cliente', $additional_data);

						$cliente_data = array(
								'nome' => $additional_data['first_name'],
								'cognome' => $additional_data['last_name'],
							//	'email' => $email,
								'user_id' => $return_id,
						);

						$this->db->insert('clienti', $cliente_data);
						$id_cliente = $this->db->insert_id();
						
						$this->session->set_userdata( 'id_cliente', $id_cliente );
						
						$this->db->select('*');
						$this->db->from('contatti_newsletter');
						$this->db->where('email_contatto', $email);
						$contatti_newsletter = $this->db->get();
						
						$row = $contatti_newsletter->row();
						if( !isset($row) ){
						
							$newsletter_data = array(
									'id_contatto_newsletter' => $id_cliente,
									'email_contatto' => $email,
									'data_contatto' => date('Y-m-d H:i:s'),
									'stato_contatto' => 1,
									'lingua_traduzione_id' => 1,
							);
							
							$this->db->insert('contatti_newsletter', $newsletter_data);
						}else{
							$newsletter_data = array(
									'stato_contatto' => 1,
							);
							$this->db->where('email_contatto', $email);
							$this->db->update('contatti_newsletter', $newsletter_data);
						}
						
			            // check to see if we are creating the user
			            // redirect them back to the admin page
			            $this->session->set_flashdata('register_message', $this->ion_auth->messages());
			            
			            // Senza chiamata AJAX
			//             $this->index();
			
			            // Con chiamata AJAX
			            //echo (  $this->ion_auth->messages() );  //$this->lang->line('account_creation_successful');


					}
					log_message('info','>> Account extends CI_Controller >> register() : out ' . $return_id . ' - ID');
					$result = array( 'return_id' => $id_cliente, 'messaggio' => $this->ion_auth->messages() );
					
					// invio email template welcome al cliente
					$this->send_email_default_template(true, 'welcome', $email, true, false, true);
					
					echo json_encode($result);
					
        }

				if($form_validated == false)
        {
						log_message('info','>> Account extends CI_Controller >> register() : 2 ');
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			log_message('info','>> Account extends CI_Controller >> register() : 2 ' .   $this->data['message'] . count($this->data['message']) . '<<');
            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            //$this->_render_page('auth/create_user', $this->data);

			//$this->session->set_flashdata('message', $this->ion_auth->messages());
			$this->session->set_flashdata('register_message', $this->data['message']);
						
			// Senza chiamata AJAX
// 			$data = array();
//			redirect('login',  $this->data);
// 			$this->show_view_with_menu('frontend/login',  $data);
			
// 			            $this->index();
			
			// Con chiamata AJAX
// 			echo ( $this->data['message'] . $this->ion_auth->messages() );
			$result = array( 'return_id' => '', 'messaggio' => ($this->data['message'] . $this->ion_auth->messages()) );
			echo json_encode($result);

        }
    }

		// Salvataggio dei dati dell'account
		public function _insert_cliente($additional_data)
		{
			log_message('info','>> Account extends CI_Controller >> register() : salvaDatiAccount - ID');
			$user = $this->ion_auth->user()->row();
			$user_id = $user->id;

			$cliente_data = array(
					'nome' => $additional_data['first_name'],
					'cognome' => $additional_data['last_name'],
					'user_id' => $user_id,
			);

			$this->db->insert('clienti', $cliente_data);
		}

		// Salvataggio dei dati dell'account
		public function salvaDatiProfilo()
		{

				$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
				$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');

				// TODO
				// $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
				// $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');

				$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
				$this->form_validation->set_rules('telefono', $this->lang->line('create_user_validation_phone_label'), 'trim');

				// indirizzo fatturazione
				$data_indirizzo_fatturazione = array(
					'indirizzo_fatt' => $this->input->post('indirizzo'),
					'civico_fatt' => $this->input->post('civico'),
					'cap_fatt' => $this->input->post('cap'),
					'nazione_fatt' => $this->input->post('nazione'),
					'citta_fatt' => $this->input->post('citta'),
					'riferimento_fatt' => $this->input->post('riferimento'),
					'note_fatt' => $this->input->post('note'),
							
				);

				$this->db->trans_start();

				$user_id = $this->ion_auth->user()->row()->id;
				
				$this->db->select('*');
				$this->db->from('clienti');
				$this->db->where('user_id', $user_id); 
				
				$query_cliente = $this->db->get();
				$cliente = $query_cliente->row();
				
				$indirizzo_fatturazione = $this->db->get_where('indirizzo_fatturazione', array( 'id_indirizzo_fatturazione' => $cliente->id_indirizzo_fatturazione ) )->row();
				
				$id_indirizzo_fatturazione;
				log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> empty($indirizzo_fatturazione)   ' . empty($indirizzo_fatturazione));
				if(empty($indirizzo_fatturazione)){
					log_message('info','>> Accpount >> salvaDatiProfilo >> empty($indirizzo_fatturazione)  ' );
					$this->db->insert('indirizzo_fatturazione', $data_indirizzo_fatturazione);
					$id_indirizzo_fatturazione = $this->db->insert_id();
				
				}else{
					log_message('info','>> Accpount >> salvaDatiProfilo >> !empty($indirizzo_fatturazione->id_indirizzo_fatturazione)  '  );
					$this->db->from('indirizzo_fatturazione');
					$this->db->where('id_indirizzo_fatturazione', $indirizzo_fatturazione->id_indirizzo_fatturazione);
					$this->db->update('indirizzo_fatturazione', $data_indirizzo_fatturazione);
					$id_indirizzo_fatturazione = $indirizzo_fatturazione->id_indirizzo_fatturazione;
				}
				

				$data_cliente = array(
						'nome' => $this->input->post('first_name'),
						'cognome' => $this->input->post('last_name'),
						'email' => $this->input->post('email'),
						'telefono' => $this->input->post('telefono'),
						'partita_iva' => $this->input->post('partita_iva'),
						'codice_fiscale' => $this->input->post('codice_fiscale'), // da fare controlli validazione
						'newsletter' => $this->input->post('newsletter'),
						'id_indirizzo_fatturazione' => $id_indirizzo_fatturazione,
				);
				
				log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> $user_id   ' . $user_id);
				$this->db->from('clienti');
				$this->db->where('user_id', $user_id);
				$this->db->update('clienti', $data_cliente);

				$cliente = $this->db->get_where('clienti', array('user_id' => $user_id))->row();



				//$id_indirizzo = $this->db->insert_id();

				//$this->db->where('user_id', $user_id);

				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{
				        // generate an error... or use the log_message() function to log your error
								// TODO
				}

				// Produces:
				// UPDATE mytable
				// SET title = '{$title}', name = '{$name}', date = '{$date}'
				// WHERE id = $id
				
				// metti cliente in sessione
				$this->session->set_userdata('id_cliente', $cliente->id_cliente);
				
//				redirect("account");
//  				$this->show_view_with_menu('frontend/account', '');
				$this->index();

		}
		
		// Salvataggio dei dati dell'account
		public function salvaIndirizzoSpedizione()
		{

			$this->form_validation->set_rules('indirizzo_sped', $this->lang->line('create_user_validation_fname_label'), 'required');
			$this->form_validation->set_rules('citta_sped', $this->lang->line('create_user_validation_lname_label'), 'required');

			// TODO
			// $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
			// $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');

			$this->form_validation->set_rules('cap_sped', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
			$this->form_validation->set_rules('nazione_sped', $this->lang->line('create_user_validation_phone_label'), 'trim');

			$this->db->trans_start();

			$user_id = $this->ion_auth->user()->row()->id;

			$cliente = $this->db->get_where('clienti', array('user_id' => $user_id))->row();

			$data_indirizzo_spedizione = array(
					'civico_sped' => $this->input->post('civico_sped'),
					'cap_sped' => $this->input->post('cap_sped'),
					'nazione_sped' => $this->input->post('nazione_sped'),
					'citta_sped' => $this->input->post('citta_sped'),
					'indirizzo_sped' => $this->input->post('indirizzo_sped'),
					'note_sped' => $this->input->post('note_sped'),
					'id_cliente' => $cliente->id_cliente,
					'riferimento_sped' => $this->input->post('riferimento_sped'),
			);
			
			log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ' . $this->input->post('id_indirizzo_sped') );
			if($this->input->post('id_indirizzo_sped') == ''){
				log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> INSERT <<<<' );
				$this->db->insert('indirizzo_spedizione', $data_indirizzo_spedizione);
			}else {
				log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> UPDATE <<<<' );
				$this->db->from('indirizzo_spedizione');
				$this->db->where('id_indirizzo_spedizione', $this->input->post('id_indirizzo_sped'));
				$this->db->update('indirizzo_spedizione', $data_indirizzo_spedizione);
			}
			
			$this->db->trans_complete();
		
			if ($this->db->trans_status() === FALSE)
			{
				// generate an error... or use the log_message() function to log your error
				// TODO
			}

//				redirect("account");
// 			$this->show_view_with_menu("frontend/account",$data);
			$this->index();
		
		}
		
		// Salvataggio dei dati dell'account
		public function salvaIndirizzoFatturazione()
		{
		
// 			$this->form_validation->set_rules('indirizzo_fatt', $this->lang->line('create_user_validation_fname_label'), 'required');
// 			$this->form_validation->set_rules('civico_fatt', $this->lang->line('create_user_validation_lname_label'), 'required');
// 			$this->form_validation->set_rules('citta_fatt', $this->lang->line('create_user_street_number_label'), 'required');
		
			// TODO
			// $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
			// $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
		
// 			$this->form_validation->set_rules('cap_fatt', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
// 			$this->form_validation->set_rules('nazione_sped', $this->lang->line('create_user_validation_phone_label'), 'trim');
		
			$this->db->trans_start();
		
// 			$cliente = $this->db->get_where('clienti', $this->input->post('id_cliente_fatt'))->row();
		
			$data_indirizzo_fatturazione = array(
					'indirizzo_fatt' => $this->input->post('indirizzo'),
					'civico_fatt' => $this->input->post('cap'),
					'citta_fatt' => $this->input->post('citta'),
					'cap_fatt' => $this->input->post('cap'),
					'nazione_fatt' => $this->input->post('nazione'),
					'note_fatt' => $this->input->post('note'),
					'riferimento_fatt' => $this->input->post('citta'),
			);
				
			$this->db->insert('indirizzo_fatturazione', $data_indirizzo_fatturazione);
			
			$id_indirizzo_fatt = $this->db->insert_id();
			
			$this->db->set('id_indirizzo_fatturazione', $id_indirizzo_fatt);
			$this->db->where('id_cliente', $this->input->post('id_cliente_fatt'));
			$this->db->update('clienti');			
			
			$this->db->trans_complete();
		
			if ($this->db->trans_status() === FALSE)
			{
				// generate an error... or use the log_message() function to log your error
				// TODO
			}
		
			$this->index();
		
		}
		
		public function dettaglio_ordine()
		{
		
			$id_ordine = $_POST['id_ordine'];
			
			log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> $id_ordine in ' . $id_ordine);
			
// 			$this->db->select('*');
// 			$this->db->from('storico_carrello');
// 			$this->db->where('id_ordine', $id_ordine);
			
			
// 			$query_dettaglio_ordine = $this->db->get();
// 			$dettaglio_ordine = $query_dettaglio_ordine->result();
			
			$this->load->model('Cliente');
			
			$dettaglio_ordine = $this->Cliente->get_storico_carrello_by_Ordine($id_ordine);
			
			echo '<table class="table table-striped table-responsive">'
					. '<thead><tr>
                      		<th>Codice</th>
                      		<th>Nome</th>
                      		<th>Tipo Prodotto</th>
                      		<th>Colore Prodotto</th>
							<th>Taglia</th>
							<th>Quantit&agrave;</th>
                    		</tr></thead>
                  		<tbody>';
			//var_dump($dettaglio_ordine);
				foreach( $dettaglio_ordine as $row )
				{	
					log_message('info','>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> $id_ordine foreach' . $id_ordine);
						echo '<tr>
							  <td>' . $row->codice . '</td>
							  <td>' . $row->nome . '</td>
							  <td>' . lang($row->tipo_prodotto) . '</td>
    					  	  <td>' . $row->colore_prodotto . '</td>
    					  	  <td>' . $row->descrizione . '</td>
    					  	  <td>' . $row->qty . '</td>
						 	  </tr>
    					  	  <tr>
    					  	  <td>' . ' Spedito:  ' . '</td>
							  <td>' . ( isset($row->indirizzo_sped) ? $row->indirizzo_sped : $row->indirizzo_fatt ) . '</td>
							  <td>' . ( isset($row->civico_sped) ? $row->civico_sped : $row->civico_fatt ) . '</td>
							  <td>' . ( isset($row->cap_sped) ? $row->cap_sped :$row->cap_fatt ) . '</td>
    					  	  <td>' . ( isset($row->citta_sped) ? $row->citta_sped : $row->citta_fatt ) . '</td>
    					  	  <td>' . ( isset($row->nazione_sped) ? $row->nazione_sped : $row->nazione_fatt ) . '</td>
						 	  </tr>' 
						;
				}

			echo '</tbody></table>';
		
		}

		//Working code for this example is in the example Auth controller in the github repo
		public function _forgot_password()
		{
			$this->form_validation->set_rules('identity', 'Email Address', 'required');
			if ($this->form_validation->run() == false) {
				//setup the input
				$this->data['identity'] = array('name'    => 'email',
											 'id'      => 'email',
											);
				//set any errors and display the form
				$data = array();
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				//$this->load->view('auth/forgot_password', $this->data);
				//$this->show_view_with_menu('frontend/login', $data);
				$result = array( 'messaggio' => $this->session->flashdata('message') );
				echo json_encode($result);
			}
			else {
				//run the forgotten password method to email an activation code to the user
				$forgotten = $this->ion_auth->frontend_forgotten_password($this->input->post('identity'));

				if ($forgotten) { //if there were no errors
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					//redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
					$result = array( 'messaggio' => $this->ion_auth->messages() );
					echo json_encode($result);
				}
				else {
					$this->session->set_flashdata('message', $this->ion_auth->errors());
// 					redirect(lang('PAGE_FORGOT_PASSWORD_URL'), 'refresh');
					$result = array( 'messaggio' => $this->ion_auth->errors() );
					echo json_encode($result);
				}
			}
		}

// *********************************************************************************************

		// forgot password
		public function forgot_password()
		{
		
			// setting validation rules by checking whether identity is username or email
			if($this->config->item('identity', 'ion_auth') != 'email' )
			{
				$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
			}
			else
			{
				$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
			}
		
		
			if ($this->form_validation->run() == false)
			{
				$this->data['type'] = $this->config->item('identity','ion_auth');
				// setup the input
				$this->data['identity'] = array('name' => 'identity', 'id' => 'identity',
				);
		
				if ( $this->config->item('identity', 'ion_auth') != 'email' ){
		
					$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
				}
				else
				{
		
					$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
				}
		
				// set any errors and display the form
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		
				// $this->_render_page('auth/forgot_password', $this->data);
				//$this->load->view('admin/login', $this->data);
				$result = array( 'messaggio' => $this->data['message'] );
				echo json_encode($result);
				return ;
			}
			else
			{
				$identity_column = $this->config->item('identity','ion_auth');
				$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();
		
				if(empty($identity)) {
		
					if($this->config->item('identity', 'ion_auth') != 'email')
					{
		
						$this->ion_auth->set_error('forgot_password_identity_not_found');
					}
					else
					{
		
						$this->ion_auth->set_error('forgot_password_email_not_found');
					}
		
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					//redirect("auth/login", 'refresh');
					$result = array( 'messaggio' => $this->ion_auth->errors() );
					echo json_encode($result);
					return ;
				}
		
				// run the forgotten password method to email an activation code to the user
				$forgotten = $this->ion_auth->frontend_forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
		
				if ($forgotten)
				{
		
					// if there were no errors
					$this->session->set_flashdata('message', $this->ion_auth->messages());
					//redirect("admin/login", 'refresh'); //we should display a confirmation page here instead of the login page
					$result = array( 'messaggio' => $this->ion_auth->messages() );
					echo json_encode($result);
				}
				else
				{
		
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					//redirect("admin/login", 'refresh');
					$result = array( 'messaggio' => $this->ion_auth->messages() );
					echo json_encode($result);
				}
			}
		}
		
// *********************************************************************************************
		// reset password - final step for forgotten password
		public function reset_password($code = NULL)
		{
			if (!$code)
			{
				show_404();
			}
		
			$user = $this->ion_auth->forgotten_password_check($code);
		
			if ($user)
			{
		
				log_message('info','********************* reset_password ** 1 ');
				// if the code is valid then display the password reset form
				$this->form_validation->set_rules('password', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');
		
				if ($this->form_validation->run() == false)
				{
					log_message('info','********************* reset_password ** 2 ');
					// display the form
		
					// set the flash data error message if there is one
					$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
		
					$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
					$this->data['password'] = array(
							'name' => 'password',
							'id'   => 'password',
							'type' => 'password',
							'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
					);
					$this->data['password_confirm'] = array(
							'name'    => 'password_confirm',
							'id'      => 'password_confirm',
							'type'    => 'password',
							'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
					);
					$this->data['user_id'] = array(
							'name'  => 'user_id',
							'id'    => 'user_id',
							'type'  => 'hidden',
							'value' => $user->id,
					);
					$this->data['csrf'] = $this->_get_csrf_nonce();
					// $this->data['csrf'] = array('csrf' => $this->_get_csrf_nonce());
					$this->data['code'] = $code;
		
					// render
					$this->load->view('frontend/resetPassword', $this->data);
				}
				else
				{
					log_message('info','********************* reset_password ** 3 ');
					// do we have a valid request?
					if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
					{
		
						// something fishy might be up
						$this->ion_auth->clear_forgotten_password_code($code);
		
						show_error($this->lang->line('error_csrf'));
		
					}
					else
					{
						log_message('info','********************* reset_password ** 3 ');
						// finally change the password
						$identity = $user->{$this->config->item('identity', 'ion_auth')};
		
						$change = $this->ion_auth->reset_password($identity, $this->input->post('password'));
		
						if ($change)
						{
							// if the password was successfully changed
							$this->session->set_flashdata('message', $this->ion_auth->messages());
							// redirect("admin/login", 'refresh');
							//$this->load->view('frontend/login', $this->data);
							$this->index();
						}
						else
						{
							$this->session->set_flashdata('message', $this->ion_auth->errors());
							// redirect('admin/resetPassword' . $code, 'refresh');
							//$this->load->view('frontend/login', $this->data);
							$this->index();
						}
					}
				}
			}
			else
			{
				// if the code is invalid then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
		
				// redirect("admin/login", 'refresh');
				$this->load->view('frontend/login');
		
			}
		}
		
		public function _get_csrf_nonce()
		{
			$this->load->helper('string');
			$key   = random_string('alnum', 8);
			$value = random_string('alnum', 20);
			$this->session->set_flashdata('csrfkey', $key);
			$this->session->set_flashdata('csrfvalue', $value);
		
			return array($key => $value);
		}
		
		
		public function _valid_csrf_nonce()
		{
			$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
			if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
}



/* End of file Account.php */
/* Location: ./application/controllers/frontend/account.php */
