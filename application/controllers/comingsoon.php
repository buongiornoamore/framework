<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class comingsoon extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();   
	}
	/**
	 * Il nome del file deve essere in minuscolo comingsoon.php
	 */
	public function index()
	{
		$this->load->view('comingsoon');
	}
	
}

/* End of file Comingsoon.php */
/* Location: ./application/controllers/Comingsoon.php */