<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ITALIANO
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_SHOP'] = "Negozio";
$lang['MENU_ABOUT'] = "Chi siamo";
$lang['MENU_SHIPPING'] = "Spedizioni";
$lang['MENU_RULES'] = "Regolamento";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_CONTACTS'] = "Contatti";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Abbigliamento artistico";
$lang['PAGE_HOME_META_DESCRIPTION'] = "Benvenuti sul sito di Ma Chlò vendita di abbigliamento di qualità basato su lavori artistici con spedizione in tutto il mondo.";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
/* SHOP */
$lang['PAGE_SHOP_CODE'] = "SHOP";
$lang['PAGE_SHOP_TITLE'] = "Negozio";
$lang['PAGE_SHOP_META_DESCRIPTION'] = "Qui puoi scoprire tutti i nostri prodotti, le taglie e i colori disponibili per portare le nostre opere a casa tua.";
$lang['PAGE_SHOP_DESCRIPTION'] = "";
$lang['PAGE_SHOP_IMAGE'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "Chi siamo";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte.";
$lang['PAGE_ABOUT_DESCRIPTION'] = "Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte. Ma Chlò, infatti, non propone delle stampe qualsiasi ma delle vere e proprie opere d’arte uniche e originali che con i loro soggetti e fantasie raffigurano la natura in tutte le sue forme e colori, qualcosa di unico che prende ispirazione anche dai profumi, dalla magia e dai paesaggi della Sardegna. Ogni quadro è ispirato ad un viaggio, un vissuto, un’emozione, un’esperienza che l’artista ha voluto esprimere su tela e nella quale ogni donna può immedesimarsi.<br><br>
Oltre alla natura sotto forma di animali, alberi, fiori, Ma Chlò propone come linea principale “Le maschere” che rappresentano le varie tappe di una storia d’amore. «L’idea delle maschere» racconta l’artista «nasce da una passeggiata lungo l’ultimo chilometro e mezzo rimasto in piedi del muro di Berlino.<br><br>Rimasi affascinata da un murales che rappresentava due facce divise da qualcosa di importante, decisi di riportarle su tela giocando con i colori e dopo il primo disegno capii che quella doveva diventare una storia, la storia di un amore che nessun muro avrebbe più potuto fermare». Una storia, dunque, articolata in diverse fasi, l’incontro, lo sguardo, il bacio, la separazione, il silenzio, la rottura, l’infinito, un nuovo inizio che Ma Chlò vuole far indossare ad ogni donna come espressione del proprio vissuto.<br><br>
Per iniziare Ma Chlò propone nella sua linea di abbigliamento t-shirt e vestiti con l’obiettivo di ampliare la sua gamma di prodotti in un’ottica di crescita continua.<h3>Mission</h3>La nostra mission è creare un punto di incontro tra moda e arte.<br>Ciò che ci contraddistingue è l’originalità ed il significato delle stampe.";
$lang['PAGE_ABOUT_IMAGE'] = "about.jpg";
/* SHIPPING */
$lang['PAGE_SHIPPING_CODE'] = "SHIPPING";
$lang['PAGE_SHIPPING_TITLE'] = "Spedizioni e consegne";
$lang['PAGE_SHIPPING_META_DESCRIPTION'] = "La spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine.";
$lang['PAGE_SHIPPING_DESCRIPTION'] = "<h3>Regole di spedizione e costi</h3>
La spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine, se invece si desidera una spedizione espressa i tempi di consegna sono di 3/5 giorni lavorativi con un costo a carico del cliente di 20 euro.";
$lang['PAGE_SHIPPING_IMAGE'] = "99fc4-blog.jpg";
/* RULES */
$lang['PAGE_RULES_CODE'] = "RULES";
$lang['PAGE_RULES_TITLE'] = "Regolamento e condizioni";
$lang['PAGE_RULES_META_DESCRIPTION'] = "Ma Chlò è marchio registrato, scopri come funziona e quale è il regolamento del servizio.";
$lang['PAGE_RULES_DESCRIPTION'] = "Ma Chlò è marchio registrato, tutti i suoi contenuti, quali, a titolo esemplificativo, le immagini, le fotografie, le opere, i disegni, le figure, i loghi ed ogni altro materiale, in qualsiasi formato, pubblicato su machlo.com , compresi i menu, le pagine web, la grafica, i colori, gli schemi, gli strumenti, i caratteri ed il design del sito web, il layouts, i metodi, i processi, le funzioni ed il software che fanno parte di Ma Chlò , sono protetti dal diritto d\\\'autore e da ogni altro diritto di proprietà intellettuale del Gestore e degli altri titolari dei diritti. È vietata la riproduzione, in tutto o in parte, in qualsiasi forma, di Ma Chlò senza il consenso espresso in forma scritta del Gestore.<br><br> Ma Chlò garantisce un utilizzo dei dati personali strettamente legato all’erogazione dei propri servizi, alla gestione del sito e all’evasione degli ordini e non verranno in alcun modo venduti a terzi.<br><br> Ma Chlò non può garantire ai propri utenti che le misure adottate per la sicurezza del sito e della trasmissione dei dati e delle informazioni sul sito siano in grado di limitare o escludere qualsiasi rischio di accesso non consentito o di dispersione dei dati da parte di dispositivi di pertinenza dell’utente. Per tale motivo, suggeriamo agli utenti del sito di assicurarsi che il proprio computer sia dotato di software adeguati per la protezione della trasmissione in rete di dati (ad esempio antivirus aggiornati) e che il proprio Internet provider abbia adottato misure idonee per la sicurezza della trasmissione di dati in rete.<br><br>
<h3>Pagamenti</h3>
Il cliente puo’ scegliere il metodo di pagamento indicato al momento dell’acquisto nel modulo d’ordine.<br> Tutti i nostri prodotti sono made in Usa, i prezzi e le transazioni sono in Euro, eventuali cambi valuta, dazi doganali, commissioni su pagamenti con carta di credito e Paypal sono a carico del cliente.<br><br> Le informazioni relative all’esecuzione della transazione saranno inviate agli enti responsabili ( Circuito Visa/Mastercard, PayPal )  tramite protocollo crittografato, senza che terzi possano aver accesso in alcun modo. Le informazioni non saranno mai visualizzate o memorizzate da parte di Ma Chlò.<br><br>
<h3>Reso</h3>
Il cliente ha 14 giorni per esercitare il diritto di recesso dalla data di ricezione della merce e la spedizione di restituzione  sarà a carico del cliente.<br><br> Non è  possibile cambiare il prodotto scelto con un altro.<br> Per richiedere l’autorizzazione al reso, accedere alla sezione contatti e scrivere un messaggio di posta elettronica  all’indirizzo info@machlo.com  con indicazioni del prodotto.<br><br> Il Diritto di Recesso si intende esercitato correttamente qualora siano interamente rispettate anche le seguenti condizioni:<br> •	I prodotti non devono essere stati danneggiati, indossati, lavati e non devono presentare nessun segno d’uso.<br> •	I resi devono essere spediti all’interno della confezione Ma Chlò entro 14 giorni dalla data di comunicazione, da parte del cliente, del diritto di recesso.<br><br> Il rimborso sarà eseguito con lo stesso mezzo da te utilizzato per il pagamento dopo aver ricevuto il prodotto reso.<br> Ma Chlò si riserva inoltre il diritto di rifiutare resi non autorizzati o comunque non conformi a tutte le condizioni previste.";
$lang['PAGE_RULES_IMAGE'] = "";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Scopri in che modo vengo trattati i tuoi dati sensibili, l\'utilizzo dei cookie e dei dati di sessione.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<h3>Cookie policy</h3>
In questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio già richiesto dall\'utente come il carrello degli acquisti. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell\'utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l\\\'utente all\\\'interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l\\\'efficienza del nostro portale.<br><br>Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br> In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L\\\'anonimato dell\\\'utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br><br> Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br> Non è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.<br><br> Tutti i dati inseriti dai nostri clienti all\\\'interno di moduli, carrello, ordini e procedure di pagamento verranno utilizzati esclusivamente per gli ordini e le consegne degli stessi. Per questo motivo i clienti sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sarà possibile utilizzare il nostro servizio.<br> In particolare i dati utilizzati per i pagamenti online non verranno assolutamente trattati e/o memorizzati sui nostri sistemi in quanto passati direttamente ai servizi di pagamento Stripe e/o Paypal tramite transazione sicura SSL (HTTPS).<br>Al momento dell\\\'ordine, della richiesta di preventivo o informazioni, l\\\'indirizzo email dell\\\'utente verrà inserito nel nostro sistema di newsletter e informazioni ai clienti da cui sarà sempre possibile disiscriversi facilmente tramite il link presente in ogni comunicazione inviata.<br> In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.";
$lang['PAGE_PRIVACY_IMAGE'] = "";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contatti e recapiti";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "Puoi contattare Ma Chlò in tantissimi modi diversi: email, telefono e socials.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Galleria immagini";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "Ecco una galleria delle nostre immagini più belle.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
