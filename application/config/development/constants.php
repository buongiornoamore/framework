<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Global frmework */
//define('SITE_URL_PATH',		               	'http://my.framework.com'); #Dev Maurizio
/* SITE_URL_ROOT è definita nella ROOT index.php */
define('SITE_URL_PATH',		               		'http://localhost/framework');
define('SITE_TITLE_NAME',		               	'Framework dev');
/* Email smtp global vars */
define('SMTP_HOST_CUSTOM',		                '');
define('SMTP_USER_CUSTOM',		                '');
define('SMTP_PASS_CUSTOM',		                '');
/* Database MySql global vars */
define('DB_HOSTNAME',	    	                'localhost');
define('DB_USERNAME',	    	                'root');
define('DB_PASSWORD',	    	                '');
define('DB_DATABASE',	    	                'framework');
/* Company global vars */
define('COMPANY_NAME',		                    'Framework');
define('COMPANY_EMAIL',		                    'info@framework.com');
define('COMPANY_COPYRIGHT',		                '&copy; ' . date("Y") . ' ' . COMPANY_NAME);
define('COMPANY_PHONE',		                    ' +39 1154454545454');
define('COMPANY_ADDRESS',		                'Roma');
define('GPLUS_LINK',		                    '');
define('YOUTUBE_LINK',		                    '');
define('PINTEREST_LINK',		                '');
define('TWITTER_LINK',		                    '');
define('FACEBOOK_LINK',		                    '');
define('INSTAGRAM_LINK',		                '');
define('GOOGLE_ANALITYCS_ID',		            '00000000');
define('PIXEL_FACEBOOK_ID', '00000000');
define('PERC_PUNTI_RETURN',		                '10');
/* Coming soon OPTIONS */
define('COMNINGSOON_LOGO',		                'logo.png');
define('COMNINGSOON_BACK_IMAGE',		        'default.jpg');
define('COMNINGSOON_BTN_COLOR',		            'EE2D20');
/* Payments info */
define('PAYPAL_EMAIL',		                    '');
define('PAYPAL_ENV',		                    'sandbox');
define('PAYPAL_SANDBOX_CLIENT_ID',		        '');
define('PAYPAL_LIVE_CLIENT_ID',		            '');
define('STRIPE_PK',	    	                    '');
define('STRIPE_SK',		                        '');
/* FLAGS */
define('POINTS_ENABLED',		                '0');
// Genearl constants
require_once(APPPATH . 'config/constants_general.php');
/* End of file constants.php */
/* Location: ./application/config/constants.php */