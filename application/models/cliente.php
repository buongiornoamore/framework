<?
class Cliente extends CI_Model {

	public $id_cliente;
	public $cognome = '';
	public $nome = '';
	public $data_nascita = '';
	public $partita_iva = '';
	public $codice_fiscale = '';
	public $email = '';
	public $telefono_fisso = '';
	public $cellulare = '';
	public $user_id;
	public $fax = '';


	public function get_Cliente_by_User_id($user_id)
	{

		log_message('info', '>> get_Cliente_by_User_id : 1 ' );

		$this->db->select('*');
		$this->db->from('clienti');
		$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = clienti.id_indirizzo_fatturazione', 'left');
		$this->db->where('user_id', $user_id);

		log_message('info', '>> get_Cliente_by_User_id : 2 ' . $user_id );

		$query = $this->db->get();
		$cliente_info = $query->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );

		return $cliente_info;
		
	}

	public function get_Ordini_by_Cliente($user_id)
	{

		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );

		$this->db->select('id_cliente');
		$this->db->from('clienti');
		$this->db->where('user_id', $user_id);

		log_message('info', '>> get_Ordini_by_Cliente : 2 ' . $user_id );

		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );

		
		$this->db->select('*');
		$this->db->from('ordini');
		$this->db->join('stato_ordine', 'stato_ordine.id_stato_ordine = ordini.stato_ordine');
		$this->db->join('stato_pagamento', 'stato_pagamento.id_stato_pagamento = ordini.stato_pagamento');
		$this->db->where('id_cliente', $cliente->id_cliente);
		
		$query_ordine = $this->db->get();
		
		
		return $query_ordine->result();


	}

	public function get_storico_carrello_by_Ordine($id_ordine)
	{
	
		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );
	
		$this->db->select('storico_carrello.*, prodotti.*, taglie.*, colori_prodotti.*, indirizzo_spedizione.*, indirizzo_fatturazione.*');
		$this->db->from('storico_carrello');
		$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
		$this->db->join('taglie', 'taglie.id_taglia = storico_carrello.taglia');
		$this->db->join('colori_prodotti', 'colori_prodotti.id_colori_prodotti = storico_carrello.colore_prodotto_id', 'left');
		$this->db->join('ordini', 'ordini.id_ordine = storico_carrello.id_ordine');
		$this->db->join('indirizzo_spedizione', 'indirizzo_spedizione.id_indirizzo_spedizione = ordini.id_indirizzo_spedizione', 'left');
		$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = ordini.id_indirizzo_fatturazione_spedizione', 'left');
		$this->db->where('storico_carrello.id_ordine', $id_ordine);
	
		$query_ordine = $this->db->get();
	
	
		return $query_ordine->result();
	
	
	}
	
	public function get_Indirizzi_Spedizione_by_Cliente($user_id)
	{
	
		log_message('info', '>> get_Ordini_by_Cliente : 1 ' );
	
		$this->db->select('id_cliente');
		$this->db->from('clienti');
		$this->db->where('user_id', $user_id);
	
		log_message('info', '>> get_Ordini_by_Cliente : 2 ' . $user_id );
	
		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row(); // ORIGINALE : $user_info = $query->row();
		//log_message('info', '>>>>>>>>>> Cliente  >> get_Cliente_by_User_id >  ' . var_dump($cliente_info) );
	
	
		$this->db->select('*');
		$this->db->from('indirizzo_spedizione');
		$this->db->where('indirizzo_spedizione.id_cliente', $cliente->id_cliente);
	
		$query_indirizzi_spedizione = $this->db->get();
	
		$indirizzi_sped = $query_indirizzi_spedizione->result();
		
		return $indirizzi_sped;
	
	
	}
}
?>
