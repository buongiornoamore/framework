<div role="tabpanel" class="tab-pane transition fade scale" id="whishlist">
  <div class="row">
    <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_001.jpg" alt="Filetti pomodoro">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Filetti pomodoro</a></h3>
          <span class="shop-item-price">
            €3,00
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
     <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/faella_023.jpg" alt="Paccheri di Gragnano">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Paccheri</a></h3>
          <span class="shop-item-price">
            €0,90
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
     <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_002.jpg" alt="Pomodorini gialli">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Pomodorini gialli</a></h3>
          <span class="shop-item-price">
            €3,00
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
     <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/faella_024.jpg" alt="Rgatoni 1kg">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Rgatoni 1kg</a></h3>
          <span class="shop-item-price">
            €2,50
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
     <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_003.jpg" alt="Friarielli">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Friarielli</a></h3>
          <span class="shop-item-price">
            €5,80
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
     <!-- Item -->
    <div class="col-md-4 col-sm-6">
      <div class="shop-item">
        <div class="shop-thumbnail">
          <a href="shop-single.html" class="item-link"></a>
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_001.jpg" alt="Caserecce 1kg">
          <div class="shop-item-tools">
            <a href="#" class="add-to-whishlist" data-toggle="tooltip" title="Rimuovi dalla Whishlist">
              <i class="material-icons close"></i>
            </a>
            <a href="#" class="add-to-cart" data-toggle="tooltip" title="Aggiungi al carrello">
              <em><i class="material-icons add"></i> Carello</em>
              <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"></path>
              </svg>
            </a>
          </div>
        </div>
        <div class="shop-item-details">
          <h3 class="shop-item-title"><a href="shop-single.html">Caserecce 1kg</a></h3>
          <span class="shop-item-price">
            €2,50
          </span>
        </div>
      </div><!-- .shop-item -->
    </div>
  </div>
</div><!-- .tab-pane#whishlist -->