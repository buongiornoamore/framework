   
    <div align="center" class="row">
	      <div align="center" >
		        <div class="row">
		        	<div align="center" class="form-element">	
						 <form method="post"  action="<? echo site_url('salvaIndirizzoSpedizione');?>" accept-charset="utf-8">
						 		<div class="row" >
							 		<div class="col-sm-10">	
								 		<?php if(count($indirizzi_sped) < 5){	?> 
					                    	<button type="button" id="aggiungi_indirizzo" class="btn btn-primary waves-effect waves-light" >AGGIUNGI INDIRIZZO</button>
					                    <?php } ?>
				                    </div>
				                </div>
								<div align="center" id="div_inserisci_indirizzo" class="row well-sm col-sm-11" >
									<div class="row ">
										<div class="col-sm-5">
						                  <button type="button" id="aggiungi_indirizzo_esci" class="btn btn-primary waves-effect waves-light">ESCI</button>
						                  <button type="submit" id="aggiungi_indirizzo_salva" class="btn btn-primary waves-effect waves-light">SALVA</button>
						                </div>
						            </div>
					                <div class="row">
						                <div class="col-sm-6">
						                  <input type="hidden" id="id_indirizzo_sped" name="id_indirizzo_sped" class="form-control" value="" />
								          <input type="text" id="indirizzo_sped" name="indirizzo_sped" class="form-control" value="" placeholder="<? echo lang("create_user_address_label"); ?>" required />
					                	</div>
					                	<div class="col-sm-4">
								          <input type="text" id="civico_sped" name="civico_sped" class="form-control" value="" placeholder="<? echo lang("create_user_street_number_label"); ?>" required /><br/>
								        </div>
							        </div>
							        <div class="row">
							        	 <div class="col-sm-3">
									         <input type="text" id="citta_sped" name="citta_sped" class="form-control" value="" placeholder="<? echo lang("create_user_city_label"); ?>" required />
						                 </div>
						                 <div class="col-sm-3">
									         <input type="text" id="cap_sped" name="cap_sped" class="form-control" value="" placeholder="<? echo lang("create_user_cap_label"); ?>" required />
						                 </div>
						                 <div class="col-sm-4">
									         <input type="text" id="nazione_sped" name="nazione_sped" class="form-control" value="" placeholder="<? echo lang("create_user_state_label"); ?>" required /><br/>
										 </div>
							        </div>
									<div class="row">
										<div class="col-sm-10">
						                  <input type="text" id="riferimento_sped" name="riferimento_sped" class="form-control" value="" placeholder="<? echo lang("LABEL_ADDRESS_REF"); ?>" required />
						                </div>
						            </div> 
						        </div>
					     </form>
				     </div>
			     </div>
	      </div>
              <div > 
                <table class="table table table-striped table-responsive" >
                  <thead>
                    <tr>
                      <th><?php echo lang('LABEL_ADDRESSES');?></th>
                      <th>Civico</th>
                      <th>CAP</th>
                      <th>Citta</th>
                      <th>Nazione</th>
                      <th>Riferimento</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

<?php 
$num = 0;
foreach( $indirizzi_sped as $row ) 
{	
	?>
                    <tr>
                        <td><?php echo $row->indirizzo_sped ?></td>
                        <td><?php echo $row->civico_sped ?></td>
                        <td><?php echo $row->cap_sped ?></td>
                        <td><?php echo $row->citta_sped ?></td>
                        <td><?php echo $row->nazione_sped ?></td>
                        <td><?php echo $row->riferimento_sped ?></td>
	                    <td>
<!--							<button type="button" class="btn btn-primary" name="btn_id_ordine" id="btn_id_ordine" data-toggle="modal" data-target="#bd-example-modal-lg" value="< ?php echo $row->i d_indirizzo_spedizione ? >" >
							  < ?php echo $row->id_ordine ?>
 							</button> -->
                            <button type="button" class="btn_modify_indirizzo_sped btn btn-default btn-simple" id="btn_modify_indirizzo_sped" rel="tooltip" data-placement="bottom" title="Change Date" value="<?php echo $row->id_indirizzo_spedizione ?>,<?php echo $num;?>">
                                <i class="material-icons">edit</i>
                            </button>
						</td>
                    </tr>	
	<?php
	$num++;
}
?>
                  </tbody>
                </table>
              </div>

    </div>

<!--
 
id_indirizzo
indirizzo
civico
cap
nazione
citta

note
 -->