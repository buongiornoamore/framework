<footer class="footer space-top-2x">
  <div class="column">
    <p class="text-sm"><? echo lang("FOOTER_HELP");?> <span class="text-primary"><br><i class="socicon-whatsapp"></i><?= COMPANY_PHONE; ?></span></p>
    <div class="social-bar text-center space-bottom" >
    <!--  <a href="#" class="sb-skype" data-toggle="tooltip" data-placement="top" title="Skype">
        <i class="socicon-skype"></i>
      </a>
      <a href="#" class="sb-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
        <i class="socicon-twitter"></i>
      </a>
      <a href="#" class="sb-google-plus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+">
        <i class="socicon-googleplus"></i>
      </a>
      -->
      <a href="#" class="sb-facebook" data-toggle="tooltip" data-placement="top" title="Facebook" onclick="window.open('<?= FACEBOOK_LINK; ?>','_blank');return false;">
        <i class="socicon-facebook"></i>
      </a>
      <a href="#" class="sb-instagram" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram" onclick="window.open('<?= INSTAGRAM_LINK; ?>','_blank');return false;">
        <i class="socicon-instagram"></i>
      </a>
      <a href="mailto:<?= COMPANY_EMAIL; ?>" class="sb-google-plus" data-toggle="tooltip" data-placement="top" title="" data-original-title="E-mail: <?= COMPANY_EMAIL; ?>">
        <i class="socicon-mail"></i>
      </a>
    </div><!-- .social-bar -->
    <p class="copyright"><?= COMPANY_COPYRIGHT; ?></p>
  </div><!-- .column -->
  <div class="column">
    <h3 class="widget-title">
      <? echo lang("FOOTER_NEWSLETTER");?>
      <small><? echo lang("FOOTER_NEWSLTTER_DESC");?></small>
    </h3>
    <form action="" method="post" target="_blank" class="subscribe-form" novalidate id="newsletterForm">
      <input type="email" class="form-control" style="border:0px solid #ededed;" name="EMAIL" placeholder="<? echo lang("FOOTER_NEWSLTTER_INPUT");?>" id="newsletter-email">
      <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1" value=""></div>
      <button type="button" class="btn-send-footer" id="newsletter-btn"><i class="material-icons send"></i></button>
    </form>
  </div><!-- .column -->
  <div class="column">
    <h3 class="widget-title">
      <? echo lang("FOOTER_PAYMENTS_TITLE");?>
      <small><? echo lang("FOOTER_PAYMENTS_DESC");?></small>
    </h3>
    <div class="cards"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/cards.png" alt="Cards"></div>
    <!-- Scroll To Top Button -->
    <div class="scroll-to-top-btn"><i class="material-icons trending_flat"></i></div>
  </div><!-- .column -->
</footer><!-- .footer -->