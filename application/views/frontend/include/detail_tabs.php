<!-- Product Tabs -->
<section class="container padding-top-2x">
  <!-- Nav Tabs -->
  <ul class="nav-tabs text-center" role="tablist">
    <li class="active"><a href="#description" role="tab" data-toggle="tab"><? echo lang("LABEL_DESCRIPTION"); ?></a></li>
   <!-- <li><a href="#additional" role="tab" data-toggle="tab">Additional Info</a></li>
    <li><a href="#reviews" role="tab" data-toggle="tab"><?// echo lang("LABEL_REVIEWS"); ?> <sup>3</sup></a></li>-->
  </ul><!-- .nav-tabs -->
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane transition fade in active" id="description">
      <div class="row space-top">
       <!-- <div class="col-md-6 space-bottom">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="https://player.vimeo.com/video/105829213?color=77cde3&title=0&byline=0&portrait=0" allowfullscreen></iframe>
          </div>
        </div>-->
        <div class="col-md-12 space-top" align="center">
            <? echo $prod->descrizione; ?>
        </div>
      </div>
    </div><!-- .tab-pane -->
   <!-- <div role="tabpanel" class="tab-pane transition fade" id="additional">
      <div class="row">
        <div class="col-md-6">
          <table class="table-no-border">
            <tr>
              <th>Weight</th>
              <td>2.65 kg</td>
            </tr>
            <tr>
              <th>Dimensions</th>
              <td>120 x 75 x 90 cm</td>
            </tr>
            <tr>
              <th>Materials</th>
              <td>40% wood, 37% cotton, 23% plastic</td>
            </tr>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table-no-border">
            <tr>
              <th>Colors</th>
              <td>Blue, Creme, Orange, Red</td>
            </tr>
            <tr>
              <th>Manufacturer</th>
              <td>Norway</td>
            </tr>
            <tr>
              <th>Other Info</th>
              <td>Repellendus ea laudantium pariatur eum.</td>
            </tr>
          </table>
        </div>
      </div>
    </div><!-- .tab-pane -->
    
    <!-- Review
    <div role="tabpanel" class="tab-pane transition fade" id="reviews">
      <div class="review">
        <div class="review-author-ava">
          <img src="img/shop/reviews/01.jpg" alt="Review Author">
        </div>
        <div class="review-body">
          <div class="review-meta">
            <div class="column">
              <h4 class="review-title">Very stylish. Great Deal!</h4>
            </div>
            <div class="column">
              <span class="product-rating text-warning">
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
              </span>
            </div>
          </div>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</p>
          <cite>Jessie James</cite>
        </div>
      </div>
      <div class="review">
        <div class="review-author-ava">
          <img src="img/shop/reviews/02.jpg" alt="Review Author">
        </div>
        <div class="review-body">
          <div class="review-meta">
            <div class="column">
              <h4 class="review-title">Nice nordic design, afordable price.</h4>
            </div>
            <div class="column">
              <span class="product-rating text-warning">
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star_border"></i>
              </span>
            </div>
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae sequi ipsa fugit officia eos! Sapiente laboriosam molestiae praesentium ducimus culpa. Magnam, odit, optio. Possimus similique eligendi explicabo, dolore, beatae sequi.</p>
          <cite>Susanna Davis</cite>
        </div>
      </div>
      <div class="review">
        <div class="review-author-ava">
          <img src="img/shop/reviews/03.jpg" alt="Review Author">
        </div>
        <div class="review-body">
          <div class="review-meta">
            <div class="column">
              <h4 class="review-title">Godd value for your money.</h4>
            </div>
            <div class="column">
              <span class="product-rating text-warning">
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star"></i>
                <i class="material-icons star_border"></i>
              </span>
            </div>
          </div>
          <p>Anuo ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</p>
          <cite>Donald Murphy</cite>
        </div>
      </div>
      <h4 class="padding-top">Leave a review</h4>
      <form method="post" class="row padding-top">
        <div class="col-sm-4">
          <div class="form-element">
            <input type="text" class="form-control" placeholder="Name*" required>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-element">
            <input type="email" class="form-control" placeholder="Email*" required>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-element form-select">
            <select class="form-control">
              <option>5 stars</option>
              <option>4 stars</option>
              <option>3 stars</option>
              <option>2 stars</option>
              <option>1 star</option>
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-element">
            <textarea rows="8" class="form-control" placeholder="Review*" required></textarea>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-lg-offset-9 col-md-offset-8 col-sm-offset-6">
              <button type="submit" class="btn btn-block btn-primary waves-effect waves-light space-top-none space-bottom-none">Leave Review</button>
            </div>
          </div>
        </div>
      </form>
    </div><!-- .tab-pane -->
  </div><!-- .tab-content -->
</section><!-- .container -->