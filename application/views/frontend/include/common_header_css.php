<!--Favicon-->
<link rel="shortcut icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.ico" type="image/x-icon">
<!-- Google Material Icons -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/material-icons.min.css" rel="stylesheet" media="screen">
<!-- Brand Icons -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/socicon.min.css" rel="stylesheet" media="screen">
<!-- Bootstrap -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- Theme Styles -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/theme.min.css" rel="stylesheet" media="screen">
<!-- Custom Styles -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/custom.css" rel="stylesheet" media="screen">
<!-- CSS per icona WhatsApp -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/flaticon-whatsapp/flaticon-whatsapp.css" rel="stylesheet" media="screen">
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/sweetalert2.min.css" rel="stylesheet" media="screen"><!-- Sweetalert2 -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/page-preloading.js"></script><!-- Page Preloading -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/modernizr.custom.js"></script><!-- Modernizr -->

<!-- GoogleAnalytics -->
<?php if('00000000' != ''. GOOGLE_ANALITYCS_ID .'' ) { include_once("analyticstracking.php"); } ?>


<!-- Pixel di Facebook -->
<?php if('00000000' != ''. PIXEL_FACEBOOK_ID .'' ) { include_once("pixel_di_facebook.php"); } ?>
