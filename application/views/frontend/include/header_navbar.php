<?php
session_start();
require_once('functions.php');
$logo_file = 'logo_small.png'; // logo_small.png
// check unique id for customer inside cookie
if(!isset($_COOKIE['uuid'])) {
	//echo "new uuid: ";
	$uuid = makeUnique(true);
	setcookie('uuid', $uuid, time() + (10 * 365 * 24 * 60 * 60), "/");
    $cookie= array(
	   'name'   => 'uuid',
	   'value'  => $uuid,                            
	   'expire' => time() + (10 * 365 * 24 * 60 * 60),   	
	   'secure' => TRUE
    );
	$this->input->set_cookie($cookie);
	//$_COOKIE['uuid'] = $uuid;
} 
?>
<!-- Navbar -->
<!-- Remove ".navbar-sticky" class to make navigation bar scrollable with the page. -->
<header class="navbar navbar-sticky" id="header-navbar">
  <!-- Site Logo -->
  <a href="<?php echo site_url(lang('PAGE_HOME_URL')); ?>" class="site-logo visible-desktop">
    <img itemprop="image" src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/<? echo $logo_file; ?>" title="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" alt="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" />
  </a><!-- site-logo.visible-desktop -->
  <a href="<?php echo site_url(lang('PAGE_HOME_URL')); ?>" class="site-logo visible-mobile">
  	<img src="<?php echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/<? echo $logo_file; ?>" title="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" alt="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" />
  </a><!-- site-logo.visible-mobile -->
  <!-- Language Switcher -->
  <div class="lang-switcher">
    <div class="lang-toggle">
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/<? echo $this->session->userdata('site_lang'); ?>.png" alt="<? echo $this->session->userdata('site_lang'); ?>">
      <i class="material-icons arrow_drop_down"></i>
      <ul class="lang-dropdown">
      	<?php 
			// load installed langs 
			foreach ($installedLangs as $lingua) {
		?>
        	<li class="lang-dropdown-li"><a href="<?php echo base_url(); ?>LangSwitch/switchLanguage/<? echo $lingua->codice_ci; ?>/<? echo $this->uri->segment(1).'/'.$this->uri->segment(2).($this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : '').($this->uri->segment(4) != '' ? '/'.$this->uri->segment(4) : ''); ?>"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/<? echo $lingua->codice_ci; ?>.png" alt="<? echo $lingua->codice_ci; ?>"><? echo strtoupper(substr($lingua->codice_ci, 0, 2)); ?></a></li>
		<? } ?>   
      </ul>
      <a href="" style="display:none" id="hidden-lang" />
    </div>
  </div><!-- .lang-switcher -->
  <!-- Main Navigation -->
  <!-- Control the position of navigation via modifier classes: "text-left, text-center, text-right" -->
  <nav class="main-navigation text-center">
    <!-- menu custom -->
  	<ul class="menu">
    	<?php 
			// load pagine
			foreach ($menuPages as $pagina) {
		?>
  		<li><a href="<?php echo createUrlMenu($pagina->url_pagina); ?>"><? echo $pagina->nome_pagina; ?></a></li>
        <?php
			}
		?>
    </ul>
  </nav><!-- .main-navigation -->
  <!-- Toolbar -->
  <div class="toolbar">
    <div class="inner">
      <a href="#" class="mobile-menu-toggle"><i class="material-icons menu"></i></a><!--account-->
      <?php
	  	$login_label = lang("LABEL_USER_LOGIN");
      	if ($this->ion_auth->logged_in()){
			$login_label = lang("LABEL_MY_ACCOUNT");
		}
	  ?>		
      <a href="<?php echo site_url(lang('PAGE_ACCOUNT_URL'))?>" title="<? echo $login_label; ?>"><i class="material-icons person"></i></a>
      <div class="cart-btn" id="cart-dropdown-div">
      </div><!-- .cart-btn -->
    </div><!-- .inner -->
  </div><!-- .toolbar -->
  <!-- COMMON HEADER SCRIPTS -->
  <script type="text/jscript">
  	function loadCartDropdown(loadDropDownDiv, reloadCoupon, reloadCartDiv){
		//console.log('loadCartDropDown');
		//$(".se-pre-con").show();
		$("#cart-dropdown-div").html('<div align="center"><img style="width: 30px;" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/field_loader.gif" /></div>');
		return $.ajax({
			url: '<?php echo base_url();?>frontend/Cart/loadCartDropDown',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {"loadDropDownDiv": loadDropDownDiv},
			error: function(msg){
				console.log('error');
				//ShowPopupTime("Richiesta non inviata.<br/>Riprova.", 2000);
				return msg;
			},
			success: function(html){
				$("#cart-dropdown-div").html(html);
				// valida il coupon se esiste
				if($('#coupon-element-input').length && $('#cart-total-float-nocoupon-hidden').val() > 0) {
					console.log('chaiamto da total loadCartDropdown da field ' + $('#coupon-element-input').val());
					validateCouponAjax(reloadCoupon, $('#coupon-element-input').val(), $('#cart-total-float-nocoupon-hidden').val(), reloadCartDiv);
				} else if(reloadCartDiv) {
					loadCart(false);	
				}
				$('#cart-total-show-div').html($('#cart-total-hidden-div').html());
				$('.item-remove-drop').on('click', function(event) {
					event.preventDefault();
					console.log("item-remove-drop: " + $(this).data('id'));
					removeFromCart($(this).data('id'), false);
				});
				return true;
			}
		});
	}

	function removeFromCart(cart_id, showCart){
		console.log('remove from cart cart_id: ' + cart_id);
		//$(".se-pre-con").show();
		return $.ajax({
			url: '<?php echo base_url();?>frontend/Cart/removeFromCart',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {"cart_id": cart_id},
			error: function(msg){
				console.log('error');
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(html){
				//if(showCart)
				//	loadCart(false);
				loadCartDropdown(true, true, showCart);
				// messagge
				swal({
				  position: 'center',
				  type: 'info',
				  title: "<?php echo lang('MSG_CART_REMOVED'); ?>",
				  showConfirmButton: false,
				  timer: 2000
				});
				return true;
			}
		});
	}
	
	function validateCouponAjax(reloadCoupon, coupon_code, total_cart, reloadCartDiv) {
		if(reloadCoupon) {
		// verificare se il coupon è attivo e se è utilizzabile poi verrà caricato nell'ordine
			$("#coupon-element-loader").show();
			$("#submit-coupon-btn").hide();
			return $.ajax({
				url: '<? echo base_url();?>frontend/Cart/validateCoupon',
				type: 'POST',
				cache: false,
				async: true,
				data: {"coupon_code": coupon_code, "total_cart": total_cart},
				error: function(msg){
					$("#coupon-element-loader").hide();
					$("#submit-coupon-btn").show();
					// @TODO verificare il loader se siamo tipo nella pagina shop
					if($(".se-pre-con").length > 0)
						$(".se-pre-con").delay(200).fadeOut("slow");  
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
					$('#coupon-element-div').removeClass('error');
					$('#coupon-element-div').removeClass('valid');
					return false;
				},
				success: function(messJson){
					console.log(messJson);
					var json = $.parseJSON(messJson);
					console.log('returned: ' + json['type'] + ' ' + json['message']);
					$("#coupon-element-loader").hide();
					$("#submit-coupon-btn").show();
					if($(".se-pre-con").length > 0)
						$(".se-pre-con").delay(200).fadeOut("slow");  
					if(json['type'] == 'success') {
						// se il codice è esistente ed attivabile
						$('#coupon-element-div').removeClass('error');
						$('#coupon-element-div').addClass('valid');
					} else if(json['type'] == 'cleared') {
						$('#coupon-element-div').removeClass('valid');
						$('#coupon-element-div').removeClass('error');
					} else {
						$('#coupon-element-div').removeClass('valid');
						$('#coupon-element-div').addClass('error');
						swal({
						  position: 'center',
						  type: json['type'],
				 		  title: json['title'],
				 		  html: json['message'],
						  showConfirmButton: false,
						  timer: 3000
						});
					}
					if(reloadCartDiv)
						loadCart(false);		
					return true;
				}
			});	
		} else {
			$('#coupon-element-div').removeClass('error');
			$('#coupon-element-div').addClass('valid');
			if($(".se-pre-con").length > 0)
				$(".se-pre-con").delay(200).fadeOut("slow");  
		}
	}
	
	function addToWishList(id) {
		console.log('add to wishlist ' + id);
		// restituire il messaggio json di aggiunta alla whislist oppure che non puoi aggiungere 
		// se non sei loggato
		/*if(!$('.se-pre-con').is(':visible')) {
			$('.se-pre-con').show();
		}
		return $.ajax({
			url: '<?// echo base_url();?>frontend/Account/addToWishlist',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {"prductcode": prductcode},
			error: function(msg){
				console.log('error');
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				return msg;
			},
			success: function(html){
				// swal message
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				return true;
			}
		});*/
	}
  </script>
</header><!-- .navbar.navbar-sticky -->
