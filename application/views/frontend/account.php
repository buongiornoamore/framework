<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Account<? echo ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Content -->
    <section class="container padding-top-3x">
      <h1 class="mobile-center">*<? echo $this->ion_auth->user()->row()->first_name; ?>, <span class="text-semibold"><? echo $this->ion_auth->user()->row()->last_name; ?></span></h1>
      <div class="row padding-top">
        <div class="col-sm-9 padding-bottom-2x">
          <!-- Nav Tabs -->
          <ul class="nav-tabs mobile-center" role="tablist">
            <li class="active"><a href="#profile" role="tab" data-toggle="tab">
              <i class="material-icons person"></i>
              <? echo lang("LABEL_PROFILE"); ?>
            </a></li>
            <li><a href="#orders" role="tab" data-toggle="tab">
              <i class="material-icons shopping_cart"></i>
              <? echo lang("LABEL_ORDERS"); ?> (<?php echo count($ordini) ?>)
            </a></li>
            <li><a href="#addresses" role="tab" data-toggle="tab">
              <i class="material-icons local_shipping"></i>
              <? echo lang("LABEL_ADDRESSES"); ?> (<?php echo count($indirizzi_sped) ?>)
            </a></li>
     <!--   <li><a href="#whishlist" role="tab" data-toggle="tab">
              <i class="material-icons favorite"></i>
              < ? echo lang("LABEL_WHISHLIST"); ?> (6)
            </a></li>
       -->
          </ul><!-- .nav-tabs -->
          <!-- Tab Panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane transition fade scale in active" id="profile">
              <form method="post"  action="<? echo site_url('it/salva_account');?>" accept-charset="utf-8">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="text" id="first_name" name="first_name" class="form-control" value="<? echo $this->ion_auth->user()->row()->first_name; ?>" placeholder="<? echo lang("create_user_cod_fiscale_label"); ?>" >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="text" id="last_name" name="last_name" class="form-control" value="<? echo $this->ion_auth->user()->row()->last_name; ?>"  placeholder="<? echo lang("create_user_cod_fiscale_label"); ?>" >
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="text" id="codice_fiscale" name="codice_fiscale" class="form-control" value="<? echo $cliente->codice_fiscale; ?>" placeholder="<? echo lang("create_user_cod_fiscale_label"); ?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="text" id="partita_iva" name="partita_iva" class="form-control" value="<? echo $cliente->partita_iva; ?>" placeholder="<? echo lang("create_user_partita_iva_label"); ?>" >

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="email" id="email" name="email"class="form-control" value="<? echo $this->ion_auth->user()->row()->email; ?>" placeholder="<? echo lang("create_user_email_label"); ?>" readonly >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                      <input type="text" id="telefono"  name="telefono" class="form-control" value="<? echo $cliente->telefono; ?>" placeholder="<? echo lang("create_user_phone_label"); ?>" required>
                    </div>
                  </div>
                </div>
                <!--  
	                <div class="row">
	                  <div class="col-sm-6">
	                    <div class="form-element">
	                      <label for="password">< ? echo lang("create_user_password_label"); ? ></label>
	                      <input type="password" id="password" name="password" class="form-control" value="" placeholder="< ? echo lang("edit_user_password_label"); ? >" placeholder="< ? echo lang("create_user_password_label"); ? >" >
	                    </div>
	                  </div>
	                  <div class="col-sm-6">
	                    <div class="form-element">
	                      <label for="password_confirm">< ? echo lang("create_user_password_confirm_label"); ? ></label>
	                      <input type="password" id="password_confirm" name="password_confirm" class="form-control" value="" placeholder="< ? echo lang("edit_user_password_confirm_label"); ? >" placeholder="< ? echo lang("create_user_password_confirm_label"); ? >" >
	                    </div>
	                  </div>
	                </div>
                -->
                <div class="row">
                    <div class="form-element col-sm-9">
                      	<input type="text" id="indirizzo" name="indirizzo" class="form-control" value="<? echo $cliente->indirizzo_fatt; ?>"  placeholder="<? echo lang("create_user_address_label"); ?>" required />
                    </div>
                    <div class="form-element col-sm-3">
                    	 <input type="text" id="civico" name="civico" class="form-control" value="<? echo $cliente->civico_fatt; ?>" placeholder="<? echo lang("create_user_street_number_label"); ?>" >
                           <!--  <div class="form-element form-select">
                           select class="form-control" name="cap" id="cap" >
                              <option value="" selected>< ? echo lang("create_user_cap_label"); ? ></option>
                              <option value="bern">Bern</option>
                              <option value="london">London</option>
                              <option value="ny">New York</option>
                              <option value="warsaw">Warsaw</option>
                           </select -->
                     </div>
                </div>
                <div class="row">
	                <div class="col-sm-3">
		                  <div class="form-element">
		                     <input type="text" id="citta"  name="citta" class="form-control" value="<? echo $cliente->citta_fatt; ?>" placeholder="<? echo lang("create_user_city_label"); ?>" required />
	                      <!--  <div class="form-element form-select">
	                      select class="form-control" name="nazione" id="nazione" >
	                        <option value="" selected>< ? echo lang("create_user_state_label"); ? ></option>
	                        <option value="1">Italia</option>
	                        <option value="2">Great Britain</option>
	                        <option value="3">Poland</option>
	                        <option value="4">Switzerland</option>
	                        <option value="5">USA</option>
	                      </select -->
	                    </div>
	                 </div>
	                  <div class="col-sm-3">
		                <div class="form-element">
		                	<input type="text" id="cap"  name="cap" class="form-control" value="<? echo $cliente->cap_fatt; ?>" placeholder="<? echo lang("create_user_cap_label"); ?>" required />
		                </div>
	                  </div>
	                 <div class="col-sm-6">
	                    <div class="form-element">
	                      <input type="text" id="nazione"  name="nazione" class="form-control" value="<? echo $cliente->nazione_fatt; ?>" placeholder="<? echo lang("create_user_state_label"); ?>" required />
	                      <!--  <div class="form-element form-select">  
	                      select class="form-control" name="citta" id="citta" >
	                        <option value="" selected>< ? echo  lang("create_user_city_label"); ? ></option>
	                        <option value="1">Bern</option>
	                        <option value="2">London</option>
	                        <option value="3" >New York</option>
	                        <option value="4">Warsaw</option>
	                      </select -->
	                    </div>
	                </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 text-right mobile-center">
                    <input type="text" id="note" name="note" class="form-control" value="<? echo $cliente->note_fatt; ?>" placeholder="<? echo lang("create_user_addressnotes_label"); ?>" >
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 mobile-center" >
	                <div class="form-element">
	                    <input type="text" id="riferimento" name="riferimento" class="form-control" value="<? echo $cliente->riferimento_fatt; ?>" placeholder="riferiento" >
	                </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <label class="checkbox space-top">
                      <input type="checkbox"  id="newsletter" name="newsletter" <? echo $cliente->newsletter == 0 ? '' : 'checked'; ?> > <? echo lang("MSG_SAVE_NEWSLETTER"); ?>
                    </label>
                  </div>
                  <div class="col-sm-6 text-right mobile-center">
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><? echo lang("LABEL_UPDATE"); ?></button>
                  </div>
                </div>
              </form>
            </div><!-- .tab-pane#profile -->
<!-- class="tab-pane transition fade scale in active" id="profile" -->
            <div role="tabpanel" class="tab-pane transition fade scale" id="orders" >
              <div > 
                <table class="table table table-striped table-responsive" >
                  <thead>
                    <tr>
                      <th>Ordine #</th>
                      <th>Data</th>
                      <th>Stato Pagamento</th>
                      <th>Stato Ordine</th>
                      <th>Totale</th>
                    </tr>
                  </thead>
                  <tbody>
<?php 
foreach( $ordini as $row ) 
{
	?>
                    <tr>
	                    <td>
							<button type="button" class="btn btn-primary" name="btn_id_ordine" id="btn_id_ordine" data-toggle="modal" data-target="#bd-example-modal-lg" value="<?php echo $row->id_ordine ?>" >
							  <?php echo $row->id_ordine ?>
							</button>
						</td>
                        <!-- <td><input type="hidden" name="id_ordine" id="id_ordine" value="< ? php echo $row->id_ordine; ? >" /></td> -->
                        <td><?php echo $row->data_ordine ?></td>
                        <td><?php echo $row->desc_stato_pagamento ?></td>
                        <td><span class="text-danger"><?php echo $row->desc_stato_ordine ?></span></td>
                        <td><?php echo $row->totale_ordine ?></td>
                    </tr>	
	<?php
}
?>
                  </tbody>
                </table>
              </div>


            </div><!-- .tab-pane#orders -->
		    <!-- inizio div per dettaglio ordine -->
			<div class="modal fade " id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content" id="popup_dettaglio_ordine" >
					
					<!-- RITORNO DELLA CHIAMATA AJAX -->
					
			    </div>
			  </div>
			</div>
			<!-- fine div per dettaglio ordine -->
			<div role="tabpanel" class="tab-pane transition fade scale" id="addresses" >
			
	
	            <!-- < ? require_once('include/whishlist.php'); ?> <!-- whislist cart -->
	            <? require_once('include/addresses.php'); ?> <!-- shipping addresses -->
	            
			</div><!-- .tab-pane#addresses -->
          </div><!-- .tab-content -->
        </div><!-- .col-sm-8 -->

        <!-- Sidebar -->
        <div class="col-sm-3 padding-bottom-2x">
          <aside class="mobile-center">
            <? if(POINTS_ENABLED) { ?>
            <!-- POINTS -->
            <h3><span class="h5"><? echo lang("LABEL_GOT"); ?></span> <span class="text-semibold"><? echo $cliente->punti; ?></span> <span class="h5"><? echo lang("LABEL_POINTS"); ?></span></h3>
            <p class="text-sm text-gray"><? echo lang("LABEL_POINTS_DESC"); ?></p>
          <!--  
          	@TODO gestione punti da parte del cliente
            <a href="points" class="btn btn-default btn-block icon-left btn-block">
                <i class="material-icons receipt"></i>
           		<?// echo lang("LABEL_MANAGE_POINTS"); ?>
            </a> --
             <? } ?>
            <a href="home" class="btn btn-ghost btn-block space-top-none">
            	<i class="material-icons shopping_basket"></i>
              	<?// echo lang("LABEL_BACK_SHOP"); ?>
            </a><!-- < ? php echo site_url('logout') ? > -->
            <a href="<? echo site_url('it/logout');?>" class="btn btn-primary btn-block icon-left btn-block">
            	<i class="material-icons arrow_back"></i>
            	<? echo lang("LABEL_LOGOUT"); ?>
            </a>
          </aside>
        </div><!-- .col-sm-4 -->
      </div><!-- .row -->
    </section><!-- .container -->
    

    <!-- PAGE CONTENT -->

    <? require_once('include/footer.php'); ?> <!-- Footer -->

  </div><!-- .page-wrapper -->

  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->

</body><!-- <body> -->
  <script type="text/javascript">
  	$(window).load(function() {
  		console.log("Siamo nella pagina di account");
		loadCartDropdown(true, false, false);
    });

//     $("#orders").on("click", function(e) {
//           //console.log("Siamo nella pagina di account");
//           alert('########');
//     });

    $(document).ready(function() {
	 
     //Faccio sparire il pulsante "Mostra"
     $("#div_inserisci_indirizzo").hide();
     $("#aggiungi_indirizzo").click(function(){
         
	     $("#button_aggiungi_indirizzo").hide();
	     $("#div_inserisci_indirizzo").show();

     });

     $("#aggiungi_indirizzo_esci").click(function(){
	     $("#button_aggiungi_indirizzo").show();
	     $("#div_inserisci_indirizzo").hide();
     });


     $("#btn_id_ordine").click(function(){
         
     	var id_ordine =  $("#btn_id_ordine").val(); 

		 console.log("Siamo nella pagina di account : " + id_ordine);
		 
	     $.ajax({  
		   	  type: 'POST',
		   	  url: '<? echo base_url();?>frontend/Account/dettaglio_ordine',  
		   	  data: 'id_ordine=' + id_ordine,
		   	  dataType: 'html',
		   	  async: true,
		   	  success: function(risposta) {  
		   		console.log("Siamo nella pagina di account : risposta " + risposta);
		   		$("#popup_dettaglio_ordine").html(risposta);  
		   	  },
		   	  error: function(){
		   		console.log("Siamo nella pagina di account : error " );
		   		//alert("Chiamata fallita!!!");
		   	  } 
	     });
     
   	 });

		var indirizzi_sped = new Array();
		var i=0;
     <?php 
      		 foreach( $indirizzi_sped as $row ) 
      		 {
      		 	
     			?>
   				indirizzo_sped[i] = new Array('<?php echo $row->indirizzo_sped ?>','<?php echo $row->civico_sped ?>','<?php echo $row->cap_sped ?>','<?php echo $row->citta_sped ?>','<?php echo $row->nazione_sped ?>','<?php echo $row->riferimento_sped ?>');
   				indirizzi_sped.push(indirizzo_sped[i]);
   				i++;
     	<?php
      		 }
     			?>   

     		$(".btn_modify_indirizzo_sped").click(function(){
		   	 console.log("Siamo in fase di modifica dell'indirizzo " + $(this).val() );
		   	 var id_indirizzo_sped = $(this).val(); 
		
		   	 var res = id_indirizzo_sped.split(",");
		   	 var riga = res[1];
// 		   	 alert(res + ' - ' + indirizzi_sped[riga][1] + ' riga: ' + riga  );
		   	 $('#id_indirizzo_sped').val(res[0]);
		   	 $('#indirizzo_sped').val(indirizzi_sped[riga][0]);
		   	 $('#civico_sped').val(indirizzi_sped[riga][1]);
		   	 $('#cap_sped').val(indirizzi_sped[riga][2]);
		   	 $('#citta_sped').val(indirizzi_sped[riga][3]);
		   	 $('#nazione_sped').val(indirizzi_sped[riga][4]);
		   	 $('#riferimento_sped').val(indirizzi_sped[riga][5]);
		   	 
		     $("#button_aggiungi_indirizzo").hide();
		     $("#div_inserisci_indirizzo").show();
		
    	});

    });



  </script>

</html>
