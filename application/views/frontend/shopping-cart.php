<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('LABEL_SHOPPING_CART') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Container -->
    <section class="container padding-top-3x padding-bottom maximized-container">
      <h1 class="space-top-half"><? echo lang("LABEL_CART"); ?></h1>
      <div class="row padding-top" id="cart-div">
      </div><!-- .row -->
    </section><!-- .container -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
    <? require_once('include/common_header_js.php'); ?> <!-- Import js -->    
</body><!-- <body> -->
 <script type="text/javascript"> 
	$(window).load(function() {
		loadCart(true);		
		//loadCartDropdown(false, true, false);
    }); 
	
	function loadCart(loadCartDropDown){
		$(".se-pre-con").show();
		return $.ajax({
			url: '<? echo base_url();?>frontend/Cart/loadCart',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {},
			error: function(msg){
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_PAYPAL_ERROR'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(html){
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				$("#cart-div").html(html);		
				if(loadCartDropDown)
					loadCartDropdown(false, true, false);
				$('.item-remove').on('click', function(event) {
					event.preventDefault();
					console.log("item-remove: " + $(this).data('id'));
					removeFromCart($(this).data('id'), true);
				});	
				$(".incr-btn").on("click", function(e) {
					var $button = $(this);
					var oldValue = $button.parent().find('.quantity').val();
					$button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
					if ($button.data('action') == "increase") {
						var newVal = parseFloat(oldValue) + 1;
					} else {
					 // Don't allow decrementing below 1
						if (oldValue > 1) {
							var newVal = parseFloat(oldValue) - 1;
						} else {
							newVal = 1;
							$button.addClass('inactive');
						}
					}
					$button.parent().find('.quantity').val(newVal);
					e.preventDefault();
					// action quantity custom
					if(oldValue != newVal)
						updateToCart($(this).data('id'), newVal);
				});
				$("#submit-coupon-btn").on("click", function(e) {
					e.preventDefault();
					// se l'utente ha inserito un valore di coupon valida 
					//validateCouponAjax(true, $('#coupon-element-input').val(), $('#cart-total-float-nocoupon-hidden').val(), true);				
					$(".se-pre-con").show();
					loadCartDropdown(false, true, true);
				});
				
				return true;
			}
		});
	} 
	 
	function updateToCart(cart_id, qty){
		console.log('update to cart cart_id: ' + cart_id + ' qty: ' + qty);
		//$(".se-pre-con").show();
		return $.ajax({
			url: '<? echo base_url();?>frontend/Cart/updateToCart',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {"cart_id": cart_id, "qty": qty},
			error: function(msg){
				console.log('error');
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(html){
				//loadCart();	
				loadCartDropdown(false, true, true);
				// messagge
				swal({
				  position: 'center',
				  type: 'info',
				  title: "<?php echo lang('MSG_CART_UPDATED'); ?>",
				  showConfirmButton: false,
				  timer: 2000
				});
				return true;
			}
		});
	} 
  </script>		    
</html>
