<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title"><?php echo $data['curr_function_title'];?>&nbsp;<a href="<?php echo site_url('admin/orders')?>" class="btn btn-danger btn-simple btn-little-icon" title="Ritorna agli ordini">
                                            <i class="material-icons">arrow_back</i>
                                        </a>
                                    </h4>    
                                    <!-- mettere una dropdown per cambiare prodotto -->
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		var crudState = '<?php echo $data['curr_state'];?>';
		//console.log('state: ' + crudState);
		//if(crudState != 'add') {
		if($('.image-thumbnail').length) {
			$('.image-thumbnail').fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	false,
				'type'          :   'image'
			});	
		}
		//}
		
		// Nasconde il pulsante per cancellare immagine
		$('.delete-anchor').hide();
		
		if($('#field-id_prodotto').length) {
			$("#field-id_prodotto").chosen().change(function() {
				// caricare dinamicamnte in ajax la select delle varianti prodotto
				loadDependantSelectVarianti($(this).val(), $('#field-id_variante').val());
			});

			// disabilita in partenza
			if(!$('#field-id_variante').val()) {
				$('#field-id_variante').empty();
				$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
			} else {
				loadDependantSelectVarianti($("#field-id_prodotto").val(), $('#field-id_variante').val());
			}
		}
    });
	
	function loadDependantSelectVarianti(prod_id, variante_id) {
		//console.log('p ' + prod_id + ' v ' + variante_id);
		$( ".loader" ).remove();
		$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
		$('#field-id_variante').next().after('&nbsp;<img style="width:15px !important;" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/field_loader.gif" class="loader" />');
		if(prod_id) {
			$.ajax({
				url: '<? echo base_url(); ?>admin/Orders/loadVariantiAjax/'+prod_id,
				type: "GET",
				dataType: "json",
				success:function(data) {
					$('#field-id_variante').empty();
					$('#field-id_variante').append('<option value=""></option>');
					$.each(data, function(key, value) {
						$('#field-id_variante').append('<option value="'+ value.id_variante +'">'+ value.codice +'</option>');
					});
					if(data.length > 0) {
						$('#field-id_variante').prop('disabled', false).trigger("chosen:updated");
						if(variante_id != null && variante_id != '' && variante_id > 0)
							$('#field-id_variante').val(variante_id).trigger("chosen:updated");
						else 
							$('#field-id_variante').val('').trigger('chosen:updated');		
					} else {	
						$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
					}
					$('.loader').hide();
				}
			});
		}else{
			$('#field-id_variante').empty();
			$('#field-id_variante').val('').trigger('chosen:updated');	
			$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
		}
	}
</script>
</html>