<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">work</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Categorie&nbsp;&nbsp;<a href="" id="guide-btn" title="Visualizza la guida"><i class="fa fa-info-circle"></i></a></h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		console.log('ready ');
		$('.image-thumbnail').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	600, 
			'speedOut'		:	200, 
			'overlayShow'	:	false,
			'type'          :   'image'
		});	
		
    	// sweet guide
		$("#guide-btn").on("click", function(e) {
			e.preventDefault();
			swal({
			  title: '<b>Guida all\'uso</b>',
			  width: 800,
			  type: 'question',
			  html: '<p align="justify">La funzionalità categorie permette di gestire le categorie dei prodotti presenti nello <b>shop</b>. E\' possibile inserire nuove categorie oltre che modificare, visualizzarle e nasconderle.</p><p align="justify">Ogni categoria è composta da un insieme di campi, alcuni obbligatori e altri facoltativi, oltre che da un immagine caratterizzante.</p><p align="justify">Se la categoria viene disattivata o cancellata i prodotti ad essa collegata non saranno più visibili nello <b>shop</b>.</p><p align="justify">Nelle tabelle di elenco sono presenti filtri di ricerca generali <i class="fa fa-search" style="color: #9c27b0"></i> oppure specifici sulle singole colonne. Le colonne possono essere ordinate (drill down) cliccando direttamente sul nome della colonna (es. Codice). Le liste tabellari possono essere stampate o esportate in excel per utilizzi esterni.</p><p align="justify">In generale non è prevista la cancellazione delle categorie inserite nel sistema in quanto queste potrebbero essere legate a prodotti e ordini effettuati in passato (storico ordini) e quindi restano necessarie ai fini della storicizzazione dei dati e alla produzione di statistiche di ampio raggio (mensili, annuali, etc.). Se si intende escludere una categoria dallo shop si può procedere entrando nella modalita di <b>modifica</b> <i class="fa fa-pencil text-info"></i> e aggiornando lo stato a <b>sospeso</b> oppure <b>cancellato</b>.</p><p align="justify">In questo modo tutti i prodotti di quella categoria non saranno visibili agli utenti dello shop ma resteranno modificabili nella sezione <b>admin</b>.</p>',
			  showCloseButton: true,
			  showCancelButton: false,
			  confirmButtonText:
				'<i class="fa fa-close"></i> Chiudi'
			});
		});
	});
</script>
</html>