<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
if(isset($css_files)) {
	foreach($css_files as $file):
    	echo '<link type="text/css" rel="stylesheet" href="'.$file.'" />';
	endforeach; 
}	
if(isset($js_files)) {
	foreach($js_files as $file):
    	echo '<script src="'.$file.'?>"></script>';
	endforeach; 
}?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title"><?php echo $data['curr_function_title'];?>&nbsp;<a href="<?php echo site_url('admin/orders')?>" class="btn btn-danger btn-simple btn-little-icon" title="Ritorna agli ordini">
                                            <i class="material-icons">arrow_back</i>
                                        </a>
                                    </h4>  
                                    <h3>Dati ordine</h3>  
                                    <? 
										$ordine = $data['ordine']; 
										
										$fatt_indirizzo = '';
										$fatt_cap = '';
										$fatt_citta = '';
										$fatt_stato_paese = '';
										$fatt_note = '';
										$sped_indirizzo = '';
										$sped_cap = '';
										$sped_citta = '';
										$sped_stato_paese = '';
										$sped_note = '';
										if($ordine->id_indirizzo_fatturazione_spedizione > 0) {
											$fatt_indirizzo = $ordine->indirizzo_fatt.', '.$ordine->civico_fatt;
											$fatt_cap = $ordine->cap_fatt;
											$fatt_citta = $ordine->citta_fatt;
											$fatt_stato_paese = $ordine->nazione_fatt;
											$fatt_note = $ordine->note_fatt;
											$sped_indirizzo = $ordine->indirizzo_fatt.', '.$ordine->civico_fatt;
											$sped_cap = $ordine->cap_fatt;
											$sped_citta = $ordine->citta_fatt;
											$sped_stato_paese = $ordine->nazione_fatt;
											$sped_note = $ordine->note_fatt;
										} else {
											// da tabella indirizzo_fatturazione
											$fatt_indirizzo = $ordine->indirizzo_fatt.', '.$ordine->civico_fatt;
											$fatt_cap = $ordine->cap_fatt;
											$fatt_citta = $ordine->citta_fatt;
											$fatt_stato_paese = $ordine->nazione_fatt;
											$fatt_note = $ordine->note_fatt;
		
											// da tabella indirizzo_spedizione
											$sped_indirizzo = $ordine->indirizzo_sped.', '.$ordine->civico_sped;
											$sped_cap = $ordine->cap_sped;
											$sped_citta = $ordine->citta_sped;
											$sped_stato_paese = $ordine->nazione_sped;
											$sped_note = $ordine->note_sped;
										}
										
										echo '<div class="row" style="font-size:16px;" align="center">';
											echo '<div class="col-sm-3 vcenter"><b>Ordine</b> # '.$ordine->id_ordine.'<br>'.$ordine->desc_stato_ordine.'</div>';
											echo '<div class="col-sm-3 vcenter"><b>Totale</b><br>'.stampaValutaHtml($ordine->totale_ordine, true, true).'</div>';					
											echo '<div class="col-sm-3 vcenter"><b>Data</b><br>'.formattaData($ordine->data_ordine, 'd/m/Y').'</div>';
											echo '<div class="col-sm-3 vcenter"><i class="fa '.$ordine->icon_tipo_pagamento.'"></i> '.$ordine->desc_tipo_pagamento.'<br>'.$ordine->desc_stato_pagamento.'</div>';
										echo '</div><br><br>';
										// cliente
										echo '<div class="row" style="font-size:16px;background-color:#ccc" align="center">';
											echo '<div class="col-sm-12 vcenter">
													<b>Cliente</b><br>'
														.$ordine->cognome.' '.$ordine->nome.'<br>'
														.$ordine->email.'<br>'.$ordine->telefono.
														($ordine->note_ordine != '' ? '<br>Note: ' . $ordine->note_ordine : '').
												 '</div>';
										echo '</div><br><br>';
										echo '<div class="row" style="font-size:16px;" align="center">';
											echo '<div class="col-sm-6 vcenter">
													<b>Indirizzo di fatturazione</b><br>'
														.$fatt_indirizzo.' - '.$fatt_citta.'<br>'.$fatt_cap.' ['.$fatt_stato_paese.']'.($ordine->partita_iva != '' ? '<br>P.Iva: ' . $ordine->partita_iva : '').($ordine->codice_fiscale != '' ? '<br>CF: ' . $ordine->codice_fiscale : '').($fatt_note != '' ? '<br>Note: ' . $fatt_note : '').
												 '</div>';
												echo '<div class="col-sm-6 vcenter">
													<b>Indirizzo di spedizione</b><br>'
														.$sped_indirizzo.' - '.$sped_citta.'<br>'.$sped_cap.' ['.$sped_stato_paese.']'.($sped_note != '' ? '<br>Note: ' . $sped_note : '').
												 '</div>';	 
										echo '</div>';
									?>
                                    <h3>Carrello</h3>
									<?
									echo '<div class="row" style="font-weight:bold;font-size:16px;background-color:#ccc;padding:5px">';
										echo '<div class="col-sm-2 vcenter">Immagine prodotto</div>';
										echo '<div class="col-sm-4 vcenter">Descrizione</div>';
										echo '<div class="col-sm-1 vcenter" align="center">Quantità</div>';
										echo '<div class="col-sm-1 vcenter" align="center">Prezzo</div>';
										echo '<div class="col-sm-2 vcenter" align="center">Prezzo scontato</div>';
										echo '<div class="col-sm-1 vcenter" align="center">Taglia</div>';
										echo '<div class="col-sm-1 vcenter" align="center">Colore</div>';
									echo '</div>';
									
									if(count($data['carrello_prodotti']) && count($data['carrello_varianti'])) {	
										foreach($data['carrello_prodotti'] as $prodotto) {
											echo '<div class="row" style="font-size:14px;border-bottom: 1px solid #ccc;padding:5px">';
												echo '<div class="col-sm-2 vcenter">
														<a href="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/storico/'.$prodotto->url_immagine.'" class="image-thumbnail">
															<img style="width: 150px !important" src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/storico/'.$prodotto->url_immagine.'" />
														</a>
													  </div>';
												echo '<div class="col-sm-4 vcenter"><b>'.$prodotto->nome.'</b><br>'.$prodotto->codice.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.$prodotto->qty.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.stampaValutaHtml($prodotto->prezzo, true, true).'</div>';
												echo '<div class="col-sm-2 vcenter" align="center">'.stampaValutaHtml($prodotto->prezzo_scontato, true, true).'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.$prodotto->taglia_codice.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.$prodotto->colore_prodotto.'</div>';
											echo '</div>';
										}
										foreach($data['carrello_varianti'] as $variante) {
											echo '<div class="row" style="font-size:14px;border-bottom: 1px solid #ccc;padding:5px">';
												echo '<div class="col-sm-2 vcenter">
														<a href="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/storico/'.$variante->url_immagine.'" class="image-thumbnail">
															<img style="width: 150px !important" src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/storico/'.$variante->url_immagine.'" />
														</a>
													  </div>';
												echo '<div class="col-sm-4 vcenter"><b>'.$variante->nome.'</b><br>'.$variante->codice.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.$variante->qty.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.stampaValutaHtml($variante->prezzo, true, true).'</div>';
												echo '<div class="col-sm-2 vcenter" align="center">'.stampaValutaHtml($variante->prezzo_scontato, true, true).'</div>';
												echo '<div class="col-sm-1 vcenter" align="center">'.$variante->taglia_codice.'</div>';
												echo '<div class="col-sm-1 vcenter" align="center"><span style="font-weight:bold;border: 1px solid #' .$variante->colore_prodotto_codice.';padding:2px;">'.$variante->colore_prodotto.'</span></div>';
											echo '</div>';
										}
									} else {
										echo '<div class="row" align="center" style="font-size:14px;border-bottom: 1px solid #ccc;padding:5px">';
												echo '<div class="col-sm-12 vcenter">Nessun prodotto presente nel carrello</div>';
										echo '</div>';		
									}
									?>  
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		if($('.image-thumbnail').length) {
			$('.image-thumbnail').fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	false,
				'type'          :   'image'
			});	
		}
    });
</script>
</html>