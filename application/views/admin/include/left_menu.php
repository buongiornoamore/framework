<div class="se-pre-con" style="display:none"></div> <!-- Page Pre-Loader -->
<div class="sidebar" data-active-color="rose" data-background-color="white" data-image="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/sidebar_default.jpg">
<!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
    <div class="logo">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text">
            <?// echo COMPANY_NAME; ?>
            <img src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_small.png" />
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="<?php echo SITE_URL_PATH; ?>" class="simple-text">
            <img src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_mini.png" />
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/faces/avatar2.jpg" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <? echo $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name; ?>
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="#">Dati profilo</a>
                        </li>
                        <li>
                            <a href="" onClick="return showConfirmDialog('Conferma logout', 'Sei sicuro di voler abbandonare la sessione?', '', '', '', '', '', '', '<?php echo site_url('admin/logout')?>', 'false'); "><? echo lang("LABEL_LOGOUT"); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <!-- current_page viene settata in header_admin.php -->
            <li <? echo ($current_page == 'ADMIN-ORDERS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/orders')?>">
                    <i class="material-icons">shopping_cart</i>
                    <p>Ordini</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#prodotti" class="collapsible-link" data-id="prodotti">
                    <i class="material-icons">shopping_basket</i>
                    <p>Prodotti
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="prodotti">
                    <ul class="nav">
                        <li <? echo ($current_page == 'ADMIN-PRODUCTS' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/products')?>">
                            	<i class="material-icons">library_books</i>
								<p>Catalogo</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-CATEGORIES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/categories')?>">
                            	<i class="material-icons">work</i>
								<p>Categorie</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-SIZES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/sizes')?>">
                            	<i class="material-icons">format_size</i>
								<p>Taglie</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-TAGS' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/tags')?>">
                            	<i class="material-icons">label</i>
								<p>Tags</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-PRODUCTCOLORS' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/productcolors')?>">
                            	<i class="material-icons">color_lens</i>
								<p>Colori</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li <? echo ($current_page == 'ADMIN-COUPONS' ? 'class="active"' : ''); ?>>
                <a href="<?php echo site_url('admin/coupons')?>">
                    <i class="material-icons">card_giftcard</i>
                    <p>Coupons</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#gallery" class="collapsible-link" data-id="gallery">
                    <i class="material-icons">photo_camera</i>
                    <p>Gallery
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="gallery">
                    <ul class="nav">
                        <li <? echo ($current_page == 'ADMIN-GALLERY-CATEGORIES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/gallerycategories')?>">
                            	<i class="material-icons">photo_album</i>
								<p>Categorie</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-GALLERY-IMAGES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/galleryimages')?>">
                            	<i class="material-icons">photo</i>
								<p>Immagini</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
             <li>
                <a data-toggle="collapse" href="#email" class="collapsible-link" data-id="email">
                    <i class="material-icons">local_post_office</i>
                    <p>Email
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="email">
                    <ul class="nav">
                    	<li <? echo ($current_page == 'ADMIN-CONTATTI' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/contatti')?>">
                                <i class="material-icons">contact_mail</i>
                                <p>Contatti</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-NEWSLETTER' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/newsletter')?>">
                                <i class="material-icons">send</i>
                                <p>Newsletter</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-EMAILTEMPLATES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/emailtemplates')?>">
                                <i class="material-icons">dashboard</i>
                                <p>Templates</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <? if($this->ion_auth->is_admin()) { ?>
             <li>
                <a data-toggle="collapse" href="#configurazioni" class="collapsible-link" data-id="configurazioni">
                    <i class="material-icons">settings</i>
                    <p>Configurazioni
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="configurazioni">
                    <ul class="nav">
                    	<li <? echo ($current_page == 'ADMIN-IMPOSTAZIONI' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/impostazioni')?>">
                                <i class="material-icons">build</i>
                                <p>Impostazioni</p>
                            </a>
                        </li>  
                        <li <? echo ($current_page == 'ADMIN-LANGUAGES' ? 'class="active"' : ''); ?>>
                            <a href="<?php echo site_url('admin/languages')?>">
                                <i class="material-icons">language</i>
                                <p>Lingue</p>
                            </a>
                        </li>
                        <li <? echo ($current_page == 'ADMIN-PAGES' ? 'class="active"' : ''); ?>>
							<a href="<?php echo site_url('admin/pages')?>">
								<i class="material-icons">content_paste</i>
								<p>Pagine</p>
							</a>
						</li>
                        <li <? echo ($current_page == 'ADMIN-HOMESLIDER' ? 'class="active"' : ''); ?>>
							<a href="<?php echo site_url('admin/homeslider')?>">
								<i class="material-icons">slideshow</i>
								<p>Home slider</p>
							</a>
						</li>

                    </ul>
                </div>
            </li>
            <? } ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
		var parentMenuActive = '<?php echo $data['collapseParentMenu']; ?>';
		if(parentMenuActive != '' && !$('#'+parentMenuActive).attr("aria-expanded")){
			$('#'+parentMenuActive).collapse(); 	
		}
		
		$('.collapsible-link').on('click', function(e){
		   e.preventDefault();
			console.log($(this).data('id'));
		   if(parentMenuActive != '' && $(this).data('id') != parentMenuActive) {
			 return true;
		   } else {
		   	return false;
		   }
		});	
	});	
</script>
