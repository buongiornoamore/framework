<?
	$current_page = (isset($data['curr_page']) ? $data['curr_page'] : (isset($curr_page) ? $curr_page : ''));
	$current_page_title = (isset($data['curr_page_title']) ? $data['curr_page_title'] : (isset($curr_page_title) ? $curr_page_title : ''));
?>
    <link rel="apple-touch-icon" sizes="76x76" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/apple-icon.png" />
    <link rel="icon" type="image/png" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.png" />
    <!-- Bootstrap core CSS  -->  
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/material-dashboard.css" rel="stylesheet" />
    <? if(isset($data['resourcetype']) && $data['resourcetype'] != 'CRUD') { ?>
	<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-ui.min.js" type="text/javascript"></script>   
    <link href="<? echo SITE_URL_PATH; ?>/assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css" rel="stylesheet" />
    <script src="<? echo SITE_URL_PATH; ?>/assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
    <script src="<? echo SITE_URL_PATH; ?>/assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js" type="text/javascript"></script>  
	<? } ?>
    <!--  CSS for Demo Purpose, don't include it in your project    
    <link href="<?// echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/demo.css" rel="stylesheet" /> -->
    <!--     Fonts and icons     -->
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN; ?>/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
        