<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template -->
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">card_giftcard</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Coupons</h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>
                        	</div>
                        </div>
                    </div>
                 </div>
            </div>
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		// 1 sconto singolo || 2 percentuale singolo || 3 sconto multiplo || 4 percentuale multiplo 
		if($('#field-tipo_coupon').length) {
			// effettaure il primo set e disabilitare quello non selezionato
			valideteTipoCoupon($("#field-tipo_coupon").val());
			$("#field-tipo_coupon").chosen().change(function() {
				valideteTipoCoupon($(this).val());
			});
		}
    });
	
	function valideteTipoCoupon(valore) {
		if(valore == 1 || valore == 3) {
			$('#field-importo_coupon').attr('disabled', false);
			$('#field-percentuale_coupon').val(0);
			$('#field-percentuale_coupon').attr('disabled', true);
		} else if(valore == 2 || valore == 4){
			$('#field-percentuale_coupon').attr('disabled', false);
			$('#field-importo_coupon').val(0);
			$('#field-importo_coupon').attr('disabled', true);
		}
	}
</script>
</html>
